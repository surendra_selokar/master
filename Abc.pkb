CREATE OR replace PACKAGE BODY crc_coresvc_multibuy_promo AS
----------------------------------------------------------------------------------------------
--Cursor define for Header data validation

    CURSOR c_svc_promo_hdr (
        i_process_id   NUMBER,
        i_chunk_id     NUMBER
    ) IS
    SELECT
        hdr.rowid AS st_rid,
        hdr.process_id,
        hdr.chunk_id,
        hdr.row_seq,
        hdr.process$status,
        hdr.promo_name,
        hdr.promo_type,
        hdr.promo_start_date,
        hdr.promo_end_date,
        hdr.mechanic,
        hdr.forecast_group,
        hdr.cust_segment,
        hdr.right_earn_ind,
        hdr.right_spend_ind,
        hdr.sticker_earn_ind,
        hdr.sticker_spend_ind,
        hdr.coupon_spend_ind,
        hdr.superburn_points,
        hdr.extra_points,
        hdr.loc_type,
        hdr.location,
        hdr.legacy_promo_id,
        hdr.reward_type,
        hdr.create_id,
        hdr.create_datetime,
        hdr.last_upd_id,
        hdr.last_upd_datetime,
		sget.reedem_amount
    FROM
        crc_svc_multibuy_hdr_stg hdr,
		crc_svc_multibuy_get_stg sget       
    WHERE
	    hdr.process_id = sget.process_id
        AND hdr.chunk_id = sget.chunk_id
        AND hdr.process_id = i_process_id
        AND hdr.chunk_id = i_chunk_id
		and rownum  = 1;

--Cursor define for Buy data validation

    CURSOR c_svc_promo_buy (
        i_process_id   NUMBER,
        i_chunk_id     NUMBER
    ) IS
      SELECT
        buy.rowid AS st_rid,
        buy.process_id,
        buy.chunk_id,
        buy.row_seq,
        buy.process$status,
        buy.buylist_group,
        buy.skulist,
        buy.item,
        buy.supplier,
        buy.qual_type,
        buy.qual_value,
        buy.REEDEM_AMOUNT,
        buy.CC_AMOUNT,
        buy.CC_START_DATE,
        buy.CC_END_DATE,
        buy.NEW_GP_AMOUNT,
        buy.NEW_GP_PERCENT,
        buy.ROQ_UNITS,
        buy.ROQ_INNERS,
        buy.OI_VALUE,
        buy.create_id,
        buy.create_datetime,
        buy.last_upd_id,
        buy.last_upd_datetime,
        hdr.location,
        hdr.loc_type,
        hdr.reward_type,
        hdr.promo_start_date,
        hdr.promo_end_date,
        hdr.promo_name
    FROM
        crc_svc_multibuy_buy_stg   buy,
        crc_svc_multibuy_hdr_stg   hdr
    WHERE
        buy.process_id = hdr.process_id
        AND buy.chunk_id = hdr.chunk_id
        AND buy.process_id = i_process_id
        AND buy.chunk_id = i_chunk_id;


--Cursor define for Get data validation

    CURSOR c_svc_promo_get (
        i_process_id   NUMBER,
        i_chunk_id     NUMBER
    ) IS
    SELECT
        sget.rowid AS st_rid,
        sget.process_id,
        sget.chunk_id,
        sget.row_seq,
        sget.process$status,
        sget.getlist_group,
        sget.buylist_group,
        sget.skulist,
        sget.item,
        sget.supplier,
        sget.qual_type,
        sget.qual_value,
        sget.change_type,
        sget.change_amount,
        sget.reedem_amount,
        sget.cc_amount,
        sget.cc_start_date,
        sget.cc_end_date,
        sget.new_gp_amount,
        sget.new_gp_percent,
        sget.roq_units,
        sget.roq_inners,
        sget.oi_value,
        sget.create_id,
        sget.create_datetime,
        sget.last_upd_id,
        sget.last_upd_datetime,
        hdr.promo_name,
        hdr.location,
        hdr.loc_type,
        hdr.promo_start_date,
        hdr.promo_end_date,
		hdr.reward_type  
    FROM
        crc_svc_multibuy_get_stg   sget,
        crc_svc_multibuy_hdr_stg   hdr
    WHERE
        sget.process_id = hdr.process_id
        AND sget.chunk_id = hdr.chunk_id
		AND sget.process_id = i_process_id
        AND sget.chunk_id = i_chunk_id;

--Cursor define for buy input for get
    CURSOR c_svc_promo_get_ins
   (    i_buylist_group NUMBER,
        i_process_id   NUMBER,
        i_chunk_id     NUMBER
    ) IS
    SELECT
        sget.process_id,
        sget.chunk_id,
        sget.process$status,
        sget.row_seq,
        sget.getlist_group,
        sget.buylist_group,
        sget.skulist,
        sget.item,
        sget.supplier,
        sget.qual_type,
        sget.qual_value,
        sget.change_type,
        sget.change_amount,
        sget.reedem_amount,
        sget.cc_amount,
        sget.cc_start_date,
        sget.cc_end_date,
        sget.new_gp_amount,
        sget.new_gp_percent,
        sget.roq_units,
        sget.roq_inners,
        sget.oi_value,
        hdr.promo_name,
        hdr.location,
        hdr.loc_type,
        hdr.promo_start_date,
        hdr.promo_end_date,
		hdr.reward_type  
    FROM
        crc_svc_multibuy_get_stg   sget,
        crc_svc_multibuy_hdr_stg   hdr
    WHERE
        sget.process_id = hdr.process_id
        AND sget.chunk_id = hdr.chunk_id
        AND sget.buylist_group = i_buylist_group
		AND sget.process_id = i_process_id
        AND sget.chunk_id = i_chunk_id;


    TYPE errors_tab_typ IS
        TABLE OF svc_admin_upld_er%rowtype;
    lp_errors_tab         errors_tab_typ;
    TYPE s9t_errors_tab_typ IS
        TABLE OF s9t_errors%rowtype;
    lp_s9t_errors_tab     s9t_errors_tab_typ;

    TYPE row_seq_tab IS TABLE OF svc_admin_upld_er.row_seq%TYPE INDEX BY BINARY_INTEGER;
    lp_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
    lp_primary_lang       lang.lang%TYPE; 

    TYPE SVC_COST_HEAD IS TABLE OF SVC_COST_SUSP_SUP_HEAD%ROWTYPE;
    LP_REC_SVC_COST_HEAD SVC_COST_HEAD;

    TYPE SVC_COST_DETAIL_LOC IS TABLE OF SVC_COST_SUSP_SUP_DETAIL_LOC%ROWTYPE;
    LP_REC_SVC_COST_DETL_LOC SVC_COST_DETAIL_LOC;

    TYPE SVC_COST_DETAIL IS TABLE OF SVC_COST_SUSP_SUP_DETAIL%ROWTYPE;
    LP_REC_SVC_COST_DETAIL SVC_COST_DETAIL;

    TYPE TAB_SVC_CFA_EXT IS TABLE OF SVC_CFA_EXT%ROWTYPE;
    LP_REC_SVC_CFA_EXT TAB_SVC_CFA_EXT;    
----------------------------------------------------------------------------------

    FUNCTION get_sheet_name_trans 
    (
        i_sheet_name IN VARCHAR2
    ) RETURN VARCHAR2 IS
        l_program VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.GET_SHEET_NAME_TRANS';
    BEGIN
        IF sheet_name_trans.EXISTS(i_sheet_name) THEN
            RETURN sheet_name_trans(i_sheet_name);
        ELSE
            RETURN NULL;
        END IF;
    END get_sheet_name_trans;
--------------------------------------------------------------------------------

    PROCEDURE write_s9t_error (
        i_file_id   IN   s9t_errors.file_id%TYPE,
        i_sheet     IN   VARCHAR2,
        i_row_seq   IN   NUMBER,
        i_col       IN   VARCHAR2,
        i_sqlcode   IN   NUMBER,
        i_sqlerrm   IN   VARCHAR2
    ) IS
    BEGIN
        lp_s9t_errors_tab.extend();
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).file_id := i_file_id;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).error_seq_no := s9t_errors_seq.nextval;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).template_key := template_key;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).wksht_key := i_sheet;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).column_key := i_col;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).row_seq := i_row_seq;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).error_key := ( CASE
            WHEN i_sqlcode IS NULL THEN
                i_sqlerrm
            ELSE 'IIND-ORA-'
                 || lpad(i_sqlcode, 5, '0')
        END );

        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).create_id := get_user;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).create_datetime := sysdate;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).last_update_id := get_user;
        lp_s9t_errors_tab(lp_s9t_errors_tab.count()).last_update_datetime := sysdate;
    END write_s9t_error;
--------------------------------------------------------------------------------

    PROCEDURE write_error (
        i_process_id    IN   svc_admin_upld_er.process_id%TYPE,
        i_error_seq     IN   svc_admin_upld_er.error_seq%TYPE,
        i_chunk_id      IN   svc_admin_upld_er.chunk_id%TYPE,
        i_table_name    IN   svc_admin_upld_er.table_name%TYPE,
        i_row_seq       IN   svc_admin_upld_er.row_seq%TYPE,
        i_column_name   IN   svc_admin_upld_er.column_name%TYPE,
        i_error_msg     IN   svc_admin_upld_er.error_msg%TYPE,
        i_error_type    IN   svc_admin_upld_er.error_type%TYPE DEFAULT 'E'
    ) IS
    BEGIN
        lp_errors_tab.extend();
        lp_errors_tab(lp_errors_tab.count()).process_id := i_process_id;
        lp_errors_tab(lp_errors_tab.count()).error_seq := i_error_seq;
        lp_errors_tab(lp_errors_tab.count()).chunk_id := i_chunk_id;
        lp_errors_tab(lp_errors_tab.count()).table_name := i_table_name;
        lp_errors_tab(lp_errors_tab.count()).row_seq := i_row_seq;
        lp_errors_tab(lp_errors_tab.count()).column_name := i_column_name;
        lp_errors_tab(lp_errors_tab.count()).error_msg := i_error_msg;
        lp_errors_tab(lp_errors_tab.count()).error_type := i_error_type;
    END write_error;

----------------------------------------------------------------------------------------------
    PROCEDURE write_svc_cost_head (
        i_process_id    IN   svc_cost_susp_sup_head.process_id%TYPE,
        i_chunk_id      IN   svc_cost_susp_sup_head.chunk_id%TYPE,
        i_row_seq       IN   svc_cost_susp_sup_head.row_seq%TYPE,
        i_cost_change   IN   svc_cost_susp_sup_head.cost_change%TYPE,
        i_cc_name       IN   svc_cost_susp_sup_head.cost_change_desc%TYPE,
        l_cc_reason     IN   svc_cost_susp_sup_head.reason%TYPE,
        l_active_date   IN   svc_cost_susp_sup_head.active_date%TYPE
    ) IS
    BEGIN
        lp_rec_svc_cost_head.extend();
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).process_id := i_process_id;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).chunk_id := i_chunk_id;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).row_seq := i_row_seq;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).action := 'NEW';
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).process$status := 'N';
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).cost_change := coresvc_costchg_eseq.nextval;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).cost_change_desc := i_cc_name;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).reason := l_cc_reason;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).active_date := l_active_date;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).status := 'S';
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).cost_change_origin := 'SKU';
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).approval_date := NULL;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).approval_id := NULL;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).create_id := get_user;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).create_datetime := sysdate;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).last_upd_id := get_user;
        lp_rec_svc_cost_head(lp_rec_svc_cost_head.count()).last_upd_datetime := sysdate;
    END write_svc_cost_head;----------------------------------------------------------------------------------------------------
    PROCEDURE write_svc_cost_detail (
        i_process_id    IN   svc_cost_susp_sup_detail.process_id%TYPE,
        i_chunk_id      IN   svc_cost_susp_sup_detail.chunk_id%TYPE,
        i_row_seq       IN   svc_cost_susp_sup_detail.row_seq%TYPE,
        i_cost_change   IN   svc_cost_susp_sup_detail.cost_change%TYPE,
        i_supplier      IN   svc_cost_susp_sup_detail.supplier%TYPE,
        i_item          IN   svc_cost_susp_sup_detail.item%TYPE,
        i_amount        IN   svc_cost_susp_sup_detail.cost_change_value%TYPE
    ) IS
    BEGIN
        lp_rec_svc_cost_detail.extend();
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).process_id := i_process_id;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).chunk_id := i_chunk_id;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).row_seq := i_row_seq;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).action := 'NEW';
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).process$status := 'N';
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).cost_change := i_cost_change;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).supplier := i_supplier;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).origin_country_id := 'TH';
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).item := i_item;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).bracket_value1 := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).bracket_uom1 := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).bracket_value2 := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).unit_cost := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).cost_change_type := 'F';
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).cost_change_value := i_amount;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).recalc_ord_ind := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).default_bracket_ind := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).dept := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).sup_dept_seq_no := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).delivery_country_id := NULL;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).create_id := get_user;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).create_datetime := sysdate;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).last_upd_id := get_user;
        lp_rec_svc_cost_detail(lp_rec_svc_cost_detail.count()).last_upd_datetime := sysdate;
    END write_svc_cost_detail;
-------------------------------------------------------------------------------------------------------
    PROCEDURE write_svc_cost_detail_loc (
        i_process_id    IN   svc_cost_susp_sup_detail_loc.process_id%TYPE,
        i_chunk_id      IN   svc_cost_susp_sup_detail_loc.chunk_id%TYPE,
        i_row_seq       IN   svc_cost_susp_sup_detail_loc.row_seq%TYPE,
        i_cost_change   IN   svc_cost_susp_sup_detail_loc.cost_change%TYPE,
        i_supplier      IN   svc_cost_susp_sup_detail_loc.supplier%TYPE,
        i_item          IN   svc_cost_susp_sup_detail_loc.item%TYPE,
        i_loc           IN   svc_cost_susp_sup_detail_loc.loc%TYPE,
        i_amount        IN   svc_cost_susp_sup_detail_loc.cost_change_value%TYPE
    ) IS
    BEGIN
        lp_rec_svc_cost_detl_loc.extend();
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).process_id := i_process_id;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).chunk_id := i_chunk_id;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).row_seq := i_row_seq;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).action := 'NEW';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).process$status := 'N';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).bracket_uom1 := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).bracket_value2 := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).unit_cost := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).cost_change_type := 'F';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).cost_change_value := i_amount;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).recalc_ord_ind := 'N';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).default_bracket_ind := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).dept := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).sup_dept_seq_no := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).delivery_country_id := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).cost_change := i_cost_change;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).supplier := i_supplier;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).origin_country_id := 'TH';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).item := i_item;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).loc_type := 'S';
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).loc := i_loc;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).bracket_value1 := NULL;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).create_id := get_user;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).create_datetime := sysdate;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).last_upd_id := get_user;
        lp_rec_svc_cost_detl_loc(lp_rec_svc_cost_detl_loc.count()).last_upd_datetime := sysdate;
    END write_svc_cost_detail_loc;
-------------------------------------------------------------------------------------------------------
    PROCEDURE write_svc_cfa_ext (
        i_process_id   IN   svc_cost_susp_sup_head_cfa_ext.process_id%TYPE,
        i_chunk_id     IN   svc_cost_susp_sup_head_cfa_ext.chunk_id%TYPE,
        i_row_seq      IN   svc_cost_susp_sup_head_cfa_ext.row_seq%TYPE,
        l_table        IN   svc_cfa_ext.base_rms_table%TYPE,
        i_key_cols     IN   svc_cfa_ext.keys_col%TYPE,
        i_attr_cols    IN   svc_cfa_ext.attrs_col%TYPE
    ) IS
    BEGIN
        lp_rec_svc_cfa_ext.extend();
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).process_id := i_process_id;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).chunk_id := i_chunk_id;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).row_seq := i_row_seq;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).action := 'NEW';
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).base_rms_table := 'COST_SUSP_SUP_HEAD';
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).group_set_view_name := 'V_CRC_CC_CFA_ATTR_GROUP_SET';
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).keys_col := i_key_cols;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).attrs_col := i_attr_cols;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).process$status := 'N';
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).create_id := get_user;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).create_datetime := sysdate;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).last_upd_id := get_user;
        lp_rec_svc_cfa_ext(lp_rec_svc_cfa_ext.count()).last_upd_datetime := sysdate;
    END write_svc_cfa_ext;
--------------------------------------------------------------------------------------------------------
    FUNCTION check_supp_variances (
        o_error_message    IN OUT  rtk_errors.rtk_text%TYPE,
        o_approve          IN OUT  BOOLEAN,
        i_cost_change      IN      svc_cost_susp_sup_detail.cost_change%TYPE,
        i_supplier         IN      svc_cost_susp_sup_detail.supplier%TYPE,
        i_item             IN      svc_cost_susp_sup_detail.item%TYPE,
        i_location         IN      svc_cost_susp_sup_detail_loc.loc%TYPE,
        i_country          IN      svc_cost_susp_sup_detail_loc.origin_country_id%TYPE,
        i_item_level_ind   IN      VARCHAR2
    ) RETURN BOOLEAN IS

        l_program           VARCHAR2(255) := 'CRC_CORESVC_MULTIBUY_PROMO.CHECK_SUPP_VARIANCES';
        l_error             BOOLEAN;
        l_cost_pct_var      sups.cost_chg_pct_var%TYPE;
        l_cost_amt_var      sups.cost_chg_amt_var%TYPE;
        l_exists_amt        VARCHAR2(1);
        l_exists_pct        VARCHAR2(1);
        lp_var_comp_basis   coresvc_costchg_config.variance_comparison_basis%TYPE;
        CURSOR c_sups IS
        SELECT
            nvl(cost_chg_pct_var, 0),
            nvl(cost_chg_amt_var, 0)
        FROM
            sups
        WHERE
            sups.supplier = i_supplier;

        CURSOR c_var_amount_item IS
        SELECT
            'X'
        FROM
            item_supp_country      isc,
            cost_susp_sup_detail   cssd
        WHERE
            cssd.cost_change = i_cost_change
            AND cssd.item = i_item
            AND cssd.supplier = i_supplier
            AND cssd.origin_country_id = i_country
            AND cssd.unit_cost > ( isc.unit_cost + l_cost_amt_var )
            AND cssd.item = isc.item
            AND cssd.supplier = isc.supplier
            AND cssd.origin_country_id = isc.origin_country_id
        UNION ALL
        SELECT
            'X'
        FROM
            item_supp_country      isc,
            cost_susp_sup_detail   cssd
        WHERE
            cssd.cost_change = i_cost_change
            AND cssd.item = i_item
            AND cssd.supplier = i_supplier
            AND cssd.origin_country_id = i_country
            AND cssd.unit_cost < ( isc.unit_cost - l_cost_amt_var )
            AND cssd.item = isc.item
            AND cssd.supplier = isc.supplier
            AND cssd.origin_country_id = isc.origin_country_id;

        CURSOR c_var_pct_item IS
        SELECT
            'X'
        FROM
            item_supp_country      isc,
            cost_susp_sup_detail   cssd
        WHERE
            cssd.cost_change = i_cost_change
            AND cssd.item = i_item
            AND cssd.supplier = i_supplier
            AND cssd.origin_country_id = i_country
            AND cssd.unit_cost > ( isc.unit_cost + ( isc.unit_cost * ( l_cost_pct_var / 100 ) ) )
            AND cssd.item = isc.item
            AND cssd.supplier = isc.supplier
            AND cssd.origin_country_id = isc.origin_country_id
        UNION ALL
        SELECT
            'X'
        FROM
            item_supp_country      isc,
            cost_susp_sup_detail   cssd
        WHERE
            cssd.cost_change = i_cost_change
            AND cssd.item = i_item
            AND cssd.supplier = i_supplier
            AND cssd.origin_country_id = i_country
            AND cssd.unit_cost < ( isc.unit_cost - ( isc.unit_cost * ( l_cost_pct_var / 100 ) ) )
            AND cssd.item = isc.item
            AND cssd.supplier = isc.supplier
            AND cssd.origin_country_id = isc.origin_country_id;

        CURSOR c_var_amount IS
        SELECT
            'X'
        FROM
            item_supp_country_loc      iscl,
            cost_susp_sup_detail_loc   cssdl
        WHERE
            cssdl.cost_change = i_cost_change
            AND cssdl.item = i_item
            AND cssdl.supplier = i_supplier
            AND cssdl.loc = i_location
            AND cssdl.origin_country_id = i_country
            AND cssdl.unit_cost > ( iscl.unit_cost + l_cost_amt_var )
            AND cssdl.item = iscl.item
            AND cssdl.supplier = iscl.supplier
            AND cssdl.origin_country_id = iscl.origin_country_id
            AND cssdl.loc = iscl.loc
        UNION ALL
        SELECT
            'X'
        FROM
            item_supp_country_loc      iscl,
            cost_susp_sup_detail_loc   cssdl
        WHERE
            cssdl.cost_change = i_cost_change
            AND cssdl.item = i_item
            AND cssdl.supplier = i_supplier
            AND cssdl.loc = i_location
            AND cssdl.origin_country_id = i_country
            AND cssdl.unit_cost < ( iscl.unit_cost - l_cost_amt_var )
            AND cssdl.item = iscl.item
            AND cssdl.supplier = iscl.supplier
            AND cssdl.origin_country_id = iscl.origin_country_id
            AND cssdl.loc = iscl.loc;

        CURSOR c_var_pct IS
        SELECT
            'X'
        FROM
            item_supp_country_loc      iscl,
            cost_susp_sup_detail_loc   cssdl
        WHERE
            cssdl.cost_change = i_cost_change
            AND cssdl.item = i_item
            AND cssdl.supplier = i_supplier
            AND cssdl.loc = i_location
            AND cssdl.origin_country_id = i_country
            AND cssdl.unit_cost > ( iscl.unit_cost + ( iscl.unit_cost * ( l_cost_pct_var / 100 ) ) )
            AND cssdl.item = iscl.item
            AND cssdl.supplier = iscl.supplier
            AND cssdl.origin_country_id = iscl.origin_country_id
            AND cssdl.loc = iscl.loc
        UNION ALL
        SELECT
            'X'
        FROM
            item_supp_country_loc      iscl,
            cost_susp_sup_detail_loc   cssdl
        WHERE
            cssdl.cost_change = i_cost_change
            AND cssdl.item = i_item
            AND cssdl.supplier = i_supplier
            AND cssdl.loc = i_location
            AND cssdl.origin_country_id = i_country
            AND cssdl.unit_cost < ( iscl.unit_cost - ( iscl.unit_cost * ( l_cost_pct_var / 100 ) ) )
            AND cssdl.item = iscl.item
            AND cssdl.supplier = iscl.supplier
            AND cssdl.origin_country_id = iscl.origin_country_id
            AND cssdl.loc = iscl.loc;

    BEGIN
   --- Verify that input variables are correctly populated
        IF i_cost_change IS NULL THEN
            o_error_message := sql_lib.create_msg('INVALID_PARM', 'I_cost_change', 'NULL', 'NOT NULL');
            RETURN false;
        END IF;

        IF i_item_level_ind IS NULL THEN
            o_error_message := sql_lib.create_msg('INVALID_PARM', 'I_item_level_ind', 'NULL', 'NOT NULL');
            RETURN false;
        END IF;

        IF i_supplier IS NULL THEN
            o_error_message := sql_lib.create_msg('INVALID_PARM', 'I_supplier', 'NULL', 'NOT NULL');
            RETURN false;
        END IF;

        IF i_item IS NULL THEN
            o_error_message := sql_lib.create_msg('INVALID_PARM', 'I_item', 'NULL', 'NOT NULL');
            RETURN false;
        END IF;

        IF i_item_level_ind = 'N' AND i_location IS NULL THEN
            o_error_message := sql_lib.create_msg('INVALID_PARM', 'I_location', 'NULL', 'NOT NULL');
            RETURN false;
        END IF;

   --- Set the approval flag equal to False.  This function
   --- will set this flag to TRUE if any of the checks pass.

        o_approve := false;

   --- retrieve the supplier costing variances
        OPEN c_sups;
        FETCH c_sups INTO
            l_cost_pct_var,
            l_cost_amt_var;

   --- Check supplier tolerances for an EDI cost change at the item level.
        IF i_item_level_ind = 'Y' THEN

      --- The new cost must fall within both percent and dollar variance
      --- of the old cost if 'B'oth is passed in.
            IF lp_var_comp_basis = 'B' THEN
                OPEN c_var_amount_item;
                FETCH c_var_amount_item INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then check to see if the cost is outside of the percent variance
                IF c_var_amount_item%notfound THEN
                    OPEN c_var_pct_item;
                    FETCH c_var_pct_item INTO l_exists_pct;

            --- If the cost is still not outside of the variance then
            --- set O_approve equal to TRUE
                    IF c_var_pct_item%notfound THEN
                        o_approve := true;
                    END IF;
                    CLOSE c_var_pct_item;
                END IF;

                CLOSE c_var_amount_item;
            END IF;  --- end if  LP_var_comp_basis = 'B'

      --- The new cost must fall within either percent or dollar variance
      --- of the old cost if 'E'ither is passed in.

            IF lp_var_comp_basis = 'E' THEN
                OPEN c_var_amount_item;
                FETCH c_var_amount_item INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
                IF c_var_amount_item%notfound THEN
                    o_approve := true;

         --- If the amount is outside of the supplier dollar variance then
         --- check to see if it is outside the percent variance
                ELSE
                    OPEN c_var_pct_item;
                    FETCH c_var_pct_item INTO l_exists_pct;

            --- If the cost is not outside of the variance then
            --- set O_approve equal to TRUE
                    IF c_var_pct_item%notfound THEN
                        o_approve := true;
                    END IF;
                    CLOSE c_var_pct_item;
                END IF;

                CLOSE c_var_amount_item;
            END IF;  --- end if LP_var_comp_basis = 'E'

      --- The new cost must fall within dollar variance of the old cost
      --- if 'D'ollar is passed in.

            IF lp_var_comp_basis = 'D' THEN
                OPEN c_var_amount_item;
                FETCH c_var_amount_item INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
                IF c_var_amount_item%notfound THEN
                    o_approve := true;
                END IF;
                CLOSE c_var_amount_item;
            END IF;  --- end if LP_var_comp_basis = 'D'

      --- The new cost must fall within percent variance of the old cost
      --- if 'P'ercent is passed in.

            IF lp_var_comp_basis = 'P' THEN
                OPEN c_var_pct_item;
                FETCH c_var_pct_item INTO l_exists_pct;

         --- If the cost is not outside of the variance then
         --- set O_approve equal to TRUE
                IF c_var_pct_item%notfound THEN
                    o_approve := true;
                END IF;
                CLOSE c_var_pct_item;
            END IF;  --- end if LP_var_comp_basis = 'P'

        ELSE --- If the cost change is at the item/bracket, item/location, or item/location/bracket

      -- Find if the costs for the bracket or location are out of the tolerance limits.
      -- If they are, set O_approve to False.  If no records are found out of tolerance ranges, then
      -- set O_approve True.
            o_approve := false;

      --- The new cost must fall within both percent and dollar variance
      --- of the old cost if 'B'oth is passed in.
            IF lp_var_comp_basis = 'B' THEN
                OPEN c_var_amount;
                FETCH c_var_amount INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then check to see if the cost is outside of the percent variance
                IF c_var_amount%notfound THEN
                    OPEN c_var_pct;
                    FETCH c_var_pct INTO l_exists_pct;

            --- If the cost is still not outside of the variance then
            --- set O_approve equal to TRUE
                    IF c_var_pct%notfound THEN
                        o_approve := true;
                    END IF;
                    CLOSE c_var_pct;
                END IF;

                CLOSE c_var_amount;
            END IF;  --- end if  LP_var_comp_basis = 'B'

      --- The new cost must fall within either percent or dollar variance
      --- of the old cost if 'E'ither is passed in.

            IF lp_var_comp_basis = 'E' THEN
                OPEN c_var_amount;
                FETCH c_var_amount INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
                IF c_var_amount%notfound THEN
                    o_approve := true;

         --- If the amount is outside of the supplier dollar variance then
         --- check to see if it is outside the percent variance
                ELSE
                    OPEN c_var_pct;
                    FETCH c_var_pct INTO l_exists_pct;

            --- If the cost is not outside of the variance then
            --- set O_approve equal to TRUE
                    IF c_var_pct%notfound THEN
                        o_approve := true;
                    END IF;
                    CLOSE c_var_pct;
                END IF;

                CLOSE c_var_amount;
            END IF;  --- end if LP_var_comp_basis = 'E'

      --- The new cost must fall within dollar variance of the old cost
      --- if 'D'ollar is passed in.

            IF lp_var_comp_basis = 'D' THEN
                OPEN c_var_amount;
                FETCH c_var_amount INTO l_exists_amt;

         --- If the cost is not outside of the amount variance
         --- then update O_approve to TRUE
                IF c_var_amount%notfound THEN
                    o_approve := true;
                END IF;
                CLOSE c_var_amount;
            END IF;  --- end if LP_var_comp_basis = 'D'

      --- The new cost must fall within percent variance of the old cost
      --- if 'P'ercent is passed in.

            IF lp_var_comp_basis = 'P' THEN
                OPEN c_var_pct;
                FETCH c_var_pct INTO l_exists_pct;

         --- If the cost is not outside of the variance then
         --- set O_approve equal to TRUE
                IF c_var_pct%notfound THEN
                    o_approve := true;
                END IF;
                CLOSE c_var_pct;
            END IF;  --- end if LP_var_comp_basis = 'P'

        END IF;

        CLOSE c_sups;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END check_supp_variances;
---------------------------------------------------------------------------------------------------  
    FUNCTION create_deals_buy (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      NUMBER,
        i_chunk_id        IN      NUMBER
    ) RETURN BOOLEAN IS

        l_program                VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_DEALS_BUY';
        l_table                  svc_admin_upld_er.table_name%TYPE := 'SVC_COST_SUSP_SUP_HEAD';
        l_vendor_funded_ind      NUMBER(2) := NULL;
        l_contribution_percent   NUMBER(20) := NULL;
        l_stg_simplepromo_id     NUMBER(15) := 0;
        promo_temp_rec           c_svc_promo_buy%rowtype;
        l_error_message          rtk_errors.rtk_text%TYPE;
        l_return_code            BOOLEAN;
        l_deal_ids_table         deal_ids_tbl := deal_ids_tbl();
        deals_head_temp_rec      deal_head_rec;
        l_deal_detail_tbl        deal_detail_tbl := deal_detail_tbl();
        l_deal_itemloc_tbl       deal_itemloc_tbl := deal_itemloc_tbl();
        l_deal_comp_prom_table   deal_comp_prom_tbl := deal_comp_prom_tbl();
        o_deal_id                NUMBER(10);
        o_deal_detail_id         NUMBER(10);
        l_promo_id               NUMBER(10);
        l_promo_comp_id          NUMBER(10);
        l_supp_parent            NUMBER(10);
        l_deal_exists            NUMBER(10);
        l_deal                   NUMBER(10);
        l_deal_detail_id         NUMBER(10);
        l_code                   BOOLEAN;
    BEGIN
        IF lp_errors_tab.count() = 0 THEN
            OPEN c_svc_promo_buy(i_process_id, i_chunk_id);
            LOOP
                FETCH c_svc_promo_buy INTO promo_temp_rec;
                EXIT WHEN c_svc_promo_buy%notfound;
                IF promo_temp_rec.reedem_amount IS NOT NULL THEN
                    l_deal_detail_tbl := NEW deal_detail_tbl();
                    l_deal_itemloc_tbl := NEW deal_itemloc_tbl();
                    l_deal_comp_prom_table := NEW deal_comp_prom_tbl();
                    SELECT DISTINCT
                        promo_id,
                        promo_comp_id
                    INTO
                        l_promo_id,
                        l_promo_comp_id
                    FROM
                        rpm_stage_multibuy_header
                    WHERE
                        comments = to_char(i_process_id);

                    SELECT
                        supplier_parent
                    INTO l_supp_parent
                    FROM
                        sups
                    WHERE
                        supplier = promo_temp_rec.supplier;

                    SELECT
                        COUNT(1)
                    INTO l_deal_exists
                    FROM
                        deal_comp_prom   dcp,
                        deal_head        dh
                    WHERE
                        dcp.deal_id = dh.deal_id
                        AND dh.supplier = l_supp_parent
                        AND dcp.promotion_id = l_promo_id
                        AND dcp.promo_comp_id = l_promo_comp_id;

                    IF l_deal_exists = 0 THEN
                        l_deal_comp_prom_table.extend();
                        l_deal_comp_prom_table(l_deal_comp_prom_table.count()) := deal_comp_prom_rec(l_promo_id, l_promo_comp_id,
                        100);

                        l_deal_itemloc_tbl.extend();
                        l_deal_itemloc_tbl(l_deal_itemloc_tbl.count()) := deal_itemloc_rec(promo_temp_rec.location, NULL, NULL, NULL
                        , NULL,
                 'TH', 'Y');

                        l_deal_detail_tbl.extend();
                        l_deal_detail_tbl(l_deal_detail_tbl.count()) := deal_detail_rec(NULL, NULL, promo_temp_rec.promo_start_date
                        , promo_temp_rec.promo_end_date, 100,
                promo_temp_rec.create_id, l_deal_itemloc_tbl, l_deal_comp_prom_table);

                        deals_head_temp_rec := deal_head_rec('S', NULL, l_supp_parent, 'THB', promo_temp_rec.promo_start_date,
              promo_temp_rec.promo_end_date, 'M', 'M', 'D', 'AA',
              'Y', 'N', promo_temp_rec.create_id, l_deal_detail_tbl);

                        crc_pm_deals_api_sql.create_deal(o_error_message, l_return_code, l_deal_ids_table, deals_head_temp_rec);
                        IF NOT l_return_code THEN
                            ROLLBACK;
                            write_error(i_process_id, svc_admin_upld_er_seq.nextval, promo_temp_rec.chunk_id, l_table, promo_temp_rec
                            .row_seq,
                                        l_program, l_error_message);

                        ELSE
                            IF l_deal_ids_table.count > 0 THEN
                                o_deal_id := l_deal_ids_table(1).deal_id;
                                o_deal_detail_id := l_deal_ids_table(1).deal_detail_id;
                            ELSE
                                o_deal_id := NULL;
                                o_deal_detail_id := NULL;
                            END IF;
                        END IF;

                        l_deal_detail_tbl.DELETE();
                        l_deal_itemloc_tbl.DELETE();
                        l_deal_comp_prom_table.DELETE();
                    END IF;

                END IF;

            END LOOP;

            CLOSE c_svc_promo_buy;
        END IF;

        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END create_deals_buy;
----------------------------------------------------------------------------------------------------
    FUNCTION create_deals_get (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      NUMBER,
        i_chunk_id        IN      NUMBER
    ) RETURN BOOLEAN IS

        l_program                VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_DEALS_GET';
        l_table                  svc_admin_upld_er.table_name%TYPE := 'DEAL_HEAD';
        l_vendor_funded_ind      NUMBER(2) := NULL;
        l_contribution_percent   NUMBER(20) := NULL;
        l_stg_simplepromo_id     NUMBER(15) := 0;
        promo_temp_rec           c_svc_promo_get%rowtype;
        l_error_message          rtk_errors.rtk_text%TYPE;
        l_return_code            BOOLEAN;
        l_deal_ids_table         deal_ids_tbl := deal_ids_tbl();
        deals_head_temp_rec      deal_head_rec;
        l_deal_detail_tbl        deal_detail_tbl := deal_detail_tbl();
        l_deal_itemloc_tbl       deal_itemloc_tbl := deal_itemloc_tbl();
        l_deal_comp_prom_table   deal_comp_prom_tbl := deal_comp_prom_tbl();
        o_deal_id                NUMBER(10);
        o_deal_detail_id         NUMBER(10);
        l_promo_id               NUMBER(10);
        l_promo_comp_id          NUMBER(10);
        l_supp_parent            NUMBER(10);
        l_deal_exists            NUMBER(10);
        l_deal                   NUMBER(10);
        l_deal_detail_id         NUMBER(10);
-- l_error_count             number(100);
        l_code                   BOOLEAN;
    BEGIN
        IF lp_errors_tab.count() = 0 THEN
            OPEN c_svc_promo_get(i_process_id, i_chunk_id);
            LOOP
                FETCH c_svc_promo_get INTO promo_temp_rec;
                EXIT WHEN c_svc_promo_get%notfound;
                IF promo_temp_rec.reedem_amount IS NOT NULL THEN
                    l_deal_detail_tbl := NEW deal_detail_tbl();
                    l_deal_itemloc_tbl := NEW deal_itemloc_tbl();
                    l_deal_comp_prom_table := NEW deal_comp_prom_tbl();
                    SELECT DISTINCT
                        promo_id,
                        promo_comp_id
                    INTO
                        l_promo_id,
                        l_promo_comp_id
                    FROM
                        rpm_stage_multibuy_header
                    WHERE
                        comments = to_char(i_process_id);

                    SELECT
                        supplier_parent
                    INTO l_supp_parent
                    FROM
                        sups
                    WHERE
                        supplier = promo_temp_rec.supplier;

                    SELECT
                        COUNT(1)
                    INTO l_deal_exists
                    FROM
                        deal_comp_prom   dcp,
                        deal_head        dh
                    WHERE
                        dcp.deal_id = dh.deal_id
                        AND dh.supplier = l_supp_parent
                        AND dcp.promotion_id = l_promo_id
                        AND dcp.promo_comp_id = l_promo_comp_id;

                    IF l_deal_exists = 0 THEN
                        l_deal_comp_prom_table.extend();
                        l_deal_comp_prom_table(l_deal_comp_prom_table.count()) := deal_comp_prom_rec(l_promo_id, l_promo_comp_id,
                        100);

                        l_deal_itemloc_tbl.extend();
                        l_deal_itemloc_tbl(l_deal_itemloc_tbl.count()) := deal_itemloc_rec(promo_temp_rec.location, NULL, NULL, NULL
                        , NULL,
                 'TH', 'Y');

                        l_deal_detail_tbl.extend();
                        l_deal_detail_tbl(l_deal_detail_tbl.count()) := deal_detail_rec(NULL, NULL, promo_temp_rec.promo_start_date
                        , promo_temp_rec.promo_end_date, 100,
                promo_temp_rec.create_id, l_deal_itemloc_tbl, l_deal_comp_prom_table);

                        deals_head_temp_rec := deal_head_rec('S', NULL, l_supp_parent, 'THB', promo_temp_rec.promo_start_date,
              promo_temp_rec.promo_end_date, 'M', 'M', 'D', 'AA',
              'Y', 'N', promo_temp_rec.create_id, l_deal_detail_tbl);

                        crc_pm_deals_api_sql.create_deal(o_error_message, l_return_code, l_deal_ids_table, deals_head_temp_rec);
                        IF NOT l_return_code THEN
                            ROLLBACK;
                            write_error(i_process_id, svc_admin_upld_er_seq.nextval, promo_temp_rec.chunk_id, l_table, promo_temp_rec
                            .row_seq,
                                        l_program, l_error_message);

                        ELSE
                            IF l_deal_ids_table.count > 0 THEN
                                o_deal_id := l_deal_ids_table(1).deal_id;
                                o_deal_detail_id := l_deal_ids_table(1).deal_detail_id;
                            ELSE
                                o_deal_id := NULL;
                                o_deal_detail_id := NULL;
                            END IF;
                        END IF;

                        l_deal_detail_tbl.DELETE();
                        l_deal_itemloc_tbl.DELETE();
                        l_deal_comp_prom_table.DELETE();
                    END IF;

                END IF;

            END LOOP;

            CLOSE c_svc_promo_get;
        END IF;

        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END create_deals_get;
---------------------------------------------------------------------------------------------------
FUNCTION create_fixed_deals_buy(
    o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
    i_process_id      IN      NUMBER,
    i_chunk_id        IN      NUMBER
) RETURN BOOLEAN IS

    l_program            VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_FIXED_DEALS_BUY';
    l_table              svc_admin_upld_er.table_name%TYPE := 'FIXED_DEAL';
    l_error_message      rtk_errors.rtk_text%TYPE;
    l_return_code        BOOLEAN;
    deals_rec_temp_rec   crc_fdeal_ext_sql.deals_rec;
    l_uda_value          NUMBER(10) := NULL;
    l_deal_no            fixed_deal.deal_no%TYPE;
    l_udaexists          NUMBER;
    l_promoid            NUMBER(10);
    l_deal_exists        NUMBER(1);
    i_buylist_group      NUMBER(15); 
    promo_temp_rec  c_svc_promo_buy%rowtype;

    CURSOR c_get_deal_no IS
    SELECT
        deal_sequence.NEXTVAL
    FROM
        dual;

BEGIN
 if  LP_errors_tab.COUNT()=0  then

   OPEN c_svc_promo_buy(i_process_id, i_chunk_id);
    LOOP
        FETCH c_svc_promo_buy INTO promo_temp_rec;
        EXIT WHEN c_svc_promo_buy%notfound;
        IF promo_temp_rec.oi_value IS NOT NULL THEN    
                --Get created PromoId
            SELECT DISTINCT
                promo_id
            INTO l_promoid
            FROM
                rpm_stage_multibuy_header
            WHERE
                comments = to_char(promo_temp_rec.process_id);

            SELECT
                COUNT(1)
            INTO l_deal_exists
            FROM
                fixed_deal
            WHERE
                promotion = l_promoid
                AND supplier = promo_temp_rec.supplier;

            IF l_deal_exists = 0 THEN
                deals_rec_temp_rec.supplier := promo_temp_rec.supplier;
                deals_rec_temp_rec.deal_desc := promo_temp_rec.promo_name;
                deals_rec_temp_rec.deal_type := 21;
                deals_rec_temp_rec.fixed_deal_amt := promo_temp_rec.oi_value;
                deals_rec_temp_rec.status := 'I';
                deals_rec_temp_rec.collect_by := 'D';
                deals_rec_temp_rec.collect_start_date := promo_temp_rec.promo_start_date;
                deals_rec_temp_rec.collect_end_date := promo_temp_rec.promo_start_date;
                deals_rec_temp_rec.collect_periods := 1;
                deals_rec_temp_rec.promotion := l_promoid;
                deals_rec_temp_rec.comments := NULL;
                deals_rec_temp_rec.ext_deal_no := NULL;
                deals_rec_temp_rec.merch_ind := 'N';
                deals_rec_temp_rec.partner_type := 'S';
                deals_rec_temp_rec.partner_id := NULL;
                deals_rec_temp_rec.vat_ind := 'N';
                deals_rec_temp_rec.invoice_processing_logic := 'AA';
                deals_rec_temp_rec.deb_cred_ind := 'D';
                deals_rec_temp_rec.non_merch_code := 'M';
                deals_rec_temp_rec.currency_code := 'THB';
                deals_rec_temp_rec.vat_rate := null;
                deals_rec_temp_rec.VAT_CODE :=null;               

                --GET Value for VAT checking in UDA  
                SELECT
                    COUNT(1)
                INTO l_udaexists
                FROM
                    uda_item_lov uv
                WHERE
                    uv.uda_id = 7
                    AND uv.item = promo_temp_rec.item;


                IF l_udaexists > 0 THEN
                    SELECT
                        uv.uda_value
                    INTO l_uda_value
                    FROM
                        uda_item_lov uv
                    WHERE
                        uv.uda_id = 7
                        AND uv.item = promo_temp_rec.item;


                    sql_lib.set_mark('OPEN', 'C_get_deal_no', 'DUAL', NULL);
                    OPEN c_get_deal_no;
                    sql_lib.set_mark('FETCH', 'C_get_deal_no', 'DUAL', NULL);
                    FETCH c_get_deal_no INTO l_deal_no;
                    sql_lib.set_mark('CLOSE', 'C_get_deal_no', 'DUAL', NULL);
                    CLOSE c_get_deal_no;


                    IF crc_fdeal_ext_sql.crc_insert_header(l_error_message, deals_rec_temp_rec, l_deal_no) = false THEN
                       WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          promo_temp_rec.chunk_id,
                          L_table,
                          promo_temp_rec.row_seq,
                          l_program,
                          l_error_message); 
                    END IF;

                ELSE
                    o_error_message := sql_lib.create_msg('ITEM_NOT_HAVE_UDA_7', NULL, NULL, NULL);

                    WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      promo_temp_rec.chunk_id,
                      L_table,
                      promo_temp_rec.row_seq,
                      l_program,
                      O_error_message);   
                END IF;

            END IF;

        END IF;

    END LOOP;

    CLOSE c_svc_promo_buy;
    end if;
  RETURN true;
EXCEPTION
    WHEN OTHERS THEN
        o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));
     WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     promo_temp_rec.chunk_id,
                     L_table,
                     promo_temp_rec.row_seq,
                     l_program,
                     O_error_message);

     RETURN false;
END create_fixed_deals_buy;

------------------------------------------------------------------------------------------------------------------------
FUNCTION create_fixed_deals_get (
    o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
    i_process_id      IN      NUMBER,
    i_chunk_id        IN      NUMBER
) RETURN BOOLEAN IS

    l_program            VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_FIXED_DEALS_GET';
    l_table              svc_admin_upld_er.table_name%TYPE := 'FIXED_DEAL';
    l_error_message      rtk_errors.rtk_text%TYPE;
    l_return_code        BOOLEAN;
    deals_rec_temp_rec   crc_fdeal_ext_sql.deals_rec;
    l_uda_value          NUMBER(2) := NULL;
    l_deal_no            fixed_deal.deal_no%TYPE;
    l_udaexists          NUMBER;
    l_promoid            NUMBER(10);
    l_deal_exists        NUMBER(1);
    i_buylist_group               NUMBER(15); 

    CURSOR c_get_deal_no IS
    SELECT
        deal_sequence.NEXTVAL
    FROM
        dual;

    promo_temp_rec       c_svc_promo_get%rowtype;
BEGIN
  if  LP_errors_tab.COUNT()=0  then
    OPEN c_svc_promo_get(i_process_id, i_chunk_id);
    LOOP
        FETCH c_svc_promo_get INTO promo_temp_rec;
        EXIT WHEN c_svc_promo_get%notfound;
        IF promo_temp_rec.oi_value IS NOT NULL THEN    
                --Get created PromoId
            SELECT DISTINCT
                promo_id
            INTO l_promoid
            FROM
                rpm_stage_multibuy_header
            WHERE
                comments = to_char(promo_temp_rec.process_id);

            SELECT
                COUNT(1)
            INTO l_deal_exists
            FROM
                fixed_deal
            WHERE
                promotion = l_promoid
                AND supplier = promo_temp_rec.supplier;

            IF l_deal_exists = 0 THEN
                deals_rec_temp_rec.supplier := promo_temp_rec.supplier;
                deals_rec_temp_rec.deal_desc := promo_temp_rec.promo_name;
                deals_rec_temp_rec.deal_type := 21;
                deals_rec_temp_rec.fixed_deal_amt := promo_temp_rec.oi_value;
                deals_rec_temp_rec.status := 'I';
                deals_rec_temp_rec.collect_by := 'D';
                deals_rec_temp_rec.collect_start_date := promo_temp_rec.promo_start_date;
                deals_rec_temp_rec.collect_end_date := promo_temp_rec.promo_start_date;
                deals_rec_temp_rec.collect_periods := 1;
                deals_rec_temp_rec.promotion := l_promoid;
                deals_rec_temp_rec.comments := NULL;
                deals_rec_temp_rec.ext_deal_no := NULL;
                deals_rec_temp_rec.merch_ind := 'N';
                deals_rec_temp_rec.partner_type := 'S';
                deals_rec_temp_rec.partner_id := NULL;
                deals_rec_temp_rec.vat_ind := 'N';
                deals_rec_temp_rec.invoice_processing_logic := 'AA';
                deals_rec_temp_rec.deb_cred_ind := 'D';
                deals_rec_temp_rec.non_merch_code := 'M';
                deals_rec_temp_rec.currency_code := 'THB';
                deals_rec_temp_rec.vat_rate := null;
                deals_rec_temp_rec.VAT_CODE :=null;

                --GET Value for VAT checking in UDA  
                SELECT
                    COUNT(1)
                INTO l_udaexists
                FROM
                    uda_item_lov uv
                WHERE
                    uv.uda_id = 7
                    AND uv.item = promo_temp_rec.item;

                IF l_udaexists > 0 THEN
                    SELECT
                        uv.uda_value
                    INTO l_uda_value
                    FROM
                        uda_item_lov uv
                    WHERE
                        uv.uda_id = 7
                        AND uv.item = promo_temp_rec.item;


                    sql_lib.set_mark('OPEN', 'C_get_deal_no', 'DUAL', NULL);
                    OPEN c_get_deal_no;
                    sql_lib.set_mark('FETCH', 'C_get_deal_no', 'DUAL', NULL);
                    FETCH c_get_deal_no INTO l_deal_no;
                    sql_lib.set_mark('CLOSE', 'C_get_deal_no', 'DUAL', NULL);
                    CLOSE c_get_deal_no;

                    IF crc_fdeal_ext_sql.crc_insert_header(l_error_message, deals_rec_temp_rec, l_deal_no) = false THEN
                        WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          promo_temp_rec.chunk_id,
                          L_table,
                          promo_temp_rec.row_seq,
                          l_program,
                          l_error_message); 
                    END IF;

                ELSE
                    o_error_message := sql_lib.create_msg('ITEM_NOT_HAVE_UDA_7', NULL, NULL, NULL);

                    WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      promo_temp_rec.chunk_id,
                      L_table,
                      promo_temp_rec.row_seq,
                      l_program,
                      O_error_message);   
                END IF;

            END IF;

        END IF;

    END LOOP;

    CLOSE c_svc_promo_get;
 end if;
    RETURN true;
EXCEPTION
    WHEN OTHERS THEN
        o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));
      WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     promo_temp_rec.chunk_id,
                     L_table,
                     promo_temp_rec.row_seq,
                     l_program,
                     O_error_message);

        RETURN false;
END create_fixed_deals_get;

----------------------------------------------------------------------------------------------------
    PROCEDURE process_s9t_hdr (
        i_file_id      IN   s9t_folder.file_id%TYPE,
        i_process_id   IN   svc_brand.process_id%TYPE
    ) IS

        TYPE svc_hdr_col_typ IS
            TABLE OF crc_svc_multibuy_hdr_stg%rowtype;
        l_temp_rec      crc_svc_multibuy_hdr_stg%rowtype;
        svc_hdr_col     svc_hdr_col_typ := NEW svc_hdr_col_typ();
        l_process_id    crc_svc_multibuy_hdr_stg.process_id%TYPE;
        l_error         BOOLEAN := false;
        l_default_rec   crc_svc_multibuy_hdr_stg%rowtype;
        dml_errors EXCEPTION;
        PRAGMA exception_init ( dml_errors, -24381 );
        l_table         VARCHAR2(30) := 'CRC_SVC_MULTIBUY_HDR_STG';
        l_pk_columns    VARCHAR2(255) := 'Promo_name';
        l_error_code    NUMBER;
        l_error_msg     rtk_errors.rtk_text%TYPE;
    BEGIN
        FOR rec IN (
            SELECT
                upper(r.get_cell(multibuyhdr$promo_name)) AS promo_name,
                upper(r.get_cell(multibuyhdr$promo_type)) AS promo_type,
                upper(r.get_cell(multibuyhdr$promo_start_date)) AS promo_start_date,
                upper(r.get_cell(multibuyhdr$promo_end_date)) AS promo_end_date,
                upper(r.get_cell(multibuyhdr$mechanic)) AS mechanic,
                upper(r.get_cell(multibuyhdr$forecast_group)) AS forecast_group,
                upper(r.get_cell(multibuyhdr$cust_segment)) AS cust_segment,
                upper(r.get_cell(multibuyhdr$right_earn_ind)) AS right_earn_ind,
                upper(r.get_cell(multibuyhdr$right_spend_ind)) AS right_spend_ind,
                upper(r.get_cell(multibuyhdr$sticker_earn_ind)) AS sticker_earn_ind,
                upper(r.get_cell(multibuyhdr$sticker_spend_ind)) AS sticker_spend_ind,
                upper(r.get_cell(multibuyhdr$coupon_spend_ind)) AS coupon_spend_ind,
                upper(r.get_cell(multibuyhdr$superburn_points)) AS superburn_points,
                upper(r.get_cell(multibuyhdr$extra_points)) AS extra_points,
                upper(r.get_cell(multibuyhdr$loc_type)) AS loc_type,
                upper(r.get_cell(multibuyhdr$location)) AS location,
                upper(r.get_cell(multibuyhdr$reward_type)) AS reward_type,
                upper(r.get_cell(multibuyhdr$legacy_promo_id)) AS legacy_promo_id,
                r.get_row_seq() AS row_seq
            FROM
                s9t_folder                      sf,
                TABLE ( sf.s9t_file_obj.sheets ) ss,
                TABLE ( ss.s9t_rows )           r
            WHERE
                sf.file_id = i_file_id
                AND ss.sheet_name = sheet_name_trans(multibuyhdr_sheet)
        ) LOOP
            l_temp_rec := NULL;
            l_temp_rec.process_id := i_process_id;
            l_temp_rec.chunk_id := 1;
            l_temp_rec.row_seq := rec.row_seq;
            l_temp_rec.process$status := 'N';
            l_temp_rec.create_id := get_user;
            l_temp_rec.last_upd_id := get_user;
            l_temp_rec.create_datetime := sysdate;
            l_temp_rec.last_upd_datetime := sysdate;
            l_error := false;
            BEGIN
                 if rec.promo_name is not null 
                     then
                        l_temp_rec.promo_name := rec.promo_name;
                 else       
                            WRITE_S9T_ERROR(i_file_id, 
                                            multibuyhdr_sheet, 
                                            rec.row_seq, 
                                            'PROMO_NAME', 
                                            NULL,
                                            'S9T_PK_COL_REQD');
                            l_error := true;
                end if;            
              EXCEPTION
                  when OTHERS 
                   then
                       WRITE_S9T_ERROR(I_file_id,
                                       multibuyhdr_sheet,
                                       rec.row_seq,
                                      'PROMO_NAME',
                                       SQLCODE,
                                        SQLERRM);
                       L_error := TRUE;
            END;

            BEGIN
                if rec.promo_type is not null 
                 then                
                    l_temp_rec.promo_type := rec.promo_type;
                else       
                            WRITE_S9T_ERROR(i_file_id, 
                                            multibuyhdr_sheet, 
                                            rec.row_seq, 
                                            'PROMO_TYPE', 
                                            NULL,
                                            'S9T_PK_COL_REQD');
                            l_error := true;
                end if;      
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'PROMO_TYPE', 
                                    sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.promo_start_date is not null 
                Then
                    l_temp_rec.promo_start_date := rec.promo_start_date;
                else       
                            WRITE_S9T_ERROR(i_file_id, 
                                            multibuyhdr_sheet, 
                                            rec.row_seq, 
                                            'PROMO_START_DATE', 
                                            NULL,
                                            'S9T_PK_COL_REQD');
                            l_error := true;
                end if;                  
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'PROMO_START_DATE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.promo_end_date is not null 
                then 
                l_temp_rec.promo_end_date := rec.promo_end_date;
                else       
                    WRITE_S9T_ERROR(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'PROMO_END_DATE', 
                                    NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                end if;                                   
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'PROMO_END_DATE', 
                                    sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.mechanic is not null
                then
                l_temp_rec.mechanic := rec.mechanic;
                else       
                    WRITE_S9T_ERROR(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'MECHANIC', 
                                    NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                end if;                 
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'MECHANIC', 
                                    sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.forecast_group is not null
                then 
                l_temp_rec.forecast_group := rec.forecast_group;
                else       
                    WRITE_S9T_ERROR(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'FORECAST_GROUP', 
                                    NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                end if;                 
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id,
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'FORECAST_GROUP', 
                                    sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.cust_segment := rec.cust_segment;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'CUST_SEGMENT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.right_earn_ind := rec.right_earn_ind;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'RIGHT_EARN_IND', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.right_spend_ind := rec.right_spend_ind;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'RIGHT_SPEND_IND', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.sticker_earn_ind := rec.sticker_earn_ind;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'STICKER_EARN_IND', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.sticker_spend_ind := rec.sticker_spend_ind;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'STICKER_SPEND_IND', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END; 
--

            BEGIN
                l_temp_rec.coupon_spend_ind := rec.coupon_spend_ind;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'COUPON_SPEND_IND', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.superburn_points := rec.superburn_points;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'SUPERBURN_POINTS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;      
--

            BEGIN
                l_temp_rec.extra_points := rec.extra_points;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'EXTRA_POINTS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.loc_type is not null 
                then
                l_temp_rec.loc_type := rec.loc_type;
                else       
                    WRITE_S9T_ERROR(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'LOC_TYPE', 
                                    NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                end if;                 
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, 
                                    multibuyhdr_sheet, 
                                    rec.row_seq, 
                                    'LOC_TYPE', 
                                    sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.location := rec.location;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'LOCATION', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.reward_type := rec.reward_type;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'REWARD_TYPE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.legacy_promo_id := rec.legacy_promo_id;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyhdr_sheet, rec.row_seq, 'LEGACY_PROMO_ID', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            IF NOT l_error THEN
                svc_hdr_col.extend();
                svc_hdr_col(svc_hdr_col.count()) := l_temp_rec;
            END IF;

        END LOOP;

        BEGIN
            FORALL i IN 1..svc_hdr_col.count SAVE EXCEPTIONS
                INSERT INTO crc_svc_multibuy_hdr_stg (
                    process_id,
                    chunk_id,
                    row_seq,
                    process$status,
                    promo_name,
                    promo_type,
                    promo_start_date,
                    promo_end_date,
                    mechanic,
                    forecast_group,
                    cust_segment,
                    right_earn_ind,
                    right_spend_ind,
                    sticker_earn_ind,
                    sticker_spend_ind,
                    coupon_spend_ind,
                    superburn_points,
                    extra_points,
                    loc_type,
                    location,
                    reward_type,
                    legacy_promo_id,
                    create_id,
                    create_datetime,
                    last_upd_id,
                    last_upd_datetime
                ) VALUES (
                    svc_hdr_col(i).process_id,
                    svc_hdr_col(i).chunk_id,
                    svc_hdr_col(i).row_seq,
                    svc_hdr_col(i).process$status,
                    svc_hdr_col(i).promo_name,
                    svc_hdr_col(i).promo_type,
                    svc_hdr_col(i).promo_start_date,
                    svc_hdr_col(i).promo_end_date,
                    svc_hdr_col(i).mechanic,
                    svc_hdr_col(i).forecast_group,
                    svc_hdr_col(i).cust_segment,
                    svc_hdr_col(i).right_earn_ind,
                    svc_hdr_col(i).right_spend_ind,
                    svc_hdr_col(i).sticker_earn_ind,
                    svc_hdr_col(i).sticker_spend_ind,
                    svc_hdr_col(i).coupon_spend_ind,
                    svc_hdr_col(i).superburn_points,
                    svc_hdr_col(i).extra_points,
                    svc_hdr_col(i).loc_type,
                    svc_hdr_col(i).location,
                    svc_hdr_col(i).reward_type,
                    svc_hdr_col(i).legacy_promo_id,
                    svc_hdr_col(i).create_id,
                    svc_hdr_col(i).create_datetime,
                    svc_hdr_col(i).last_upd_id,
                    svc_hdr_col(i).last_upd_datetime
                );

        EXCEPTION
            WHEN dml_errors THEN
                FOR i IN 1..SQL%bulk_exceptions.count LOOP
                    l_error_code := SQL%bulk_exceptions(i).error_code;
                    IF l_error_code = 1 THEN
                        l_error_code := NULL;
                        l_error_msg := sql_lib.create_msg('DUP_REC_EXISTS_S9T', l_pk_columns);
                    END IF;

                    write_s9t_error(i_file_id, multibuyhdr_sheet, svc_hdr_col(SQL%bulk_exceptions(i).error_index).row_seq, NULL,
                    l_error_code,
                                    l_error_msg);

                END LOOP;
        END;

    END process_s9t_hdr;
-----------------------------------------------------------------------------

    PROCEDURE process_s9t_buy (
        i_file_id      IN   s9t_folder.file_id%TYPE,
        i_process_id   IN   svc_brand.process_id%TYPE
    ) IS

        TYPE svc_buy_col_typ IS
            TABLE OF crc_svc_multibuy_buy_stg%rowtype;
        l_temp_rec      crc_svc_multibuy_buy_stg%rowtype;
        svc_buy_col     svc_buy_col_typ := NEW svc_buy_col_typ();
        l_process_id    crc_svc_multibuy_buy_stg.process_id%TYPE;
        l_error         BOOLEAN := false;
        l_default_rec   crc_svc_multibuy_buy_stg%rowtype;
        dml_errors EXCEPTION;
        PRAGMA exception_init ( dml_errors, -24381 );
        l_table         VARCHAR2(30) := 'CRC_SVC_MULTIBUY_BUY_STG';
        l_pk_columns    VARCHAR2(255) := 'buylist_group';
        l_error_code    NUMBER;
        l_error_msg     rtk_errors.rtk_text%TYPE;
    BEGIN
        FOR rec IN (
            SELECT
                upper(r.get_cell(multibuybuy$buylist_group)) AS buylist_group,
                upper(r.get_cell(multibuybuy$skulist)) AS skulist,
                upper(r.get_cell(multibuybuy$item)) AS item,
                upper(r.get_cell(multibuybuy$supplier)) AS supplier,
                upper(r.get_cell(multibuybuy$qual_type)) AS qual_type,
                upper(r.get_cell(multibuybuy$qual_value)) AS qual_value,

                upper(r.get_cell(multibuybuy$reedem_amount )) as reedem_amount,
                upper(r.get_cell(multibuybuy$cc_amount     )) as cc_amount,
                upper(r.get_cell(multibuybuy$cc_start_date )) as cc_start_date,
                upper(r.get_cell(multibuybuy$cc_end_date   )) as cc_end_date,
                upper(r.get_cell(multibuybuy$new_gp_amount )) as new_gp_amount,
                upper(r.get_cell(multibuybuy$new_gp_percent)) as new_gp_percent,
                upper(r.get_cell(multibuybuy$roq_units     )) as roq_units,
                upper(r.get_cell(multibuybuy$roq_inners    )) as roq_inners,
                upper(r.get_cell(multibuybuy$oi_value      )) as oi_value,

                r.get_row_seq() AS row_seq
            FROM
                s9t_folder                      sf,
                TABLE ( sf.s9t_file_obj.sheets ) ss,
                TABLE ( ss.s9t_rows )           r
            WHERE
                sf.file_id = i_file_id
                AND ss.sheet_name = sheet_name_trans(multibuybuy_sheet)
        ) LOOP
            l_temp_rec := NULL;
            l_temp_rec.process_id := i_process_id;
            l_temp_rec.chunk_id := 1;
            l_temp_rec.row_seq := rec.row_seq;
            l_temp_rec.process$status := 'N';
            l_temp_rec.create_id := get_user;
            l_temp_rec.last_upd_id := get_user;
            l_temp_rec.create_datetime := sysdate;
            l_temp_rec.last_upd_datetime := sysdate;
            l_error := false;
            BEGIN
                if rec.buylist_group is not null
                then                
                l_temp_rec.buylist_group := rec.buylist_group;
                else
                        WRITE_S9T_ERROR(I_file_id,
                                        multibuybuy_sheet,
                                        rec.row_seq,
                                        'BUYLIST_GROUP',
                                        null,
                                        'S9T_PK_COL_REQD');
                         L_error := TRUE;
                end if;                
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'BUYLIST_GROUP', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.skulist := rec.skulist;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'SKULIST', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.item is not null
                then
                l_temp_rec.item := rec.item;
                ELSE
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'ITEM', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'ITEM', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.supplier is not null
                then
                l_temp_rec.supplier := rec.supplier;
                ELSE
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'SUPPLIER', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'SUPPLIER', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.qual_type := rec.qual_type;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'QUAL_TYPE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.qual_value := rec.qual_value;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'QUAL_VALUE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.REEDEM_AMOUNT := rec.REEDEM_AMOUNT;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'REEDEM_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.CC_AMOUNT := rec.CC_AMOUNT;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'CC_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.CC_START_DATE := rec.CC_START_DATE;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'CC_START_DATE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.CC_END_DATE := rec.CC_END_DATE;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'CC_END_DATE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.NEW_GP_AMOUNT := rec.NEW_GP_AMOUNT;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'NEW_GP_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.NEW_GP_PERCENT := rec.NEW_GP_PERCENT;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'NEW_GP_PERCENT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.ROQ_UNITS := rec.ROQ_UNITS;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'ROQ_UNITS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

             BEGIN
                l_temp_rec.ROQ_INNERS := rec.ROQ_INNERS;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'ROQ_INNERS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;


            BEGIN
                l_temp_rec.OI_VALUE := rec.OI_VALUE;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuybuy_sheet, rec.row_seq, 'OI_VALUE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            IF NOT l_error THEN
                svc_buy_col.extend();
                svc_buy_col(svc_buy_col.count()) := l_temp_rec;
            END IF;

        END LOOP;

        BEGIN
            FORALL i IN 1..svc_buy_col.count SAVE EXCEPTIONS
                INSERT INTO crc_svc_multibuy_buy_stg (
                    process_id,
                    chunk_id,
                    row_seq,
                    process$status,
                    buylist_group,
                    skulist,
                    item,
                    supplier,
                    qual_type,
                    qual_value,
                    reedem_amount,
                    cc_amount    ,
                    cc_start_date,
                    cc_end_date  ,
                    new_gp_amount,
                    new_gp_percent,
                    roq_units   ,
                    roq_inners   ,
                    oi_value    ,
                    create_id,
                    create_datetime,
                    last_upd_id,
                    last_upd_datetime
                ) VALUES (
                    svc_buy_col(i).process_id,
                    svc_buy_col(i).chunk_id,
                    svc_buy_col(i).row_seq,
                    svc_buy_col(i).process$status,
                    svc_buy_col(i).buylist_group,
                    svc_buy_col(i).skulist,
                    svc_buy_col(i).item,
                    svc_buy_col(i).supplier,
                    svc_buy_col(i).qual_type,
                    svc_buy_col(i).qual_value,
                    svc_buy_col(i).reedem_amount,
                    svc_buy_col(i).cc_amount    ,
                    svc_buy_col(i).cc_start_date,
                    svc_buy_col(i).cc_end_date  ,
                    svc_buy_col(i).new_gp_amount,
                    svc_buy_col(i).new_gp_percent,
                    svc_buy_col(i).roq_units   ,
                    svc_buy_col(i).roq_inners   ,
                    svc_buy_col(i).oi_value    ,
                    svc_buy_col(i).create_id,
                    svc_buy_col(i).create_datetime,
                    svc_buy_col(i).last_upd_id,
                    svc_buy_col(i).last_upd_datetime
                );

        EXCEPTION
            WHEN dml_errors THEN
                FOR i IN 1..SQL%bulk_exceptions.count LOOP
                    l_error_code := SQL%bulk_exceptions(i).error_code;
                    IF l_error_code = 1 THEN
                        l_error_code := NULL;
                        l_error_msg := sql_lib.create_msg('DUP_REC_EXISTS_S9T', l_pk_columns);
                    END IF;

                    write_s9t_error(i_file_id, multibuybuy_sheet, svc_buy_col(SQL%bulk_exceptions(i).error_index).row_seq, NULL,
                    l_error_code,
                                    l_error_msg);

                END LOOP;
        END;

    END process_s9t_buy;
----------------------------------------------------------------------------------------------

    PROCEDURE process_s9t_get (
        i_file_id      IN   s9t_folder.file_id%TYPE,
        i_process_id   IN   svc_brand.process_id%TYPE
    ) IS

        TYPE svc_get_col_typ IS
            TABLE OF crc_svc_multibuy_get_stg%rowtype;
        l_temp_rec      crc_svc_multibuy_get_stg%rowtype;
        svc_get_col     svc_get_col_typ := NEW svc_get_col_typ();
        l_process_id    crc_svc_multibuy_get_stg.process_id%TYPE;
        l_error         BOOLEAN := false;
        l_default_rec   crc_svc_multibuy_get_stg%rowtype;
        dml_errors EXCEPTION;
        PRAGMA exception_init ( dml_errors, -24381 );
        l_table         VARCHAR2(30) := 'CRC_SVC_MULTIBUY_GET_STG';
        l_pk_columns    VARCHAR2(255) := 'getlist_group';
        l_error_code    NUMBER;
        l_error_msg     rtk_errors.rtk_text%TYPE;
    BEGIN
        FOR rec IN (
            SELECT
                upper(r.get_cell(multibuyget$buylist_group)) AS buylist_group,
                upper(r.get_cell(multibuyget$getlist_group)) AS getlist_group,
                upper(r.get_cell(multibuyget$skulist)) AS skulist,
                upper(r.get_cell(multibuyget$item)) AS item,
                upper(r.get_cell(multibuyget$supplier)) AS supplier,
                upper(r.get_cell(multibuyget$qual_type)) AS qual_type,
                upper(r.get_cell(multibuyget$qual_value)) AS qual_value,
                upper(r.get_cell(multibuyget$change_type)) AS change_type,
                upper(r.get_cell(multibuyget$change_amount)) AS change_amount,
                upper(r.get_cell(multibuyget$reedem_amount)) AS reedem_amount,
                upper(r.get_cell(multibuyget$cc_amount)) AS cc_amount,
                upper(r.get_cell(multibuyget$cc_start_date)) AS cc_start_date,
                upper(r.get_cell(multibuyget$cc_end_date)) AS cc_end_date,
                upper(r.get_cell(multibuyget$new_gp_amount)) AS new_gp_amount,
                upper(r.get_cell(multibuyget$new_gp_percent)) AS new_gp_percent,
                upper(r.get_cell(multibuyget$roq_units)) AS roq_units,
                upper(r.get_cell(multibuyget$roq_inners)) AS roq_inners,
                upper(r.get_cell(multibuyget$oi_value)) AS oi_value,
                r.get_row_seq() AS row_seq
            FROM
                s9t_folder                      sf,
                TABLE ( sf.s9t_file_obj.sheets ) ss,
                TABLE ( ss.s9t_rows )           r
            WHERE
                sf.file_id = i_file_id
                AND ss.sheet_name = sheet_name_trans(multibuyget_sheet)
        ) LOOP
            l_temp_rec := NULL;
            l_temp_rec.process_id := i_process_id;
            l_temp_rec.chunk_id := 1;
            l_temp_rec.row_seq := rec.row_seq;
            l_temp_rec.process$status := 'N';
            l_temp_rec.create_id := get_user;
            l_temp_rec.last_upd_id := get_user;
            l_temp_rec.create_datetime := sysdate;
            l_temp_rec.last_upd_datetime := sysdate;
            l_error := false;
            BEGIN
                if rec.getlist_group is not null
                then
                l_temp_rec.getlist_group := rec.getlist_group;
                ELSE
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'GETLIST_GROUP', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                 
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'GETLIST_GROUP', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.buylist_group is not null
                then 
                l_temp_rec.buylist_group := rec.buylist_group;
                ELSE
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'BUYLIST_GROUP', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                   
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'BUYLIST_GROUP', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.skulist := rec.skulist;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'SKULIST', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.item := rec.item;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'ITEM', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.supplier := rec.supplier;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'SUPPLIER', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.qual_type := rec.qual_type;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'QUAL_TYPE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.qual_value := rec.qual_value;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'QUAL_VALUE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.change_type is not null
                then
                l_temp_rec.change_type := rec.change_type;
                ELSE
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CHANGE_TYPE', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                    
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CHANGE_TYPE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                if rec.change_amount is not null
                then
                l_temp_rec.change_amount := rec.change_amount;
                ELSE
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CHANGE_AMOUNT', NULL,
                                    'S9T_PK_COL_REQD');
                    l_error := true;
                END IF;                  
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CHANGE_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.reedem_amount := rec.reedem_amount;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'REEDEM_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.cc_amount := rec.cc_amount;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CC_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.cc_start_date := rec.cc_start_date;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CC_START_DATE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.cc_end_date := rec.cc_end_date;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'CC_END_DATE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.new_gp_amount := rec.new_gp_amount;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'NEW_GP_AMOUNT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.new_gp_percent := rec.new_gp_percent;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'NEW_GP_PERCENT', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.roq_units := rec.roq_units;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'ROQ_UNITS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.roq_inners := rec.roq_inners;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'ROQ_INNERS', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            BEGIN
                l_temp_rec.oi_value := rec.oi_value;
            EXCEPTION
                WHEN OTHERS THEN
                    write_s9t_error(i_file_id, multibuyget_sheet, rec.row_seq, 'OI_VALUE', sqlcode,
                                    sqlerrm);
                    l_error := true;
            END;

            IF NOT l_error THEN
                svc_get_col.extend();
                svc_get_col(svc_get_col.count()) := l_temp_rec;
            END IF;

        END LOOP;

        BEGIN
            FORALL i IN 1..svc_get_col.count SAVE EXCEPTIONS
                INSERT INTO crc_svc_multibuy_get_stg (
                    process_id,
                    chunk_id,
                    row_seq,
                    process$status,
                    buylist_group,
                    getlist_group,
                    skulist,
                    item,
                    supplier,
                    qual_type,
                    qual_value,
                    change_type,
                    change_amount,
                    reedem_amount,
                    cc_amount,
                    cc_start_date,
                    cc_end_date,
                    new_gp_amount,
                    new_gp_percent,
                    roq_units,
                    roq_inners,
                    oi_value,
                    create_id,
                    create_datetime,
                    last_upd_id,
                    last_upd_datetime
                ) VALUES (
                    svc_get_col(i).process_id,
                    svc_get_col(i).chunk_id,
                    svc_get_col(i).row_seq,
                    svc_get_col(i).process$status,
                    svc_get_col(i).buylist_group,
                    svc_get_col(i).getlist_group,
                    svc_get_col(i).skulist,
                    svc_get_col(i).item,
                    svc_get_col(i).supplier,
                    svc_get_col(i).qual_type,
                    svc_get_col(i).qual_value,
                    svc_get_col(i).change_type,
                    svc_get_col(i).change_amount,
                    svc_get_col(i).reedem_amount,
                    svc_get_col(i).cc_amount,
                    svc_get_col(i).cc_start_date,
                    svc_get_col(i).cc_end_date,
                    svc_get_col(i).new_gp_amount,
                    svc_get_col(i).new_gp_percent,
                    svc_get_col(i).roq_units,
                    svc_get_col(i).roq_inners,
                    svc_get_col(i).oi_value,
                    svc_get_col(i).create_id,
                    svc_get_col(i).create_datetime,
                    svc_get_col(i).last_upd_id,
                    svc_get_col(i).last_upd_datetime
                );

        EXCEPTION
            WHEN dml_errors THEN
                FOR i IN 1..SQL%bulk_exceptions.count LOOP
                    l_error_code := SQL%bulk_exceptions(i).error_code;
                    IF l_error_code = 1 THEN
                        l_error_code := NULL;
                        l_error_msg := sql_lib.create_msg('DUP_REC_EXISTS_S9T', l_pk_columns);
                    END IF;

                    write_s9t_error(i_file_id, multibuyget_sheet, svc_get_col(SQL%bulk_exceptions(i).error_index).row_seq, NULL,
                    l_error_code,
                                    l_error_msg);

                END LOOP;
        END;

    END process_s9t_get;
-------------------------------------------------------------------------

    FUNCTION process_s9t (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        o_error_count     IN OUT  NUMBER,
        i_file_id         IN      s9t_folder.file_id%TYPE,
        i_process_id      IN      NUMBER
    ) RETURN BOOLEAN IS

        l_program          VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.PROCESS_S9T';
        l_file             s9t_file;
        l_sheets           s9t_pkg.names_map_typ;
        l_process_status   svc_process_tracker.status%TYPE;
        invalid_format EXCEPTION;
        PRAGMA exception_init ( invalid_format, -31011 );
        max_char EXCEPTION;
        PRAGMA exception_init ( max_char, -01706 );
    BEGIN
        COMMIT;
        s9t_pkg.ods2obj(i_file_id);
        COMMIT;
        l_file := s9t_pkg.get_obj(i_file_id);
        lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
        IF s9t_pkg.code2desc(o_error_message, template_category, l_file, true) = false THEN
            RETURN false;
        END IF;

        s9t_pkg.save_obj(l_file);
        IF s9t_pkg.validate_template(i_file_id) = false THEN
            write_s9t_error(i_file_id, NULL, NULL, NULL, NULL,
                            'S9T_INVALID_TEMPLATE');
        ELSE
            sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key, l_file.user_lang);
            process_s9t_hdr(i_file_id, i_process_id);
            process_s9t_buy(i_file_id, i_process_id);
            process_s9t_get(i_file_id, i_process_id);
        END IF;

        o_error_count := lp_s9t_errors_tab.count();
        FORALL i IN 1..o_error_count
            INSERT INTO s9t_errors VALUES lp_s9t_errors_tab ( i );

        lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
        IF o_error_count = 0 THEN
            l_process_status := 'PS';
        ELSE
            l_process_status := 'PE';
        END IF;

        UPDATE svc_process_tracker
        SET
            status = l_process_status,
            file_id = i_file_id
        WHERE
            process_id = i_process_id;

        COMMIT;
        RETURN true;
    EXCEPTION
        WHEN invalid_format THEN
            ROLLBACK;
            o_error_message := sql_lib.create_msg('INV_FILE_FORMAT', NULL, NULL, NULL);
            lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
            write_s9t_error(i_file_id, NULL, NULL, NULL, NULL,
                            'INV_FILE_FORMAT');
            o_error_count := lp_s9t_errors_tab.count();
            FORALL i IN 1..o_error_count
                INSERT INTO s9t_errors VALUES lp_s9t_errors_tab ( i );

            UPDATE svc_process_tracker
            SET
                status = 'PE',
                file_id = i_file_id
            WHERE
                process_id = i_process_id;

            COMMIT;
            RETURN false;
        WHEN max_char THEN
            ROLLBACK;
            o_error_message := sql_lib.create_msg('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
            lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
            write_s9t_error(i_file_id, NULL, NULL, NULL, NULL,
                            'EXCEEDS_4000_CHAR');
            o_error_count := lp_s9t_errors_tab.count();
            FORALL i IN 1..o_error_count
                INSERT INTO s9t_errors VALUES lp_s9t_errors_tab ( i );

            UPDATE svc_process_tracker
            SET
                status = 'PE',
                file_id = i_file_id
            WHERE
                process_id = i_process_id;

            COMMIT;
            RETURN false;
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END process_s9t;

----------------------------------------------------------------------------------------------------
FUNCTION Create_cc_get(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_CC_GET';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_SUSP_SUP_HEAD';
   L_PROCESS_ID   SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%type;
   L_chunk_id    SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%type;
   L_ACTIVE_DATE    SVC_COST_SUSP_SUP_HEAD.ACTIVE_DATE%type;
   L_end_date      SVC_COST_SUSP_SUP_HEAD.ACTIVE_DATE%type;
   L_ITEM    SVC_COST_SUSP_SUP_DETAIL.ITEM%type;
   L_Supplier    SVC_COST_SUSP_SUP_DETAIL.Supplier%type;
   L_cc_reason    SVC_COST_SUSP_SUP_HEAD.reason%type;
   L_cost_change    SVC_COST_SUSP_SUP_DETAIL.cost_change%type;
   promo_temp_rec   c_svc_promo_get%ROWTYPE;
   l_unit_cost      SVC_COST_SUSP_SUP_DETAIL.unit_cost%type;
   L_COST_CHANGE_SEQ     SVC_COST_SUSP_SUP_DETAIL.cost_change%type;
   l_row_seq  number(10);
   l_group_id number(10);
   HCOST_ROW_SEQ number(10):=0; 
   DCOST_ROW_SEQ number(10):=0;
   CFA_COST_ROW_SEQ number(10):=0; 
   DLOC_COST_ROW_SEQ number(10):=0;
   l_error_count number(2):=0;
   I_promo_id  number(10);
   L_tot_error_cnt number(10);
   L_warning_cnt number(10);
   l_item_cost    number(1);
   i_buylist_group               NUMBER(15);    
   l_zone_id   number(4);
   key_col_array KEY_VAL_PAIRS:=KEY_VAL_PAIRS();
   attr_col_array KEY_VAL_PAIRS:=KEY_VAL_PAIRS();
   TYPE  cost_cre_record IS RECORD ( PROCESS_ID SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%type,
                                    COST_CHANGE SVC_COST_SUSP_SUP_HEAD.COST_CHANGE%type,
                                    ACTIVE_DATE SVC_COST_SUSP_SUP_HEAD.ACTIVE_DATE%type,
                                     Supplier SVC_COST_SUSP_SUP_DETAIL.Supplier%type,
                                    cc_reason    SVC_COST_SUSP_SUP_HEAD.reason%type
                                  );

   TYPE Cost_cre_record_tab IS TABLE OF cost_cre_record;
   L_COST_CRE Cost_cre_record_tab;

  cursor C_ERRORS_EXIST is
      select COUNT(error_type) tot_err_count,
             NVL(SUM(DECODE(error_type, 'W', 1, 0)), 0) warn_count
        from coresvc_costchg_err
       where process_id = I_process_id;

       cursor C_ERRORS_EXIST_ERR_CHK is
      /* select row_seq ,
       table_name,
       error_msg from coresvc_costchg_err
       where process_id = I_process_id;*/
       select   stg.process_id,
                stg.row_seq,stg.item,
                cost_error.error_msg ,
                cost_error.row_seq cost_err_seq ,
                 cost_error.table_name
       from CRC_SVC_MULTIBUY_GET_STG stg,    
         (select distinct  cssh.process_id ,
                          cssh.cost_change,cssh.row_seq, 
                          cc_err.error_msg,
                          cssdl.item item ,
                         cssh.active_date,cc_err.table_name
                 from svc_cost_susp_sup_head cssh,
                      svc_cost_susp_sup_detail_loc cssdl, 
                       coresvc_costchg_err cc_err
                 where   cssh.process_id=cssdl.process_id
                 and cssh.cost_change=cssdl.cost_change
                 and cc_err.process_id=cssh.process_id
                 and cc_err.process_id=cssh.process_id
                and cc_err.row_seq=cssh.row_seq
                 union
               select distinct  cssh.process_id ,
                                cssh.cost_change,cssh.row_seq,
                                cc_err.error_msg,
                                cssdl.item item ,
                                cssh.active_date ,cc_err.table_name
                 from svc_cost_susp_sup_head cssh,
                     svc_cost_susp_sup_detail cssdl,
                      coresvc_costchg_err cc_err
                  where   cssh.process_id=cssdl.process_id
                  and cssh.cost_change=cssdl.cost_change
                  and cc_err.process_id=cssh.process_id
                  and cc_err.process_id=cssh.process_id
                  and cc_err.row_seq=cssh.row_seq  
        ) cost_error where stg.process_id=cost_error.process_id
                     and stg.item=cost_error.item 
                     and  cost_error.process_id = I_process_id;

      cursor C_ZONE_LOC is
              select zone_id ,
                     location
                       from rpm_zone_location
                       where zone_id=l_zone_id ;

   Cursor C_SVC_COST (L_PROCESS_ID number,
                        L_chunk_id number,
                         l_supplier number,
                         l_cc_reason number,
                         L_ACTIVE_DATE date ) is 
   select  distinct CHEAD.PROCESS_ID,
            CHEAD.COST_CHANGE,
            CHEAD.ACTIVE_DATE, 
            CDETAIL.SUPPLIER ,
             CHEAD.REASON
         from SVC_COST_SUSP_SUP_HEAD CHEAD,
              SVC_COST_SUSP_SUP_DETAIL CDETAIL
         where CHEAD.PROCESS_ID=CDETAIL.PROCESS_ID
         and CHEAD.CHUNK_ID=CDETAIL.CHUNK_ID
          AND CHEAD.COST_CHANGE=CDETAIL.COST_CHANGE
          and CHEAD.PROCESS_ID=L_PROCESS_ID
          and CHEAD.Chunk_id=L_chunk_id
          and CHEAD.ACTIVE_DATE=L_ACTIVE_DATE
          and CDETAIL.SUPPLIER=l_supplier
          and CHEAD.reason=l_cc_reason
          and chead.status='S'
          union

        SELECT DISTINCT
        chead.process_id,
        chead.cost_change,
        chead.active_date,
        cdetail.supplier,
        chead.reason
    FROM
        svc_cost_susp_sup_head chead,
        svc_cost_susp_sup_detail_loc cdetail
    WHERE
        chead.process_id = cdetail.process_id
        AND chead.chunk_id = cdetail.chunk_id
        AND chead.cost_change = cdetail.cost_change
        AND chead.process_id = l_process_id
        AND chead.chunk_id = l_chunk_id
        AND chead.active_date = l_active_date
        AND cdetail.supplier = l_supplier
        AND chead.reason = l_cc_reason
        AND chead.status = 'S';

  BEGIN
  
   if  LP_errors_tab.COUNT()=0 
    then
      HCOST_ROW_SEQ:=0;
      DCOST_ROW_SEQ:=0;
      CFA_COST_ROW_SEQ:=0;
      open c_svc_promo_get(I_process_id,i_chunk_id);
          loop
          fetch c_svc_promo_get into promo_temp_rec;
           EXIT WHEN c_svc_promo_get%NOTFOUND;

           L_COST_CRE :=new Cost_cre_record_tab();
           L_PROCESS_ID:=promo_temp_rec.PROCESS_ID;
           L_chunk_id:=promo_temp_rec.Chunk_id;
           l_supplier:=promo_temp_rec.supplier;
           L_ACTIVE_DATE:=promo_temp_rec.CC_START_DATE;
           L_end_date :=promo_temp_rec.CC_END_DATE;
           select group_id into l_group_id from cfa_attrib WHERE view_col_name ='V_CRC_CC_ATTR';
        --  select count(error_seq)  into l_error_count from   SVC_ADMIN_UPLD_ER where process_id=L_PROCESS_ID and chunk_id=l_chunk_id 
         -- ;--and row_seq=promo_temp_rec.row_seq;


  --       if  LP_errors_tab.COUNT()=0 and (promo_temp_rec.CC_START_DATE is not null and promo_temp_rec.cc_amount is not null) then


              if promo_temp_rec.CHANGE_AMOUNT is not null then

                   select distinct promo_id into I_promo_id from rpm_stage_multibuy_header 
                    where comments=to_char(I_process_id);

              end if;

              LP_REC_SVC_COST_HEAD := NEW SVC_COST_HEAD();
              LP_REC_SVC_COST_DETAIL := NEW SVC_COST_DETAIL();
              LP_REC_SVC_CFA_EXT := NEW TAB_SVC_CFA_EXT();
              LP_REC_SVC_COST_DETL_LOC :=new SVC_COST_DETAIL_LOC();
              key_col_array :=new KEY_VAL_PAIRS();
              attr_col_array := new KEY_VAL_PAIRS();
              l_cc_reason:=11;

               open C_SVC_COST(L_PROCESS_ID, L_chunk_id,l_supplier,l_cc_reason,L_ACTIVE_DATE);
               fetch C_SVC_COST bulk collect into L_COST_CRE;
               close C_SVC_COST;

                 if  L_COST_CRE.count=0 then

                         HCOST_ROW_SEQ:=HCOST_ROW_SEQ+1;
                         DCOST_ROW_SEQ:=DCOST_ROW_SEQ+1;
                         CFA_COST_ROW_SEQ:=CFA_COST_ROW_SEQ+1;
                         DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;

                         WRITE_SVC_COST_HEAD(promo_temp_rec.PROCESS_ID,
                                          promo_temp_rec.CHUNK_ID,
                                           HCOST_ROW_SEQ,
                                          CORESVC_COSTCHG_ESEQ.nextval,
                                          promo_temp_rec.PROMO_NAME,
                                          l_cc_reason,
                                           promo_temp_rec.CC_START_DATE) ;


                         L_COST_CHANGE_SEQ:=CORESVC_COSTCHG_ESEQ.currval;

                         key_col_array.extend();
                         key_col_array(key_col_array.count()):=KEY_VALUE_PAIR('COST_CHANGE',to_char(CORESVC_COSTCHG_ESEQ.currval));
                         attr_col_array.extend();
                         attr_col_array(attr_col_array.count()):=KEY_VALUE_PAIR('V_CRC_CC_ATTR',to_char(I_PROMO_ID));
                         attr_col_array.extend();
                         attr_col_array(attr_col_array.count()):=KEY_VALUE_PAIR('ROW_SEQ',to_char(HCOST_ROW_SEQ));

                          WRITE_SVC_CFA_EXT(promo_temp_rec.PROCESS_ID,
                                              promo_temp_rec.CHUNK_ID ,
                                             CFA_COST_ROW_SEQ,
                                              'COST_SUSP_SUP_HEAD' ,
                                                key_col_array,
                                                attr_col_array
                                            );


                        if  promo_temp_rec.loc_type='A'  then  

                                 WRITE_SVC_COST_DETAIL(promo_temp_rec.PROCESS_ID,
                                                      promo_temp_rec.CHUNK_ID,
                                                      DCOST_ROW_SEQ,
                                                       CORESVC_COSTCHG_ESEQ.currval,
                                                       promo_temp_rec.supplier,
                                                      promo_temp_rec.item,
                                                       promo_temp_rec.CC_AMOUNT ) ;

                         elsif promo_temp_rec.loc_type='S' then

                                 WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                           promo_temp_rec.CHUNK_ID     ,
                                                           DLOC_COST_ROW_SEQ ,
                                                           CORESVC_COSTCHG_ESEQ.currval ,
                                                            promo_temp_rec.supplier  ,
                                                          promo_temp_rec.item   ,
                                                            promo_temp_rec.location    ,
                                                            promo_temp_rec.CC_AMOUNT    );

                         elsif promo_temp_rec.loc_type='Z' then

                                  select zone_id into l_zone_id from rpm_zone where zone_display_id=promo_temp_rec.location;

                                  for rec in C_ZONE_LOC 
                                    loop         
                                            WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                                   promo_temp_rec.CHUNK_ID     ,
                                                                   DLOC_COST_ROW_SEQ ,
                                                                   CORESVC_COSTCHG_ESEQ.currval ,
                                                                    promo_temp_rec.supplier  ,
                                                                    promo_temp_rec.item   ,
                                                                    rec.location    ,
                                                                    promo_temp_rec.CC_AMOUNT    );
                                            DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;
                                    end loop;

                          end if;


                   else 

                            L_cost_change:=L_COST_CRE(1).cost_change;

                           if  promo_temp_rec.loc_type='A'  then  

                                 select count(1) into l_item_cost from SVC_COST_SUSP_SUP_DETAIL  where cost_change=L_cost_change
                                                    and item=promo_temp_rec.item;

                                 if l_item_cost =0 then

                                          DCOST_ROW_SEQ:=DCOST_ROW_SEQ+1;
                                        WRITE_SVC_COST_DETAIL(promo_temp_rec.PROCESS_ID,
                                                              promo_temp_rec.CHUNK_ID,
                                                              DCOST_ROW_SEQ,
                                                              L_cost_change,
                                                              promo_temp_rec.supplier,
                                                              promo_temp_rec.item,
                                                              promo_temp_rec.CC_AMOUNT ) ;
                                 end  if;

                            elsif promo_temp_rec.loc_type='S' then

                                  select count(1) into l_item_cost from SVC_COST_SUSP_SUP_DETAIL_LOC  where cost_change=L_cost_change
                                                    and item=promo_temp_rec.item and  loc=promo_temp_rec.location;

                                   if l_item_cost =0 then

                                         DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;
                                        WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                                    promo_temp_rec.CHUNK_ID     ,
                                                                    DLOC_COST_ROW_SEQ ,
                                                                   L_cost_change ,
                                                                    promo_temp_rec.supplier  ,
                                                                    promo_temp_rec.item   ,
                                                                     promo_temp_rec.location    ,
                                                                  promo_temp_rec.CC_AMOUNT    );
                                   end if;

                            elsif promo_temp_rec.loc_type='Z' then

                                 select zone_id into l_zone_id from rpm_zone where zone_display_id=promo_temp_rec.location;

                                  for rec in C_ZONE_LOC 
                                   loop  
                                    select count(1) into l_item_cost from SVC_COST_SUSP_SUP_DETAIL_LOC  where cost_change=L_cost_change
                                                    and item=promo_temp_rec.item and  loc=rec.location;
                                          if l_item_cost =0 then

                                                     WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                                                promo_temp_rec.CHUNK_ID     ,
                                                                                DLOC_COST_ROW_SEQ ,
                                                                                CORESVC_COSTCHG_ESEQ.currval ,
                                                                                promo_temp_rec.supplier  ,
                                                                                 promo_temp_rec.item   ,
                                                                                 rec.location    ,
                                                                                  promo_temp_rec.CC_AMOUNT    );
                                                    DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;
                                         end if;
                                   end loop;

                           end if;  

               end if; 

             key_col_array.delete();
             attr_col_array.delete();

             if L_end_date is not null then



                      key_col_array :=new KEY_VAL_PAIRS();
                      attr_col_array := new KEY_VAL_PAIRS();
                      l_cc_reason:=12;
                      L_COST_CRE.extend();

                      L_end_date:=l_end_date+1;
                      open C_SVC_COST(L_PROCESS_ID, L_chunk_id,l_supplier,l_cc_reason,L_end_date);
                      fetch C_SVC_COST bulk collect into L_COST_CRE;
                      close C_SVC_COST;

                      if  L_COST_CRE.count=0 then

                              HCOST_ROW_SEQ:=HCOST_ROW_SEQ+1;
                              DCOST_ROW_SEQ:=DCOST_ROW_SEQ+1;
                              CFA_COST_ROW_SEQ:=CFA_COST_ROW_SEQ+1;
                              DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;

                              WRITE_SVC_COST_HEAD(promo_temp_rec.PROCESS_ID,
                                                  promo_temp_rec.CHUNK_ID,
                                                  HCOST_ROW_SEQ,
                                                  CORESVC_COSTCHG_ESEQ.nextval,
                                                   promo_temp_rec.PROMO_NAME,
                                                   l_cc_reason,
                                                   L_end_date) ;

                               key_col_array.extend();
                               key_col_array(key_col_array.count()):=KEY_VALUE_PAIR('COST_CHANGE',to_char(CORESVC_COSTCHG_ESEQ.currval));
                               attr_col_array.extend();
                                attr_col_array(attr_col_array.count()):=KEY_VALUE_PAIR('V_CRC_CC_ATTR',to_char(I_PROMO_ID));
                               attr_col_array.extend();
                               attr_col_array(attr_col_array.count()):=KEY_VALUE_PAIR('ROW_SEQ',to_char(HCOST_ROW_SEQ));

                             WRITE_SVC_CFA_EXT(promo_temp_rec.PROCESS_ID,
                                               promo_temp_rec.CHUNK_ID ,
                                                CFA_COST_ROW_SEQ,
                                                'COST_SUSP_SUP_HEAD' ,
                                                 key_col_array,
                                                attr_col_array
                                              );

                             if  promo_temp_rec.loc_type='A' then   

                              select unit_cost into l_unit_cost from item_supp_country where item=promo_temp_rec.item and 
                             supplier=promo_temp_rec.supplier and origin_country_id='TH';

                                          WRITE_SVC_COST_DETAIL(promo_temp_rec.PROCESS_ID,
                                                                 promo_temp_rec.CHUNK_ID,
                                                                 DCOST_ROW_SEQ,
                                                                CORESVC_COSTCHG_ESEQ.currval,
                                                                  promo_temp_rec.supplier,
                                                                   promo_temp_rec.item,
                                                                l_unit_cost ) ;   

                           elsif promo_temp_rec.loc_type='S' then

                            select unit_cost into l_unit_cost from item_supp_country_loc where item=promo_temp_rec.item and 
                             supplier=promo_temp_rec.supplier and origin_country_id='TH' and loc= promo_temp_rec.location;

                                         WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                                   promo_temp_rec.CHUNK_ID     ,
                                                                     DLOC_COST_ROW_SEQ ,
                                                                     CORESVC_COSTCHG_ESEQ.currval ,
                                                                     promo_temp_rec.supplier  ,
                                                                    promo_temp_rec.item   ,
                                                                     promo_temp_rec.location    ,
                                                                     l_unit_cost    );

                            elsif promo_temp_rec.loc_type='Z' then

                                   select zone_id into l_zone_id from rpm_zone where zone_display_id=promo_temp_rec.location;

                                   for rec in C_ZONE_LOC 
                                     loop     

                                       select unit_cost into l_unit_cost from item_supp_country_loc where item=promo_temp_rec.item and 
                             supplier=promo_temp_rec.supplier and origin_country_id='TH' and loc= rec.location;

                                  WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                              promo_temp_rec.CHUNK_ID     ,
                                                              DLOC_COST_ROW_SEQ ,
                                                             CORESVC_COSTCHG_ESEQ.currval ,
                                                             promo_temp_rec.supplier  ,
                                                             promo_temp_rec.item   ,
                                                           rec.location    ,
                                                              l_unit_cost   );
                                     DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;
                                   end loop;
                         end if;                       
                 else 

                           L_cost_change:=L_COST_CRE(1).cost_change;

                         if  promo_temp_rec.loc_type='A'  then  

                                 select count(1) into l_item_cost from SVC_COST_SUSP_SUP_DETAIL  where cost_change=L_cost_change
                                                    and item=promo_temp_rec.item;

                                 if l_item_cost =0 then

                                      select unit_cost into l_unit_cost from item_supp_country where item=promo_temp_rec.item and 
                                        supplier=promo_temp_rec.supplier and origin_country_id='TH';

                                            DCOST_ROW_SEQ:=DCOST_ROW_SEQ+1;
                                          WRITE_SVC_COST_DETAIL(promo_temp_rec.PROCESS_ID,
                                                             promo_temp_rec.CHUNK_ID,
                                                             DCOST_ROW_SEQ,
                                                                L_cost_change,
                                                            promo_temp_rec.supplier,
                                                             promo_temp_rec.item,
                                                            l_unit_cost ) ;
                               end if;                       

                          elsif promo_temp_rec.loc_type='S' then

                                 select count(1) into l_item_cost from SVC_COST_SUSP_SUP_DETAIL_LOC  where cost_change=L_cost_change
                                                    and item=promo_temp_rec.item and  loc=promo_temp_rec.location;

                                 if l_item_cost =0 then

                                          select unit_cost into l_unit_cost from item_supp_country_loc where item=promo_temp_rec.item and 
                                          supplier=promo_temp_rec.supplier and origin_country_id='TH' and loc= promo_temp_rec.location;

                                        DLOC_COST_ROW_SEQ:=DLOC_COST_ROW_SEQ+1;
                                         WRITE_SVC_COST_DETAIL_LOC(promo_temp_rec.PROCESS_ID   ,
                                                                  promo_temp_rec.CHUNK_ID     ,
                                                                    DLOC_COST_ROW_SEQ ,
                                                                   L_cost_change,
                                                                    promo_temp_rec.supplier  ,
                                                                    promo_temp_rec.item   ,
                                                                      promo_temp_rec.location    ,
                                                                   l_unit_cost    );
                                end if;

                           elsif promo_temp_rec.loc_type='Z' then

                                  SELECT
                                        zone_id
                                    INTO l_zone_id
                                    FROM
                                        rpm_zone
                                    WHERE
                                        zone_display_id = promo_temp_rec.location;

                        for rec in C_ZONE_LOC 
                             LOOP
                                SELECT
                                    COUNT(1)
                                INTO l_item_cost
                                FROM
                                    svc_cost_susp_sup_detail_loc
                                WHERE
                                    cost_change = l_cost_change
                                    AND item = promo_temp_rec.item
                                    AND loc = rec.location;
                            
                                IF l_item_cost = 0 THEN
                                    SELECT
                                        unit_cost
                                    INTO l_unit_cost
                                    FROM
                                        item_supp_country_loc
                                    WHERE
                                        item = promo_temp_rec.item
                                        AND supplier = promo_temp_rec.supplier
                                        AND origin_country_id = 'TH'
                                        AND loc = rec.location;
                            
                                    write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq, l_cost_change, promo_temp_rec
                                    .supplier,
                                                              promo_temp_rec.item, rec.location, l_unit_cost);
                            
                                    dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                END IF;
                            
                            END LOOP;
                         end if;                          

                end if;
         end if;    
            if LP_REC_SVC_COST_HEAD.count<>0 then
                 forall i in 1..LP_REC_SVC_COST_HEAD.count SAVE EXCEPTIONS
                      insert into SVC_COST_SUSP_SUP_HEAD  values LP_REC_SVC_COST_HEAD(i);
            end if;

           if LP_REC_SVC_COST_DETAIL.count<>0 then
               forall i in 1..LP_REC_SVC_COST_DETAIL.count SAVE EXCEPTIONS
                  insert into SVC_COST_SUSP_SUP_DETAIL  values LP_REC_SVC_COST_DETAIL(i);
           end if;


            if LP_REC_SVC_CFA_EXT.count<>0 then
                   forall i in 1..LP_REC_SVC_CFA_EXT.count SAVE EXCEPTIONS
                    insert into SVC_CFA_EXT  values LP_REC_SVC_CFA_EXT(i);
             end if;

             if LP_REC_SVC_COST_DETL_LOC.count<>0 then
                   forall i in 1..LP_REC_SVC_COST_DETL_LOC.count SAVE EXCEPTIONS
                        insert into SVC_COST_SUSP_SUP_DETAIL_LOC  values LP_REC_SVC_COST_DETL_LOC(i);
             end if;

              --  commit;
                  LP_REC_SVC_COST_HEAD.delete();
                  LP_REC_SVC_COST_DETAIL.delete();
                  LP_REC_SVC_CFA_EXT.delete();
                  LP_REC_SVC_COST_DETL_LOC.delete();
                  key_col_array.delete();
                  attr_col_array.delete();
 --        end if;
     end loop;
  close c_svc_promo_get;
 end if; 
  return true;
EXCEPTION
   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

        WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     promo_temp_rec.chunk_id,
                     L_table,
                     promo_temp_rec.row_seq,
                     'CRC_CORESVC_MULTIBUY_PROMO.CREATE_CC_GET',
                     O_error_message);

     return false;
END create_cc_get;	
----------------------------------------------------------------------------------------------------
    FUNCTION create_cc_buy (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      NUMBER,
        i_chunk_id        IN      NUMBER
    ) RETURN BOOLEAN IS

        l_program           VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_CC_BUY';
        l_table             svc_admin_upld_er.table_name%TYPE := 'SVC_COST_SUSP_SUP_HEAD';
        l_process_id        svc_cost_susp_sup_head.process_id%TYPE;
        l_chunk_id          svc_cost_susp_sup_head.chunk_id%TYPE;
        l_active_date       svc_cost_susp_sup_head.active_date%TYPE;
        l_end_date          svc_cost_susp_sup_head.active_date%TYPE;
        l_item              svc_cost_susp_sup_detail.item%TYPE;
        l_supplier          svc_cost_susp_sup_detail.supplier%TYPE;
        l_cc_reason         svc_cost_susp_sup_head.reason%TYPE;
        l_cost_change       svc_cost_susp_sup_detail.cost_change%TYPE;
        promo_temp_rec      c_svc_promo_buy%rowtype;
        l_unit_cost         svc_cost_susp_sup_detail.unit_cost%TYPE;
        l_cost_change_seq   svc_cost_susp_sup_detail.cost_change%TYPE;
        l_row_seq           NUMBER(10);
        l_group_id          NUMBER(10);
        hcost_row_seq       NUMBER(10) := 0;
        dcost_row_seq       NUMBER(10) := 0;
        cfa_cost_row_seq    NUMBER(10) := 0;
        dloc_cost_row_seq   NUMBER(10) := 0;
        l_error_count       NUMBER(2) := 0;
        i_promo_id          NUMBER(10);
        l_tot_error_cnt     NUMBER(10);
        l_warning_cnt       NUMBER(10);
        l_item_cost         NUMBER(1);
        l_zone_id           NUMBER(4);
        key_col_array       key_val_pairs := key_val_pairs();
        attr_col_array      key_val_pairs := key_val_pairs();
        TYPE cost_cre_record IS RECORD (
            process_id    svc_cost_susp_sup_head.process_id%TYPE,
            cost_change   svc_cost_susp_sup_head.cost_change%TYPE,
            active_date   svc_cost_susp_sup_head.active_date%TYPE,
            supplier      svc_cost_susp_sup_detail.supplier%TYPE,
            cc_reason     svc_cost_susp_sup_head.reason%TYPE
        );
        TYPE cost_cre_record_tab IS
            TABLE OF cost_cre_record;
        l_cost_cre          cost_cre_record_tab;
        CURSOR c_zone_loc IS
        SELECT
            zone_id,
            location
        FROM
            rpm_zone_location
        WHERE
            zone_id = l_zone_id
            AND location IN (
                SELECT
                    loc
                FROM
                    item_supp_country_loc
                WHERE
                    item = promo_temp_rec.item
                    AND supplier = promo_temp_rec.supplier
                    AND origin_country_id = 'TH'
            );

        CURSOR c_svc_cost (
            l_process_id    NUMBER,
            l_chunk_id      NUMBER,
            l_supplier      NUMBER,
            l_cc_reason     NUMBER,
            l_active_date   DATE
        ) IS
        ( SELECT DISTINCT
            chead.process_id,
            chead.cost_change,
            chead.active_date,
            cdetail.supplier,
            chead.reason
        FROM
            svc_cost_susp_sup_head     chead,
            svc_cost_susp_sup_detail   cdetail
        WHERE
            chead.process_id = cdetail.process_id
            AND chead.chunk_id = cdetail.chunk_id
            AND chead.cost_change = cdetail.cost_change
            AND chead.process_id = l_process_id
            AND chead.chunk_id = l_chunk_id
            AND chead.active_date = l_active_date
            AND cdetail.supplier = l_supplier
            AND chead.reason = l_cc_reason
            AND chead.status = 'S'
        UNION
        SELECT DISTINCT
            chead.process_id,
            chead.cost_change,
            chead.active_date,
            cdetail.supplier,
            chead.reason
        FROM
            svc_cost_susp_sup_head         chead,
            svc_cost_susp_sup_detail_loc   cdetail
        WHERE
            chead.process_id = cdetail.process_id
            AND chead.chunk_id = cdetail.chunk_id
            AND chead.cost_change = cdetail.cost_change
            AND chead.process_id = l_process_id
            AND chead.chunk_id = l_chunk_id
            AND chead.active_date = l_active_date
            AND cdetail.supplier = l_supplier
            AND chead.reason = l_cc_reason
            AND chead.status = 'S'
        );

    BEGIN
        IF lp_errors_tab.count() = 0 THEN
/*      HCOST_ROW_SEQ:=10;
      DCOST_ROW_SEQ:=10;
      CFA_COST_ROW_SEQ:=10;
      DLOC_COST_ROW_SEQ:=10;
  */
            SELECT
                nvl(MAX(row_seq), 0)
            INTO hcost_row_seq
            FROM
                svc_cost_susp_sup_head
            WHERE
                process_id = i_process_id;

            SELECT
                nvl(MAX(row_seq), 0)
            INTO dcost_row_seq
            FROM
                svc_cost_susp_sup_detail
            WHERE
                process_id = i_process_id;

            SELECT
                nvl(MAX(row_seq), 0)
            INTO cfa_cost_row_seq
            FROM
                svc_cfa_ext
            WHERE
                process_id = i_process_id;

            SELECT
                nvl(MAX(row_seq), 0)
            INTO dloc_cost_row_seq
            FROM
                svc_cost_susp_sup_detail_loc
            WHERE
                process_id = i_process_id;

            OPEN c_svc_promo_buy(i_process_id, i_chunk_id);
            LOOP
                FETCH c_svc_promo_buy INTO promo_temp_rec;
                EXIT WHEN c_svc_promo_buy%notfound;
                l_cost_cre := NEW cost_cre_record_tab();
                l_process_id := promo_temp_rec.process_id;
                l_chunk_id := promo_temp_rec.chunk_id;
                l_supplier := promo_temp_rec.supplier;
                l_active_date := promo_temp_rec.cc_start_date;
                l_end_date := promo_temp_rec.cc_end_date;
                SELECT
                    group_id
                INTO l_group_id
                FROM
                    cfa_attrib
                WHERE
                    view_col_name = 'V_CRC_CC_ATTR';

                IF ( promo_temp_rec.cc_start_date IS NOT NULL AND promo_temp_rec.cc_amount IS NOT NULL ) THEN
                    SELECT DISTINCT
                        promo_id
                    INTO i_promo_id
                    FROM
                        rpm_stage_multibuy_header
                    WHERE
                        comments = to_char(i_process_id);

                    lp_rec_svc_cost_head := NEW svc_cost_head();
                    lp_rec_svc_cost_detail := NEW svc_cost_detail();
                    lp_rec_svc_cfa_ext := NEW tab_svc_cfa_ext();
                    lp_rec_svc_cost_detl_loc := NEW svc_cost_detail_loc();
                    key_col_array := NEW key_val_pairs();
                    attr_col_array := NEW key_val_pairs();
                    l_cc_reason := 11;
                    OPEN c_svc_cost(l_process_id, l_chunk_id, l_supplier, l_cc_reason, l_active_date);
                    FETCH c_svc_cost BULK COLLECT INTO l_cost_cre;
                    CLOSE c_svc_cost;
                    IF l_cost_cre.count = 0 THEN
                        hcost_row_seq := hcost_row_seq + 1;
                        dcost_row_seq := dcost_row_seq + 1;
                        cfa_cost_row_seq := cfa_cost_row_seq + 1;
                        dloc_cost_row_seq := dloc_cost_row_seq + 1;
                        write_svc_cost_head(promo_temp_rec.process_id, promo_temp_rec.chunk_id, hcost_row_seq, coresvc_costchg_eseq
                        .nextval, promo_temp_rec.promo_name,
                                            l_cc_reason, promo_temp_rec.cc_start_date);

                        l_cost_change_seq := coresvc_costchg_eseq.currval;
                        key_col_array.extend();
                        key_col_array(key_col_array.count()) := key_value_pair('COST_CHANGE', to_char(coresvc_costchg_eseq.currval
                        ));

                        attr_col_array.extend();
                        attr_col_array(attr_col_array.count()) := key_value_pair('V_CRC_CC_ATTR', to_char(i_promo_id));

                        attr_col_array.extend();
                        attr_col_array(attr_col_array.count()) := key_value_pair('ROW_SEQ', to_char(hcost_row_seq));

                        write_svc_cfa_ext(promo_temp_rec.process_id, promo_temp_rec.chunk_id, cfa_cost_row_seq, 'COST_SUSP_SUP_HEAD'
                        , key_col_array,
                                          attr_col_array);

                        IF promo_temp_rec.loc_type = 'A' THEN
                            write_svc_cost_detail(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dcost_row_seq, coresvc_costchg_eseq
                            .currval, promo_temp_rec.supplier,
                                                  promo_temp_rec.item, promo_temp_rec.cc_amount);
                        ELSIF promo_temp_rec.loc_type = 'S' THEN
                            write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq, coresvc_costchg_eseq
                            .currval, promo_temp_rec.supplier,
                                                      promo_temp_rec.item, promo_temp_rec.location, promo_temp_rec.cc_amount);
                        ELSIF promo_temp_rec.loc_type = 'Z' THEN
                            SELECT
                                zone_id
                            INTO l_zone_id
                            FROM
                                rpm_zone
                            WHERE
                                zone_display_id = promo_temp_rec.location;

                            FOR rec IN c_zone_loc LOOP
                                write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq, coresvc_costchg_eseq
                                .currval, promo_temp_rec.supplier,
                                                          promo_temp_rec.item, rec.location, promo_temp_rec.cc_amount);

                                dloc_cost_row_seq := dloc_cost_row_seq + 1;
                            END LOOP;

                        END IF;

                    ELSE
                        l_cost_change := l_cost_cre(1).cost_change;
                        IF promo_temp_rec.loc_type = 'A' THEN
                            SELECT
                                COUNT(1)
                            INTO l_item_cost
                            FROM
                                svc_cost_susp_sup_detail
                            WHERE
                                cost_change = l_cost_change
                                AND item = promo_temp_rec.item;

                            IF l_item_cost = 0 THEN
                                dcost_row_seq := dcost_row_seq + 1;
                                write_svc_cost_detail(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dcost_row_seq, l_cost_change
                                , promo_temp_rec.supplier,
                                                      promo_temp_rec.item, promo_temp_rec.cc_amount);

                            END IF;

                        ELSIF promo_temp_rec.loc_type = 'S' THEN
                            SELECT
                                COUNT(1)
                            INTO l_item_cost
                            FROM
                                svc_cost_susp_sup_detail_loc
                            WHERE
                                cost_change = l_cost_change
                                AND item = promo_temp_rec.item
                                AND loc = promo_temp_rec.location;

                            IF l_item_cost = 0 THEN
                                dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq, l_cost_change
                                , promo_temp_rec.supplier,
                                                          promo_temp_rec.item, promo_temp_rec.location, promo_temp_rec.cc_amount)
                                                          ;

                            END IF;

                        ELSIF promo_temp_rec.loc_type = 'Z' THEN
                            SELECT
                                zone_id
                            INTO l_zone_id
                            FROM
                                rpm_zone
                            WHERE
                                zone_display_id = promo_temp_rec.location;

                            FOR rec IN c_zone_loc LOOP
                                SELECT
                                    COUNT(1)
                                INTO l_item_cost
                                FROM
                                    svc_cost_susp_sup_detail_loc
                                WHERE
                                    cost_change = l_cost_change
                                    AND item = promo_temp_rec.item
                                    AND loc = rec.location;

                                IF l_item_cost = 0 THEN
                                    write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq
                                    , l_cost_change, promo_temp_rec.supplier,
                                                              promo_temp_rec.item, rec.location, promo_temp_rec.cc_amount);

                                    dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                END IF;

                            END LOOP;

                        END IF;

                    END IF;

                    key_col_array.DELETE();
                    attr_col_array.DELETE();
                    IF l_end_date IS NOT NULL THEN
                        key_col_array := NEW key_val_pairs();
                        attr_col_array := NEW key_val_pairs();
                        l_cc_reason := 12;
                        l_cost_cre.extend();
                        l_end_date := l_end_date + 1;
                        OPEN c_svc_cost(l_process_id, l_chunk_id, l_supplier, l_cc_reason, l_end_date);
                        FETCH c_svc_cost BULK COLLECT INTO l_cost_cre;
                        CLOSE c_svc_cost;
                        IF l_cost_cre.count = 0 THEN
                            hcost_row_seq := hcost_row_seq + 1;
                            dcost_row_seq := dcost_row_seq + 1;
                            cfa_cost_row_seq := cfa_cost_row_seq + 1;
                            dloc_cost_row_seq := dloc_cost_row_seq + 1;
                            write_svc_cost_head(promo_temp_rec.process_id, promo_temp_rec.chunk_id, hcost_row_seq, coresvc_costchg_eseq
                            .nextval, promo_temp_rec.promo_name,
                                                l_cc_reason, l_end_date);

                            key_col_array.extend();
                            key_col_array(key_col_array.count()) := key_value_pair('COST_CHANGE', to_char(coresvc_costchg_eseq.currval
                            ));

                            attr_col_array.extend();
                            attr_col_array(attr_col_array.count()) := key_value_pair('V_CRC_CC_ATTR', to_char(i_promo_id));

                            attr_col_array.extend();
                            attr_col_array(attr_col_array.count()) := key_value_pair('ROW_SEQ', to_char(hcost_row_seq));

                            write_svc_cfa_ext(promo_temp_rec.process_id, promo_temp_rec.chunk_id, cfa_cost_row_seq, 'COST_SUSP_SUP_HEAD'
                            , key_col_array,
                                              attr_col_array);

                            IF promo_temp_rec.loc_type = 'A' THEN
                                SELECT
                                    unit_cost
                                INTO l_unit_cost
                                FROM
                                    item_supp_country
                                WHERE
                                    item = promo_temp_rec.item
                                    AND supplier = promo_temp_rec.supplier
                                    AND origin_country_id = 'TH';

                                write_svc_cost_detail(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dcost_row_seq, coresvc_costchg_eseq
                                .currval, promo_temp_rec.supplier,
                                                      promo_temp_rec.item, l_unit_cost);

                            ELSIF promo_temp_rec.loc_type = 'S' THEN
                                SELECT
                                    unit_cost
                                INTO l_unit_cost
                                FROM
                                    item_supp_country_loc
                                WHERE
                                    item = promo_temp_rec.item
                                    AND supplier = promo_temp_rec.supplier
                                    AND origin_country_id = 'TH'
                                    AND loc = promo_temp_rec.location;

                                write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq,
                                coresvc_costchg_eseq.currval, promo_temp_rec.supplier,
                                                          promo_temp_rec.item, promo_temp_rec.location, l_unit_cost);

                            ELSIF promo_temp_rec.loc_type = 'Z' THEN
                                SELECT
                                    zone_id
                                INTO l_zone_id
                                FROM
                                    rpm_zone
                                WHERE
                                    zone_display_id = promo_temp_rec.location;

                                FOR rec IN c_zone_loc LOOP
                                    SELECT
                                        unit_cost
                                    INTO l_unit_cost
                                    FROM
                                        item_supp_country_loc
                                    WHERE
                                        item = promo_temp_rec.item
                                        AND supplier = promo_temp_rec.supplier
                                        AND origin_country_id = 'TH'
                                        AND loc = rec.location;

                                    write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq
                                    , coresvc_costchg_eseq.currval, promo_temp_rec.supplier,
                                                              promo_temp_rec.item, rec.location, l_unit_cost);

                                    dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                END LOOP;

                            END IF;

                        ELSE
                            l_cost_change := l_cost_cre(1).cost_change;
                            IF promo_temp_rec.loc_type = 'A' THEN
                                SELECT
                                    COUNT(1)
                                INTO l_item_cost
                                FROM
                                    svc_cost_susp_sup_detail
                                WHERE
                                    cost_change = l_cost_change
                                    AND item = promo_temp_rec.item;

                                IF l_item_cost = 0 THEN
                                    SELECT
                                        unit_cost
                                    INTO l_unit_cost
                                    FROM
                                        item_supp_country
                                    WHERE
                                        item = promo_temp_rec.item
                                        AND supplier = promo_temp_rec.supplier
                                        AND origin_country_id = 'TH';

                                    dcost_row_seq := dcost_row_seq + 1;
                                    write_svc_cost_detail(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dcost_row_seq, l_cost_change
                                    , promo_temp_rec.supplier,
                                                          promo_temp_rec.item, l_unit_cost);

                                END IF;

                            ELSIF promo_temp_rec.loc_type = 'S' THEN
                                SELECT
                                    COUNT(1)
                                INTO l_item_cost
                                FROM
                                    svc_cost_susp_sup_detail_loc
                                WHERE
                                    cost_change = l_cost_change
                                    AND item = promo_temp_rec.item
                                    AND loc = promo_temp_rec.location;

                                IF l_item_cost = 0 THEN
                                    SELECT
                                        unit_cost
                                    INTO l_unit_cost
                                    FROM
                                        item_supp_country_loc
                                    WHERE
                                        item = promo_temp_rec.item
                                        AND supplier = promo_temp_rec.supplier
                                        AND origin_country_id = 'TH'
                                        AND loc = promo_temp_rec.location;

                                    dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                    write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq
                                    , l_cost_change, promo_temp_rec.supplier,
                                                              promo_temp_rec.item, promo_temp_rec.location, l_unit_cost);

                                END IF;

                            ELSIF promo_temp_rec.loc_type = 'Z' THEN
                                SELECT
                                    zone_id
                                INTO l_zone_id
                                FROM
                                    rpm_zone
                                WHERE
                                    zone_display_id = promo_temp_rec.location;

                                FOR rec IN c_zone_loc LOOP
                                    SELECT
                                        COUNT(1)
                                    INTO l_item_cost
                                    FROM
                                        svc_cost_susp_sup_detail_loc
                                    WHERE
                                        cost_change = l_cost_change
                                        AND item = promo_temp_rec.item
                                        AND loc = rec.location;

                                    IF l_item_cost = 0 THEN
                                        SELECT
                                            unit_cost
                                        INTO l_unit_cost
                                        FROM
                                            item_supp_country_loc
                                        WHERE
                                            item = promo_temp_rec.item
                                            AND supplier = promo_temp_rec.supplier
                                            AND origin_country_id = 'TH'
                                            AND loc = rec.location;

                                        write_svc_cost_detail_loc(promo_temp_rec.process_id, promo_temp_rec.chunk_id, dloc_cost_row_seq
                                        , l_cost_change, promo_temp_rec.supplier,
                                                                  promo_temp_rec.item, rec.location, l_unit_cost);

                                        dloc_cost_row_seq := dloc_cost_row_seq + 1;
                                    END IF;

                                END LOOP;

                            END IF;

                        END IF;

                    END IF;

                    IF lp_rec_svc_cost_head.count <> 0 THEN
                        FORALL i IN 1..lp_rec_svc_cost_head.count SAVE EXCEPTIONS
                            INSERT INTO svc_cost_susp_sup_head VALUES lp_rec_svc_cost_head ( i );

                    END IF;

                    IF lp_rec_svc_cost_detail.count <> 0 THEN
                        FORALL i IN 1..lp_rec_svc_cost_detail.count SAVE EXCEPTIONS
                            INSERT INTO svc_cost_susp_sup_detail VALUES lp_rec_svc_cost_detail ( i );

                    END IF;

                    IF lp_rec_svc_cfa_ext.count <> 0 THEN
                        FORALL i IN 1..lp_rec_svc_cfa_ext.count SAVE EXCEPTIONS
                            INSERT INTO svc_cfa_ext VALUES lp_rec_svc_cfa_ext ( i );

                    END IF;

                    IF lp_rec_svc_cost_detl_loc.count <> 0 THEN
                        FORALL i IN 1..lp_rec_svc_cost_detl_loc.count SAVE EXCEPTIONS
                            INSERT INTO svc_cost_susp_sup_detail_loc VALUES lp_rec_svc_cost_detl_loc ( i );

                    END IF;

              -- commit;

                    lp_rec_svc_cost_head.DELETE();
                    lp_rec_svc_cost_detail.DELETE();
                    lp_rec_svc_cfa_ext.DELETE();
                    lp_rec_svc_cost_detl_loc.DELETE();
                    key_col_array.DELETE();
                    attr_col_array.DELETE();
                END IF;

            END LOOP;

            CLOSE c_svc_promo_buy;
        END IF;

        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            write_error(i_process_id, svc_admin_upld_er_seq.nextval, promo_temp_rec.chunk_id, l_table, promo_temp_rec.row_seq,
                        'CRC_CORESVC_MULTIBUY_PROMO.CREATE_CC_BUY', o_error_message);

            RETURN false;
    END create_cc_buy;----------------------------------------------------------------------------------------------------
function COST_CHG_EXE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       NUMBER,
                            I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.COST_CHG_EXE';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_SUSP_SUP_HEAD'; 
   L_PROCESS_ID   SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%type;   
   L_tot_error_cnt number(10);
   L_warning_cnt number(10);

  cursor C_ERRORS_EXIST is
      select COUNT(error_type) tot_err_count,
             NVL(SUM(DECODE(error_type, 'W', 1, 0)), 0) warn_count
        from coresvc_costchg_err
       where process_id = I_process_id;

     cursor C_ERRORS_EXIST_ERR_CHK is
       select   stg.process_id,
                stg.row_seq,stg.item,
                cost_error.error_msg ,
                cost_error.row_seq cost_err_seq ,
                 cost_error.table_name
       from CRC_SVC_MULTIBUY_BUY_STG stg,    
         (select distinct  cssh.process_id ,
                          cssh.cost_change,cssh.row_seq, 
                          cc_err.error_msg,
                          cssdl.item item ,
                         cssh.active_date,cc_err.table_name
                 from svc_cost_susp_sup_head cssh,
                      svc_cost_susp_sup_detail_loc cssdl, 
                       coresvc_costchg_err cc_err
                 where   cssh.process_id=cssdl.process_id
                 and cssh.cost_change=cssdl.cost_change
                 and cc_err.process_id=cssh.process_id
                 and cc_err.process_id=cssh.process_id
                and cc_err.row_seq=cssh.row_seq
                 union
               select distinct  cssh.process_id ,
                                cssh.cost_change,cssh.row_seq,
                                cc_err.error_msg,
                                cssdl.item item ,
                                cssh.active_date ,cc_err.table_name
                 from svc_cost_susp_sup_head cssh,
                     svc_cost_susp_sup_detail cssdl,
                      coresvc_costchg_err cc_err
                  where   cssh.process_id=cssdl.process_id
                  and cssh.cost_change=cssdl.cost_change
                  and cc_err.process_id=cssh.process_id
                  and cc_err.process_id=cssh.process_id
                  and cc_err.row_seq=cssh.row_seq  
        ) cost_error where stg.process_id=cost_error.process_id
                     and stg.item=cost_error.item 
                     and  cost_error.process_id = I_process_id;

begin 

    if  LP_errors_tab.COUNT()=0 
    then

  L_PROCESS_ID:=I_process_id;

  if CORESVC_COSTCHG.PROCESS(O_error_message  ,
                    L_PROCESS_ID ) = FALSE then

       return false;

   end if;

   open C_ERRORS_EXIST;
   fetch C_ERRORS_EXIST into L_tot_error_cnt,
                             L_warning_cnt;
   close C_ERRORS_EXIST;


   if L_tot_error_cnt <>0 or  L_warning_cnt <>0 then

    FOR i IN C_ERRORS_EXIST_ERR_CHK
         LOOP
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         i.table_name,
                         i.row_seq,
                         L_program,
                         i.error_msg);


         END LOOP;

      end if;
 end if;     
  return true;

EXCEPTION
   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

        WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     1,
                     L_program,
                     O_error_message);
       return false;

END COST_CHG_EXE;
-----------------------------------------------------------------------------------------------------

    FUNCTION process_promo_hdr (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      crc_svc_multibuy_hdr_stg.process_id%TYPE,
        i_chunk_id        IN      crc_svc_multibuy_hdr_stg.chunk_id%TYPE
    ) RETURN BOOLEAN IS

        l_program                     VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO_HDR.PROCESS_PROMO_HDR';
        l_table                       svc_admin_upld_er.table_name%TYPE := 'HDR_DATA';
        l_error                       BOOLEAN;
        l_approve                     BOOLEAN;
        l_process_error               BOOLEAN := false;
        l_error_message               VARCHAR2(600);
        promo_temp_rec                crc_svc_multibuy_hdr_stg%rowtype;
        l_loc_range                   NUMBER(3) := 0;
        l_store                       NUMBER(1) := 0;
        l_st_open_date                DATE;
        l_st_close_date               DATE;
        l_zone                        NUMBER(1);
        l_forecast_gr                 NUMBER(2);
        l_mechanic                    NUMBER(2);
        l_cust_seg                    NUMBER(2);
        l_sup                         NUMBER(1);
        l_item_sup                    NUMBER(1);
        l_sup_status                  NUMBER(1);
        l_cc_start_check              NUMBER(1);
        l_cc_end_check                NUMBER(1);
        l_duplicate_check             NUMBER(2);
        l_item_ranging                NUMBER(2);
        l_item_zone_range             NUMBER(2);
        l_compare_cost                NUMBER(1);
        l_compare_retail              NUMBER(1);
        l_promo_comp_custattr         NUMBER(15);
        l_promo_comp_custattrexist    NUMBER(2);
        l_stg_comp_custattrid         NUMBER(15);
        l_stg_comp_custattrexists     NUMBER(2);
        l_promo_dtl_custattr          NUMBER(15);
        l_promo_dtl_custattrexist     NUMBER(2);
        l_stg_dtl_custattrid          NUMBER(15);
        l_stg_dtl_custattrexists      NUMBER(2);
        l_stg_promo_eventid           NUMBER(6) := NULL;
        l_stg_zone_grpid              NUMBER(4) := NULL;
        stg_multibuy_temp_rec         rpm_stage_multibuy_header%rowtype;
        o_deal_id                     NUMBER(10);
        o_deal_detail_id              NUMBER(10);
        l_zone_id                     NUMBER(10);
        l_zone_idexists               NUMBER(2);
        l_cust_count                  NUMBER(3);
        l_promo_event                 NUMBER(1);
        l_exist_spromo                NUMBER(2);
        l_stg_buylist_id              NUMBER(10);

    BEGIN
        FOR rec IN c_svc_promo_hdr(i_process_id, i_chunk_id) LOOP
            l_error := false;
            l_process_error := false;
           ----**** ---- Valdiation check against values  ----**** ----
            BEGIN
               --- Duplicate valdiation
                SELECT
                    COUNT(1)
                INTO l_exist_spromo
                FROM
                    rpm_promo_dtl              rpd,
                    rpm_promo_dtl_merch_node   mrch,
                    rpm_promo_comp             rpc,
                    rpm_promo_zone_location    rploc
                WHERE
                    rpd.promo_dtl_id = mrch.promo_dtl_id
                    AND rpd.promo_dtl_id = rploc.promo_dtl_id
                    AND rpc.promo_comp_id = rpd.promo_comp_id
                    AND rpd.state NOT IN (
                        0,
                        1,
                        2,
                        4
                    )
                    AND rploc.location = rec.location
                    AND to_date(rpd.start_date) = rec.promo_start_date
                    AND to_date(rpd.end_date) = rec.promo_end_date
                    AND rpc.type = 1;

                SELECT
                    COUNT(1)
                INTO l_mechanic
                FROM
                    code_detail
                WHERE
                    code_type = 'XRMC'
                    AND UPPER(code_desc) = rec.mechanic;

                SELECT
                    COUNT(1)
                INTO l_forecast_gr
                FROM
                    code_detail
                WHERE
                    code_type = 'XRFG'
                    AND UPPER(code_desc) = rec.forecast_group;

                SELECT
                    COUNT(1)
                INTO l_cust_seg
                FROM
                    customer_segments
                WHERE
                    customer_segment_id = rec.cust_segment;

               ---Validate promo_type

                SELECT
                    COUNT(1)
                INTO l_promo_event
                FROM
                    rpm_promo_event
                WHERE
                    UPPER(description) = rec.promo_type;

               ---- Based on lacation type

                SELECT
                    store_open_date,
                    store_close_date
                INTO
                    l_st_open_date,
                    l_st_close_date
                FROM
                    store
                WHERE
                    store = rec.location;

                SELECT
                    COUNT(store)
                INTO l_store
                FROM
                    store
                WHERE
                    store = rec.location;

                SELECT
                    COUNT(zone_id)
                INTO l_item_zone_range
                FROM
                    (
                        SELECT
                            rzl.zone_id
                        FROM
                            rpm_zone_group      rzg,
                            rpm_zone            rz,
                            rpm_zone_location   rzl
                        WHERE --rzg.name = 'All Stores'
                            rz.zone_group_id = 1
                            AND rzg.zone_group_id = rz.zone_group_id
                            AND rz.zone_id = rzl.zone_id
                            AND rz.currency_code = 'THB'
                        GROUP BY
                            rzl.zone_id
                        MINUS
                        SELECT
                            rzl.zone_id
                        FROM
                            rpm_zone_group      rzg,
                            rpm_zone            rz,
                            rpm_zone_location   rzl,
                            item_loc            il
                        WHERE --rzg.name = 'All Stores'
                            rz.zone_group_id = 1
                            AND rzg.zone_group_id = rz.zone_group_id
                            AND rz.zone_id = rzl.zone_id
                            AND rzl.location = il.loc
                            AND rz.currency_code = 'THB'
                               -- AND il.item = rec.item
                        GROUP BY
                            rzl.zone_id
                    ) zone_count;

                IF ( rec.loc_type = 'Z' ) THEN
                    SELECT
                        COUNT(1)
                    INTO l_zone
                    FROM
                        rpm_zone
                    WHERE
                        zone_display_id = rec.location;

                END IF;

            EXCEPTION
                WHEN OTHERS THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'Error for Validation Values', sqlerrm);

                    l_error := true;
            END;
           ---End  Valdiation check against values ----

		  ---Valdiating business checks for values

            IF rec.promo_name IS NOT NULL AND length(rec.promo_name) > 160 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_NAME', 'CRC_VARCHAR2_TOO_LONG');

                l_error := true;
            END IF;

            IF l_promo_event = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_TYPE', 'CRC_NO_VALID_PROMO_EVENT');

                l_error := true;
            END IF;

            IF rec.promo_start_date IS NULL OR rec.promo_start_date < to_date(get_vdate()) THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_START_DATE', 'CRC_INVALID_START_DATE');

                l_error := true;
            END IF;

            IF rec.promo_start_date > rec.promo_end_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_START_DATE', 'VAL_START_DATE');

                l_error := true;
            END IF;

            IF l_exist_spromo >= 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_START_DATE', 'CRC_MULTIBUY_PROMO_EXIST');

                l_error := true;
            END IF;

            IF rec.promo_end_date IS NULL OR rec.promo_end_date < rec.promo_start_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'PROMO_END_DATE', 'CRC_INVALID_END_DATE');

                l_error := true;
            END IF;

            IF l_mechanic = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'MECHANIC', 'CRC_INVALID_MECHANIC');

                l_error := true;
            END IF;

            IF l_forecast_gr = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'FORECAST_GROUP', 'CRC_INVALID_FORECAST_GRP');

                l_error := true;
            END IF;

            IF rec.cust_segment IS NOT NULL AND l_cust_seg = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CUST_SEGMENT', 'CUS_SMT_ID_NOT_EXIST ');

                l_error := true;
            END IF;

            IF rec.loc_type = 'A' AND rec.location IS NOT NULL THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'LOC_TYPE', 'CRC_ALL_LOC');

                l_error := true;
            END IF;

            IF rec.loc_type = 'S' AND l_store = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'LOCATION', 'INV_STORE');

                l_error := true;
            END IF;

            IF rec.promo_start_date < l_st_open_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'LOCATION', 'CRC_LOC_OPEN_B4_START_DT');

                l_error := true;
            END IF;

            IF rec.promo_end_date > l_st_close_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'LOCATION', 'CRC_LOC_CLS_DT_B4_END_DT');

                l_error := true;
            END IF;

            IF l_zone = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'LOCATION', 'INV_PRICE_ZONE');

                l_error := true;
            END IF;

         if (rec.RIGHT_EARN_IND is not null and (coalesce(rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.STICKER_SPEND_IND,
                    rec.COUPON_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS
                    ) is not null ))
              or (rec.RIGHT_SPEND_IND is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.STICKER_EARN_IND,
                    rec.STICKER_SPEND_IND,
                    rec.COUPON_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS
                    ) is not null ))
             or (rec.STICKER_EARN_IND is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_SPEND_IND,
                    rec.COUPON_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS
                    ) is not null )) 
             or (rec.STICKER_SPEND_IND is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.COUPON_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS
                    ) is not null  ) ) 
             or (rec.COUPON_SPEND_IND is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.STICKER_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS 
                    ) is not null  )) 
            or (rec.STICKER_SPEND_IND is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.COUPON_SPEND_IND,
                    rec.EXTRA_POINTS,
                    rec.SUPERBURN_POINTS
                    ) is not null  ) ) 
            or (rec.EXTRA_POINTS is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.COUPON_SPEND_IND,
                    rec.STICKER_SPEND_IND,
                    rec.SUPERBURN_POINTS
                    ) is not null  )) 
             or (rec.SUPERBURN_POINTS is not null and (coalesce(rec.RIGHT_EARN_IND,
                    rec.RIGHT_SPEND_IND,
                    rec.STICKER_EARN_IND,
                    rec.COUPON_SPEND_IND,
                    rec.STICKER_SPEND_IND,
                    rec.EXTRA_POINTS
                    ) is not null ))
                then                   
                      WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CUST ATTRIBUTES ERROR',
                        'CRC_CUSTOM_ATTR_ERR');
           L_error :=TRUE;
        end if;

        END LOOP;

        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END process_promo_hdr;	
-----------------------------------------------------------------------------------------------------

    FUNCTION process_promo_buy (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      crc_svc_multibuy_buy_stg.process_id%TYPE,
        i_chunk_id        IN      crc_svc_multibuy_buy_stg.chunk_id%TYPE
    ) RETURN BOOLEAN IS

        l_program                     VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.PROCESS_PROMO_BUY';
        l_table                       svc_admin_upld_er.table_name%TYPE := 'BUY_DATA';
        l_error                       BOOLEAN;
        l_approve                     BOOLEAN;
        l_process_error               BOOLEAN := false;
        l_error_message               VARCHAR2(600);
        promo_temp_rec                crc_svc_multibuy_buy_stg%rowtype;
        l_item_count                  NUMBER(1) := 0;
        l_item_status                 VARCHAR2(1);
        l_tran_level                  NUMBER(2) := 0;
        l_sellable                    VARCHAR2(1);
        l_loc_range                   NUMBER(3) := 0;
        l_store                       NUMBER(1) := 0;
        l_item_loc                    VARCHAR2(1);
        l_il_status                   VARCHAR2(1);
        l_zone                        NUMBER(1);
        l_forecast_gr                 NUMBER(2);
        l_mechanic                    NUMBER(2);
        l_cust_seg                    NUMBER(2);
        l_sup                         NUMBER(1);
        l_item_sup                    NUMBER(1);
        l_sup_status                  NUMBER(1);
        l_cc_start_check              NUMBER(1);
        l_cc_end_check                NUMBER(1);
        l_item_ranging                NUMBER(2);
        l_item_zone_range             NUMBER(2);
        l_compare_cost                NUMBER(1);
        l_compare_retail              NUMBER(1);
        l_stg_promo_eventid           NUMBER(6) := NULL;
        l_stg_zone_grpid              NUMBER(4) := NULL;
        stg_multibuy_buy_temp_rec     rpm_stage_multibuy_buylist%rowtype;
        o_deal_id                     NUMBER(10);
        o_deal_detail_id              NUMBER(10);
        l_item_cnt_check              NUMBER(2);
        l_zone_id                     NUMBER(10);
        l_zone_idexists               NUMBER(2);
        l_cust_count                  NUMBER(3);
        l_promo_event                 NUMBER(1);
        l_exist_spromo                NUMBER(2);
        l_stg_buylist_id              NUMBER(15);   
        l_skulist_count               NUMBER(1) := 0;
        l_skulist_item_exists         NUMBER(1) := 0;
        l_buygrp_count                NUMBER(1) := 0;
        l_ml_item_count               NUMBER(1) := 0;   
        l_sku_or_item                 NUMBER(1) := 0; 
        l_unit_cost                   NUMBER(10); 
        l_itm_cnt                     NUMBER(5);
        L_max_buy_lists NUMBER := NULL;

    BEGIN
        FOR rec IN c_svc_promo_buy(i_process_id, i_chunk_id) LOOP
            l_error := false;
            l_process_error := false;
           ----**** ---- Valdiation check against values  ----**** ----
            BEGIN
                IF rec.skulist is not null 
                Then
                   -- SkuList Validations
                    SELECT
                        COUNT(skulist)
                    INTO l_skulist_count
                    FROM
                        skulist_head
                    WHERE
                        skulist = rec.skulist;
    
                    SELECT
                        COUNT(1)
                    INTO l_skulist_item_exists
                    FROM
                        skulist_detail
                    WHERE
                        skulist = rec.skulist
                    AND item = rec.item;      
                
                END IF;

               select count(1)
                INTO l_buygrp_count
                from crc_svc_multibuy_buy_stg bystg1
                where process_id= rec.process_id
                and buylist_group  = rec.buylist_group
                and skulist is null
                and exists
                (select 1 from crc_svc_multibuy_buy_stg bystg2
                       where bystg2.process_id=bystg1.process_id
                       and bystg2.buylist_group=bystg1.buylist_group
                        and skulist is null );            


        SELECT
            COUNT(1)
        INTO l_ml_item_count
        FROM
            crc_svc_multibuy_buy_stg bystg1
        WHERE
            process_id = rec.process_id
            AND buylist_group = rec.buylist_group
            AND skulist IS NULL
            AND EXISTS (
                SELECT
                    1
                FROM
                    crc_svc_multibuy_buy_stg bystg2
                WHERE
                    bystg2.process_id = bystg1.process_id
                    AND bystg2.buylist_group = bystg1.buylist_group
                    AND skulist IS NULL
            );

    			-- Item Validations
                SELECT
                    COUNT(item)
                INTO l_item_count
                FROM
                    item_master
                WHERE
                    item = rec.item;


                SELECT
                    status,
                    sellable_ind
                INTO
                    l_item_status,
                    l_sellable
                FROM
                    item_master
                WHERE
                    item = rec.item;


                SELECT
                    COUNT(1)
                INTO l_tran_level
                FROM
                    item_master
                WHERE
                    item = rec.item
                    AND item_level <= tran_level;


 	 ---- Based on lacation type

                IF rec.loc_type = 'S' THEN

				  SELECT
                        isc.unit_cost
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.loc = rec.location
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND iscl.origin_country_id = 'TH';

						    SELECT
                        COUNT(1)
                    INTO l_cc_start_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_start_date
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc = rec.location
                        );
                    SELECT
                        COUNT(1)
                    INTO l_cc_end_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_end_date + 1
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc = rec.location
                        );

                    SELECT
                        COUNT(item)
                    INTO l_loc_range
                    FROM
                        item_loc
                    WHERE
                        item = rec.item
                     and loc = rec.location;

                END IF;

                SELECT
                    COUNT(1)
                INTO l_item_loc
                FROM
                    item_loc
                WHERE
                    item = rec.item
                    AND loc = rec.location;

                SELECT
                    status
                INTO l_il_status
                FROM
                    item_loc
                WHERE
                    item = rec.item
                    AND loc = rec.location;

                IF ( rec.loc_type = 'Z' ) THEN

				SELECT
                        MAX(isc.unit_cost)
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND loc IN (
                            SELECT
                                location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rzl.zone_id = rz.zone_id
                                AND rz.zone_display_id = rec.location
                                AND rz.currency_code = 'THB'
                        );

						 SELECT
                        COUNT(1)
                    INTO l_cc_start_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_start_date
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc IN (
                                    SELECT
                                        location
                                    FROM
                                        rpm_zone_location   rzl,
                                        rpm_zone            rz
                                    WHERE
                                        rzl.zone_id = rz.zone_id
                                        AND rz.zone_display_id = rec.location
                                        AND rz.currency_code = 'THB'
                                )
                        );
                SELECT
                        COUNT(1)
                    INTO l_cc_end_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_end_date + 1
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc IN (
                                    SELECT
                                        location
                                    FROM
                                        rpm_zone_location   rzl,
                                        rpm_zone            rz
                                    WHERE
                                        rzl.zone_id = rz.zone_id
                                        AND rz.zone_display_id = rec.location
                                        AND rz.currency_code = 'THB'
                                )
                        );

						SELECT
                        COUNT(DISTINCT q1.compare_cost)
                    INTO l_compare_cost
                    FROM
                        (
                            SELECT
                                isc.unit_cost,
                                CASE
                                    WHEN rec.cc_amount > isc.unit_cost    THEN
                                        '1'
                                    WHEN rec.cc_amount <= isc.unit_cost   THEN
                                        '0'
                                END compare_cost
                            FROM
                                item_supp_country       isc,
                                item_supp_country_loc   iscl,
                                rpm_zone_location       rzl,
                                rpm_zone                rz
                            WHERE
                                isc.item = iscl.item
                                AND isc.supplier = iscl.supplier
                                AND isc.origin_country_id = iscl.origin_country_id
                                AND iscl.loc = rzl.location
                                AND rz.zone_id = rzl.zone_id
                                AND rz.zone_display_id = rec.location
                                AND iscl.supplier = rec.supplier
                                AND iscl.item = rec.item
                        ) q1
                    WHERE
                        q1.compare_cost = 1;

                    SELECT
                        COUNT(DISTINCT item)
                    INTO l_item_ranging
                    FROM
                        item_loc
                    WHERE
                        item = rec.item
                        AND loc IN (
                            SELECT
                                rzl.location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rz.zone_display_id = rec.location
                                AND rzl.zone_id = rz.zone_id
                        );

                ELSIF ( rec.loc_type = 'A' ) THEN
				SELECT
                        MAX(isc.unit_cost)
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND loc IN (
                            SELECT
                                location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                        );

						SELECT
                        COUNT(DISTINCT q1.compare_cost)
                    INTO l_compare_cost
                    FROM
                        (
                            SELECT
                                isc.unit_cost,
                                CASE
                                    WHEN rec.cc_amount > isc.unit_cost    THEN
                                        '1'
                                    WHEN rec.cc_amount <= isc.unit_cost   THEN
                                        '0'
                                END compare_cost
                            FROM
                                item_supp_country       isc,
                                item_supp_country_loc   iscl,
                                rpm_zone_location       rzl,
                                rpm_zone                rz
                            WHERE
                                isc.item = iscl.item
                                AND isc.supplier = iscl.supplier
                                AND isc.origin_country_id = iscl.origin_country_id
                                AND rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                                AND iscl.supplier = rec.supplier
                                AND iscl.item = rec.item
                        ) q1
                    WHERE
                        q1.compare_cost = 1;

                    SELECT
                        COUNT(zone_id)
                    INTO l_item_zone_range
                    FROM
                        (
                            SELECT
                                rzl.zone_id
                            FROM
                                rpm_zone_group      rzg,
                                rpm_zone            rz,
                                rpm_zone_location   rzl
                            WHERE --rzg.name = 'All Stores'
                                rz.zone_group_id = 1
                                AND rzg.zone_group_id = rz.zone_group_id
                                AND rz.zone_id = rzl.zone_id
                                AND rz.currency_code = 'THB'
                            GROUP BY
                                rzl.zone_id
                            MINUS
                            SELECT
                                rzl.zone_id
                            FROM
                                rpm_zone_group      rzg,
                                rpm_zone            rz,
                                rpm_zone_location   rzl,
                                item_loc            il
                            WHERE --rzg.name = 'All Stores'
                                rz.zone_group_id = 1
                                AND rzg.zone_group_id = rz.zone_group_id
                                AND rz.zone_id = rzl.zone_id
                                AND rzl.location = il.loc
                                AND rz.currency_code = 'THB'
                                AND il.item = rec.item
                            GROUP BY
                                rzl.zone_id
                        ) zone_count;

                END IF;

                SELECT
                    COUNT(1)
                INTO l_sup
                FROM
                    sups
                WHERE
                    supplier = rec.supplier
                    AND supplier_parent IS NOT NULL;

                SELECT
                    COUNT(1)
                INTO l_item_sup
                FROM
                    item_supplier
                WHERE
                    item = rec.item
                    AND supplier = rec.supplier;

                SELECT
                    COUNT(1)
                INTO l_sup_status
                FROM
                    sups
                WHERE
                    supplier = rec.supplier
                    AND supplier_parent IS NOT NULL
                    AND sup_status = 'A';

                SELECT
                    COUNT(1)
                INTO l_item_cnt_check
                FROM
                    item_supp_country
                WHERE
                    item = rec.item
                    AND supplier = rec.supplier
                    AND origin_country_id = 'TH';

            EXCEPTION
                WHEN OTHERS THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'Error for Validation Values', sqlerrm);

                    l_error := true;
            END;
           ----**** ----End  Valdiation check against values  ----**** ----
           ---Valdiating business checks for values
--------- MultiBuy Buy validation start here-----

         --Positive number 
            IF rec.buylist_group < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'BUYLIST_GROUP', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

		 -- Check Skulist exists
            IF rec.skulist IS NOT NULL AND l_skulist_count = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SKULIST', 'INV_SKU_LIST');

                l_error := true;
            END IF;

		 -- Check item exists in skulist
            IF rec.skulist IS NOT NULL AND l_skulist_item_exists = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_ITEM_NOT_FROM_SKULST');

                l_error := true;
            END IF;            

		 -- Check item_count
            IF l_buygrp_count > 1  THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_ITEM_AND_SKULIST');

                l_error := true;
            END IF;     

		 -- Check item_count
            IF l_ml_item_count > 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_SINGLE_ITEM_4_GROUP');

                l_error := true;
            END IF;    

		 --Invalid Item number check

            IF rec.item IS NULL OR l_item_count = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INVALID_ITEM');

                l_error := true;
            END IF;

		 --Item status check

            IF l_item_status <> 'A' THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'ITEM_STATUS ');

                l_error := true;
            END IF;

          --Item transaction level check

            IF l_tran_level = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INV_ITEM_LEVEL');

                l_error := true;
            END IF;     

	 --Item not selleable check

            IF l_sellable = 'N' THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'ITEM_NOT_SELLABLE ');
            END IF;

	--Item/loc not valid check

            IF rec.loc_type = 'S' AND l_loc_range = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INVALID_ITEM_LOC');

                l_error := true;
            END IF;

        --Item/loc ranging check

            IF l_item_ranging = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_NOT_RANGED_2_ZONE');

                l_error := true;
            END IF;

        --Item/loc ranging check

            IF l_item_zone_range <> 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_NOT_RANGED_2_ZONE');
            END IF;
--Supplier site check

            IF l_sup = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'INV_SUPPLIER_SITE');

                l_error := true;
            END IF;

--Supplier site status check

            IF l_sup_status = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'INACTIVE_SUPP_SITE');

                l_error := true;

--Supplier site check			
            END IF;

            IF l_item_sup = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'SVC_ISU_ISP_FK');

                l_error := true;
            END IF;



		IF rec.reedem_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'REEDEM_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.cc_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.loc_type = 'S' AND l_unit_cost < rec.cc_amount THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'CRC_INVALID_COST');

                l_error := true;
            END IF;

            IF l_compare_cost = 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'CRC_INVALID_COST');

                l_error := true;
            END IF;

        if rec.cc_amount is not null then

         if CHECK_SUPP_VARIANCES(O_error_message,
                              L_approve ,
                              rec.CC_AMOUNT ,   
                              rec.supplier ,
                              rec.item,
                              rec.location ,
                              'TH'       ,
                            'Y') =FALSE then
                 WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CC_AMOUNT',
                        'FAILED_SUPP_VAR_CHECK');
            L_error :=TRUE;            
         end if;
         end if;

            IF rec.cc_start_date < to_date(get_vdate()) + 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_START_DATE', 'CRC_INVALID_START_DATE');

                l_error := true;
            END IF;

            IF rec.cc_start_date > rec.cc_end_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_START_DATE', 'VAL_START_DATE');

                l_error := true;
                IF l_cc_start_check <> 0 THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'CC_START_DATE', 'COST_CHG_EXISTS_II');

                    l_error := true;
                END IF;


            END IF;

            IF rec.cc_end_date < rec.cc_start_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_END_DATE', 'CRC_INVALID_END_DATE ');

                l_error := true;
            END IF;

            IF l_cc_end_check <> 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_END_DATE', 'COST_CHG_EXISTS_II');

                l_error := true;
            END IF;

            IF rec.new_gp_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'NEW_GP_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.new_gp_percent < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'NEW_GP_PERCENT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.roq_units < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ROQ_UNITS', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.roq_inners < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ROQ_INNERS', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.oi_value < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'OI_VALUE', 'POSITIVE_VALUE');

                l_error := true;
            END IF;            

		END LOOP;

                   select 
                          max_buy_lists
                     into 
                          L_max_buy_lists
                     from rpm_system_options;

                SELECT
                    COUNT(1)
                INTO l_itm_cnt
                FROM
                    crc_svc_multibuy_buy_stg
                WHERE
             process_id = i_process_id;                

         		 --BUYlist count
            IF l_itm_cnt > L_max_buy_lists THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, L_max_buy_lists,
                            'ITEM', 'CRC_MAX_NUM_BUY_LISTS');

                l_error := true;
            END IF;


        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END process_promo_buy;	
-----------------------------------------------------------------------------------------------------------------------

    FUNCTION process_promo_get (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        i_process_id      IN      crc_svc_multibuy_get_stg.process_id%TYPE,
        i_chunk_id        IN      crc_svc_multibuy_get_stg.chunk_id%TYPE
    ) RETURN BOOLEAN IS

        l_program                     VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO_GET.PROCESS_PROMO_GET';
        l_table_buy                   svc_admin_upld_er.table_name%TYPE := 'BUY_DATA';		
        l_table                       svc_admin_upld_er.table_name%TYPE := 'GET_DATA';
        l_error                       BOOLEAN;
        l_approve                     BOOLEAN;
        l_process_error               BOOLEAN := false;
        l_error_message               VARCHAR2(600);
        promo_temp_rec                crc_svc_multibuy_get_stg%rowtype;
        promo_temp_rec_buy            c_svc_promo_buy%rowtype;
        promo_temp_rec_get            c_svc_promo_get%rowtype;        
        l_item_count                  NUMBER(1) := 0;
        l_item_status                 VARCHAR2(1);
        l_tran_level                  NUMBER(2) := 0;
        l_sellable                    VARCHAR2(1);
        l_loc_range                   NUMBER(3) := 0;
        l_store                       NUMBER(1) := 0;
        l_st_open_date                DATE;
        l_st_close_date               DATE;
        l_item_loc                    VARCHAR2(1);
        l_il_status                   VARCHAR2(1);
        l_zone                        NUMBER(1);
        l_forecast_gr                 NUMBER(2);
        l_mechanic                    NUMBER(2);
        l_cust_seg                    NUMBER(2);
        l_sup                         NUMBER(1);
        l_item_sup                    NUMBER(1);
        l_sup_status                  NUMBER(1);
        l_cc_start_check              NUMBER(1);
        l_cc_end_check                NUMBER(1);
        l_duplicate_check             NUMBER(2);
        l_item_ranging                NUMBER(2);
        l_item_zone_range             NUMBER(2);
        l_compare_cost                NUMBER(1);
        l_compare_retail              NUMBER(1);
        l_stg_promo_eventid           NUMBER(6) := NULL;
        l_stg_zone_grpid              NUMBER(4) := NULL;
        stg_multibuy_buy_temp_rec     rpm_stage_multibuy_buylist%rowtype;		
        stg_multibuy_get_temp_rec     rpm_stage_multibuy_rwdlist%rowtype;
        o_deal_id                     NUMBER(10);
        o_deal_detail_id              NUMBER(10);
        l_item_cnt_check              NUMBER(2);
        l_promo_event                 NUMBER(1);
        l_exist_spromo                NUMBER(2);
        l_unit_cost                   NUMBER(10);
        l_unit_retail                 NUMBER(10);
		l_stg_buylist_id              NUMBER(10);
		l_UOM                         VARCHAR2(10);
        l_skulist_count               NUMBER(2);
        l_skulist_item_exists         NUMBER(2);
        l_getgrp_count                NUMBER(2);
        l_buygrp_exist                NUMBER(2);   
		i_buylist_group               NUMBER(15); 
		l_buylist_group               NUMBER(10); 
        l_getlist_group               NUMBER(10); 
        l_skulist                     NUMBER(8); 
        l_qual_type                   VARCHAR2(1);
        l_qual_value                  NUMBER(20,2);
        l_item                        VARCHAR2(25);
        l_sku_itm_cnt_buy             NUMBER(5);
        l_sku_itm_cnt_get             NUMBER(5);
        l_buygrp_cnt_chk              NUMBER(5);
        l_itm_cnt                     NUMBER(5);
        L_max_rwd_lists               NUMBER := NULL;   
------------------Header-----
        l_promo_comp_custattr         NUMBER(15);
        l_promo_comp_custattrexist    NUMBER(2);
        l_stg_comp_custattrid         NUMBER(15);
        l_stg_comp_custattrexists     NUMBER(2);
        l_promo_dtl_custattr          NUMBER(15);
        l_promo_dtl_custattrexist     NUMBER(2);
        l_stg_dtl_custattrid          NUMBER(15);
        l_stg_dtl_custattrexists      NUMBER(2);
        l_stg_promo_eventid           NUMBER(6) := NULL;
        l_stg_zone_grpid              NUMBER(4) := NULL;
        stg_multibuy_temp_rec         rpm_stage_multibuy_header%rowtype;
        o_deal_id                     NUMBER(10);
        o_deal_detail_id              NUMBER(10);
        l_zone_id                     NUMBER(10);
        l_zone_idexists               NUMBER(2);
        l_cust_count                  NUMBER(3);
        l_promo_event                 NUMBER(1);
        l_buy_redeem_amt NUMBER(10);
        l_get_redeem_amt NUMBER(10);
        l_buy_cc_amt NUMBER(10);
        l_get_cc_amt NUMBER(10);
        l_buy_cc_start_date DATE;
        l_get_cc_start_date DATE;
        l_buy_cc_end_date DATE;
        l_get_cc_end_date DATE;
        l_buy_oi_value NUMBER(10);
        l_get_oi_value NUMBER(10);      

    BEGIN

        FOR rec IN c_svc_promo_get(i_process_id, i_chunk_id) LOOP
            l_error := false;
            l_process_error := false;
           ----**** ---- Valdiation check against values  ----**** ----
            BEGIN

                IF rec.skulist IS NOT NULL THEN
                                -- SkuList Validations
                    SELECT
                        COUNT(skulist)
                    INTO l_skulist_count
                    FROM
                        skulist_head
                    WHERE
                        skulist = rec.skulist;
                
                    SELECT
                        COUNT(1)
                    INTO l_skulist_item_exists
                    FROM
                        skulist_detail
                    WHERE
                        skulist = rec.skulist
                        AND item = rec.item;
                
                END IF;
                
                SELECT
                    COUNT(1)
                    INTO l_getgrp_count
                FROM
                    crc_svc_multibuy_get_stg getstg1
                WHERE
                    process_id = rec.process_id
                    AND getlist_group = rec.getlist_group
                    AND skulist IS NOT NULL
                    AND item IS NOT NULL
                    AND EXISTS (
                        SELECT
                            1
                        FROM
                            crc_svc_multibuy_get_stg getstg2
                        WHERE
                            getstg2.process_id = getstg1.process_id
                            AND getstg2.getlist_group = getstg1.getlist_group
                            AND skulist IS NOT NULL
                            AND item IS NOT NULL
                    );

 				-- BuyGroup list validation
                SELECT
                    COUNT(buylist_group)
                INTO l_buygrp_exist
                FROM
                    crc_svc_multibuy_buy_stg
                WHERE
                    buylist_group = rec.buylist_group
                AND process_id = rec.process_id;
    
               IF rec.item is not null
               Then
 				-- Item Validations
                SELECT
                    COUNT(item)
                INTO l_item_count
                FROM
                    item_master
                WHERE
                    item = rec.item;

                SELECT
                    status,
                    sellable_ind
                INTO
                    l_item_status,
                    l_sellable
                FROM
                    item_master
                WHERE
                    item = rec.item;
       
                SELECT
                    COUNT(1)
                INTO l_tran_level
                FROM
                    item_master
                WHERE
                    item = rec.item
                    AND item_level <= tran_level;
                SELECT
                    COUNT(1)
                INTO l_item_loc
                FROM
                    item_loc
                WHERE
                    item = rec.item
                    AND loc = rec.location;

                SELECT
                    status
                INTO l_il_status
                FROM
                    item_loc
                WHERE
                    item = rec.item
                    AND loc = rec.location;

                SELECT
                    COUNT(1)
                INTO l_sup
                FROM
                    sups
                WHERE
                    supplier = rec.supplier
                    AND supplier_parent IS NOT NULL;

                SELECT
                    COUNT(1)
                INTO l_item_sup
                FROM
                    item_supplier
                WHERE
                    item = rec.item
                    AND supplier = rec.supplier;

                SELECT
                    COUNT(1)
                INTO l_sup_status
                FROM
                    sups
                WHERE
                    supplier = rec.supplier
                    AND supplier_parent IS NOT NULL
                    AND sup_status = 'A';

                SELECT
                    COUNT(1)
                INTO l_item_cnt_check
                FROM
                    item_supp_country
                WHERE
                    item = rec.item
                    AND supplier = rec.supplier
                    AND origin_country_id = 'TH';

            END IF;
         ---- Based on lacation type

                IF rec.loc_type = 'S' THEN
                    IF rec.item is not null
                        then
                    SELECT
                        COUNT(item)
                    INTO l_loc_range
                    FROM
                        item_loc
                    WHERE
                        item = rec.item;

                    SELECT
                        COUNT(1)
                    INTO l_item_loc
                    FROM
                        item_loc
                    WHERE
                        item = rec.item
                        AND loc = rec.location;

                    SELECT
                        status
                    INTO l_il_status
                    FROM
                        item_loc
                    WHERE
                        item = rec.item
                        AND loc = rec.location;

                    SELECT
                        isc.unit_cost
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.loc = rec.location
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND iscl.origin_country_id = 'TH';

                   SELECT
                        COUNT(1)
                    INTO l_cc_start_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_start_date
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc = rec.location
                        );

                    SELECT
                        COUNT(1)
                    INTO l_cc_end_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_end_date + 1
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc = rec.location
                        );

                    SELECT
                        unit_retail
                    INTO l_unit_retail
                    FROM
                        item_loc
                    WHERE
                        loc = rec.location
                        AND item = rec.item;

                    IF rec.change_amount IS NOT NULL THEN
                        SELECT
                            COUNT(1)
                        INTO l_exist_spromo
                        FROM
                            rpm_promo_dtl              rpd,
                            rpm_promo_dtl_merch_node   mrch,
                            rpm_promo_comp             rpc,
                            rpm_promo_zone_location    rploc
                        WHERE
                            rpd.promo_dtl_id = mrch.promo_dtl_id
                            AND rpd.promo_dtl_id = rploc.promo_dtl_id
                            AND rpc.promo_comp_id = rpd.promo_comp_id
                            AND rpd.state NOT IN (
                                0,
                                1,
                                2,
                                4
                            )
                            AND mrch.item = rec.item
                            AND rploc.location = rec.location
                            AND to_date(rpd.start_date) = rec.promo_start_date
                            AND to_date(rpd.end_date) = rec.promo_end_date
                            AND rpc.type = 1;

                    END IF;
                   END IF;     
                ELSIF ( rec.loc_type = 'A' ) THEN
                    SELECT
                        MAX(isc.unit_cost)
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND loc IN (
                            SELECT
                                location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                        );

                    SELECT
                        COUNT(DISTINCT q1.compare_cost)
                    INTO l_compare_cost
                    FROM
                        (
                            SELECT
                                isc.unit_cost,
                                CASE
                                    WHEN rec.cc_amount > isc.unit_cost    THEN
                                        '1'
                                    WHEN rec.cc_amount <= isc.unit_cost   THEN
                                        '0'
                                END compare_cost
                            FROM
                                item_supp_country       isc,
                                item_supp_country_loc   iscl,
                                rpm_zone_location       rzl,
                                rpm_zone                rz
                            WHERE
                                isc.item = iscl.item
                                AND isc.supplier = iscl.supplier
                                AND isc.origin_country_id = iscl.origin_country_id
                                AND rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                                AND iscl.supplier = rec.supplier
                                AND iscl.item = rec.item
                        ) q1
                    WHERE
                        q1.compare_cost = 1;

                    SELECT
                        COUNT(DISTINCT compare_retail)
                    INTO l_compare_retail
                    FROM
                        (
                            SELECT
                                il.unit_retail,
                                CASE
                                    WHEN rec.change_amount > il.unit_retail   THEN
                                        '1'
                                    WHEN rec.change_amount <= il.unit_retail  THEN
                                        '0'
                                END compare_retail
                            FROM
                                item_loc            il,
                                rpm_zone_location   rzl,
                                rpm_zone            rz,
                                item_supplier       isup
                            WHERE
                                rzl.location = il.loc
                                AND rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                                AND isup.item = il.item
                                AND isup.supplier = rec.supplier
                                AND il.item = rec.item
                        ) q1
                    WHERE
                        compare_retail = 1;

                    SELECT
                        MAX(il.unit_retail)
                    INTO l_unit_retail
                    FROM
                        item_loc                il,
                        item_supp_country_loc   iscl
                    WHERE
                        il.item = iscl.item
                        AND il.loc = iscl.loc
                        AND il.item = rec.item
                        AND iscl.supplier = rec.supplier  --and iscl.primary_loc_ind='Y'  
                        AND il.loc IN (
                            SELECT
                                location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rzl.zone_id = rz.zone_id
                                AND rz.zone_group_id = 1
                                AND rz.currency_code = 'THB'
                        )
                        AND il.loc_type = 'S';

                    SELECT
                        COUNT(zone_id)
                    INTO l_item_zone_range
                    FROM
                        (
                            SELECT
                                rzl.zone_id
                            FROM
                                rpm_zone_group      rzg,
                                rpm_zone            rz,
                                rpm_zone_location   rzl
                            WHERE --rzg.name = 'All Stores'
                                rz.zone_group_id = 1
                                AND rzg.zone_group_id = rz.zone_group_id
                                AND rz.zone_id = rzl.zone_id
                                AND rz.currency_code = 'THB'
                            GROUP BY
                                rzl.zone_id
                            MINUS
                            SELECT
                                rzl.zone_id
                            FROM
                                rpm_zone_group      rzg,
                                rpm_zone            rz,
                                rpm_zone_location   rzl,
                                item_loc            il
                            WHERE --rzg.name = 'All Stores'
                                rz.zone_group_id = 1
                                AND rzg.zone_group_id = rz.zone_group_id
                                AND rz.zone_id = rzl.zone_id
                                AND rzl.location = il.loc
                                AND rz.currency_code = 'THB'
                                AND il.item = rec.item
                            GROUP BY
                                rzl.zone_id
                        ) zone_count;
                ELSIF ( rec.loc_type = 'Z' ) THEN
                    SELECT
                        MAX(isc.unit_cost)
                    INTO l_unit_cost
                    FROM
                        item_supp_country       isc,
                        item_supp_country_loc   iscl
                    WHERE
                        isc.item = iscl.item
                        AND isc.supplier = iscl.supplier
                        AND isc.origin_country_id = iscl.origin_country_id
                        AND iscl.item = rec.item
                        AND iscl.supplier = rec.supplier
                        AND loc IN (
                            SELECT
                                location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rzl.zone_id = rz.zone_id
                                AND rz.zone_display_id = rec.location
                                AND rz.currency_code = 'THB'
                        );

                    SELECT
                        COUNT(1)
                    INTO l_cc_start_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_start_date
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc IN (
                                    SELECT
                                        location
                                    FROM
                                        rpm_zone_location   rzl,
                                        rpm_zone            rz
                                    WHERE
                                        rzl.zone_id = rz.zone_id
                                        AND rz.zone_display_id = rec.location
                                        AND rz.currency_code = 'THB'
                                )
                        );

                    SELECT
                        COUNT(1)
                    INTO l_cc_end_check
                    FROM
                        cost_susp_sup_head cch
                    WHERE
                        active_date = rec.cc_end_date + 1
                        AND EXISTS (
                            SELECT
                                1
                            FROM
                                cost_susp_sup_detail_loc ccd
                            WHERE
                                ccd.cost_change = cch.cost_change
                                AND ccd.item = rec.item
                                AND ccd.supplier = rec.supplier
                                AND ccd.loc IN (
                                    SELECT
                                        location
                                    FROM
                                        rpm_zone_location   rzl,
                                        rpm_zone            rz
                                    WHERE
                                        rzl.zone_id = rz.zone_id
                                        AND rz.zone_display_id = rec.location
                                        AND rz.currency_code = 'THB'
                                )
                        );

                    SELECT
                        MAX(unit_retail)
                    INTO l_unit_retail
                    FROM
                        item_loc            il,
                        rpm_zone_location   rzl,
                        rpm_zone            rz
                    WHERE
                        il.item = rec.item
                        AND rzl.zone_id = rz.zone_id
                        AND rz.zone_display_id = rec.location
                        AND rzl.location = il.loc
                        AND EXISTS (
                            SELECT
                                loc
                            FROM
                                item_supp_country_loc iscl
                            WHERE
                                iscl.item = il.item
                                AND iscl.loc = il.loc --and iscl.origin_country_id='TH'
                                AND iscl.supplier = il.primary_supp
                                AND il.primary_cntry = iscl.origin_country_id
                        );

                    SELECT
                        COUNT(DISTINCT compare_retail)
                    INTO l_compare_retail
                    FROM
                        (
                            SELECT
                                il.unit_retail,
                                CASE
                                    WHEN rec.change_amount > il.unit_retail   THEN
                                        '1'
                                    WHEN rec.change_amount <= il.unit_retail  THEN
                                        '0'
                                END compare_retail
                            FROM
                                item_loc            il,
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rz.zone_id = rzl.zone_id
                                AND rzl.location = il.loc
                                AND rz.zone_display_id = rec.location
                                AND il.item = rec.item
                        ) q1
                    WHERE
                        compare_retail = 1;

                    SELECT
                        COUNT(DISTINCT item)
                    INTO l_item_ranging
                    FROM
                        item_loc
                    WHERE
                        item = rec.item
                        AND loc IN (
                            SELECT
                                rzl.location
                            FROM
                                rpm_zone_location   rzl,
                                rpm_zone            rz
                            WHERE
                                rz.zone_display_id = rec.location
                                AND rzl.zone_id = rz.zone_id
                        );

                    SELECT
                        COUNT(1)
                    INTO l_zone
                    FROM
                        rpm_zone
                    WHERE
                        zone_display_id = rec.location;

                    SELECT
                        COUNT(DISTINCT q1.compare_cost)
                    INTO l_compare_cost
                    FROM
                        (
                            SELECT
                                isc.unit_cost,
                                CASE
                                    WHEN rec.cc_amount > isc.unit_cost    THEN
                                        '1'
                                    WHEN rec.cc_amount <= isc.unit_cost   THEN
                                        '0'
                                END compare_cost
                            FROM
                                item_supp_country       isc,
                                item_supp_country_loc   iscl,
                                rpm_zone_location       rzl,
                                rpm_zone                rz
                            WHERE
                                isc.item = iscl.item
                                AND isc.supplier = iscl.supplier
                                AND isc.origin_country_id = iscl.origin_country_id
                                AND iscl.loc = rzl.location
                                AND rz.zone_id = rzl.zone_id
                                AND rz.zone_display_id = rec.location
                                AND iscl.supplier = rec.supplier
                                AND iscl.item = rec.item
                        ) q1
                    WHERE
                        q1.compare_cost = 1;

                END IF;
             if rec.item is not null
             Then
                SELECT
                    reedem_amount,
                    cc_amount,
                    cc_start_date,
                    cc_end_date,
                    oi_value
                INTO
                    l_buy_redeem_amt,
                    l_buy_cc_amt,
                    l_buy_cc_start_date,
                    l_buy_cc_end_date,
                    l_buy_oi_value
                FROM
                    crc_svc_multibuy_buy_stg
                WHERE
                    process_id = rec.process_id
                    AND item = rec.item;
                
                SELECT
                    reedem_amount,
                    cc_amount,
                    cc_start_date,
                    cc_end_date,
                    oi_value
                INTO
                    l_get_redeem_amt,
                    l_get_cc_amt,
                    l_get_cc_start_date,
                    l_get_cc_end_date,
                    l_get_oi_value
                FROM
                    crc_svc_multibuy_get_stg
                WHERE
                    process_id = rec.process_id
                    AND item = rec.item;
             END IF;   
            EXCEPTION
                WHEN OTHERS THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'Error for Validation Values', sqlerrm);

                    l_error := true;
            END;
           ----**** ----End  Valdiation check against values  ----**** ----
           ---Valdiating business checks for values
--------- MultiBuy Buy validation start here-----

         --Positive number 
            IF rec.getlist_group < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'GETLIST_GROUP', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF l_buygrp_exist = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'BUYLIST_GROUP', 'CRC_BLGROUP_NOT_EXIST');

                l_error := true;
            END IF;

		 -- Check Skulist exists
            IF rec.skulist IS NOT NULL AND l_skulist_count = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SKULIST', 'INV_SKU_LIST');

                l_error := true;
            END IF;

		 -- Check item exists in skulist
            IF rec.skulist IS NOT NULL AND l_skulist_item_exists = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_ITEM_NOT_FROM_SKULST');

                l_error := true;
            END IF;            

		 -- Check item_count komal
            IF l_getgrp_count > 0  THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_ITEM_OR_SKULIST');

                l_error := true;
            END IF;               



            IF rec.item IS NOT NULL AND l_item_count = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INVALID_ITEM');

                l_error := true;
            END IF;

		 --Item status check

            IF rec.item IS NOT NULL AND l_item_status <> 'A' THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'ITEM_STATUS ');

                l_error := true;
            END IF;

          --Item transaction level check

           IF rec.item IS NOT NULL AND l_tran_level = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INV_ITEM_LEVEL');

                l_error := true;
            END IF;     

	 --Item not selleable check

            IF rec.item IS NOT NULL AND l_sellable = 'N' THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'ITEM_NOT_SELLABLE ');
            END IF;

	--Item/loc not valid check

            IF rec.item IS NOT NULL AND rec.loc_type = 'S' AND l_loc_range = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'INVALID_ITEM_LOC');

                l_error := true;
            END IF;

        --Item/loc ranging check

            IF rec.item IS NOT NULL AND l_item_ranging = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_NOT_RANGED_2_ZONE');

                l_error := true;
            END IF;

        --Item/loc ranging check

            IF l_item_zone_range <> 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_NOT_RANGED_2_ZONE');
            END IF;
--Supplier site check

            IF l_sup = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'INV_SUPPLIER_SITE');

                l_error := true;
            END IF;

--Supplier site status check

            IF l_sup_status = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'INACTIVE_SUPP_SITE');

                l_error := true;

--Supplier site check			
            END IF;

            IF l_item_sup = 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SUPPLIER', 'SVC_ISU_ISP_FK');

                l_error := true;
            END IF;

--

            IF ( rec.loc_type = 'A' OR rec.loc_type = 'Z' ) AND rec.change_type = 'F' AND l_compare_retail = 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CHANGE_AMOUNT', 'CRC_INVALID_UNIT_RETAIL');

                l_error := true;
            END IF;

            IF rec.change_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CHANGE_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.reedem_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'REEDEM_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.change_type = 'A' THEN
                IF rec.reedem_amount > rec.change_amount THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'REEDEM_AMOUNT', 'CRC_INVALID_RDM');

                    l_error := true;
                END IF;
            ELSIF rec.change_type = 'F' THEN
                IF rec.reedem_amount > ( l_unit_retail - rec.change_amount ) THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'REEDEM_AMOUNT', 'CRC_INVALID_RDM');

                    l_error := true;
                END IF;
            ELSIF rec.change_type = 'P' THEN
                IF rec.reedem_amount > ( l_unit_retail - ( ( l_unit_retail * rec.change_amount ) / 100 ) ) THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'REEDEM_AMOUNT', 'CRC_INVALID_RDM');

                    l_error := true;
                END IF;
            END IF;

            IF rec.cc_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.loc_type = 'S' AND l_unit_cost < rec.cc_amount THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'CRC_INVALID_COST');

                l_error := true;
            END IF;

            IF l_compare_cost = 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'CRC_INVALID_COST');

                l_error := true;
            END IF;

        if rec.cc_amount is not null then

         if CHECK_SUPP_VARIANCES(O_error_message,
                              L_approve ,
                              rec.CC_AMOUNT ,   
                              rec.supplier ,
                              rec.item,
                              rec.location ,
                              'TH'       ,
                            'Y') =FALSE then
                 WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CC_AMOUNT',
                        'FAILED_SUPP_VAR_CHECK');
            L_error :=TRUE;            
         end if;
         end if;

            IF rec.change_type = 'F' AND rec.cc_amount > rec.change_amount THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_AMOUNT', 'CRC_COST_LT_RETAIL');

                l_error := true;
            END IF;

            IF rec.cc_start_date < to_date(get_vdate()) + 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_START_DATE', 'CRC_INVALID_START_DATE');

                l_error := true;
            END IF;

            IF rec.cc_start_date > rec.cc_end_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_START_DATE', 'VAL_START_DATE');

                l_error := true;
                IF l_cc_start_check <> 0 THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                'CC_START_DATE', 'COST_CHG_EXISTS_II');

                    l_error := true;
                END IF;


            END IF;

            IF rec.cc_end_date < rec.cc_start_date THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_END_DATE', 'CRC_INVALID_END_DATE ');

                l_error := true;
            END IF;

            IF l_cc_end_check <> 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'CC_END_DATE', 'COST_CHG_EXISTS_II');

                l_error := true;
            END IF;

            IF rec.new_gp_amount < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'NEW_GP_AMOUNT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.new_gp_percent < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'NEW_GP_PERCENT', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.roq_units < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ROQ_UNITS', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.roq_inners < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ROQ_INNERS', 'POSITIVE_VALUE');

                l_error := true;
            END IF;

            IF rec.oi_value < 0 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'OI_VALUE', 'POSITIVE_VALUE');

                l_error := true;
            END IF;
			-- Added new validations
		    IF  rec.reward_type = 0  and rec.buylist_group is not null THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'BUYLIST_GROUP', 'CRC_BUYLIST_MSTB_EMPTY');

                l_error := true;
            END IF;
            IF (rec.reward_type = 0 OR rec.reward_type = 1) and rec.getlist_group <> 1 THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'GETLIST_GROUP', 'CRC_WRONG_GETLIST');

                l_error := true;
            END IF;
            IF (rec.reward_type = 0 OR rec.reward_type = 1) and rec.skulist is not null THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'SKULIST', 'CRC_SKULIST_MSTB_EMPTY');

                l_error := true;
            END IF;
         IF (rec.reward_type = 0 OR rec.reward_type = 1) and rec.item is not null THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'ITEM', 'CRC_ITEM_MSTB_EMPTY');

                l_error := true;
            END IF;

             IF (rec.reward_type = 0 OR rec.reward_type = 1) and rec.qual_type is not null THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'QUAL_TYPE', 'CRC_QUAL_MSTB_EMPTY');

                l_error := true;
            END IF;

             IF (rec.reward_type = 0 OR rec.reward_type = 1) and rec.qual_value is not null THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'QUAL_VALUE', 'CRC_QUAL_MSTB_EMPTY');

                l_error := true;
            END IF;
--------New Validation start here---
         if l_buy_redeem_amt <> l_get_redeem_amt
            then 
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                            'REEDEM_AMOUNT', 'CRC_RDM_MUST_BE_SAME');

                l_error := true;
         END IF;  
         
         if l_buy_cc_amt <> l_get_cc_amt
            then 
            write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                        'CC_AMOUNT', 'CRC_CC_MUST_BE_SAME');
            
            l_error := true;
        END IF;  

         if l_buy_cc_start_date <> l_get_cc_start_date
            then 
            write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                        'CC_START_DATE', 'CRC_CC_STRT_DATE_SAME');
            
            l_error := true;
        END IF;

         if l_buy_cc_end_date <> l_get_cc_end_date
            then 
            write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                        'CC_END_DATE', 'CRC_CC_END_DATE_SAME');
            
            l_error := true;
        END IF;

         if l_buy_oi_value <> l_get_oi_value
            then 
            write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                        'OI_VALUE', 'CRC_OI_MUST_BE_SAME');
            
            l_error := true;
        END IF;
         
     END LOOP;

            SELECT
                max_reward_lists
            INTO l_max_rwd_lists
            FROM
                rpm_system_options;
            
            SELECT
                COUNT(1)
            INTO l_itm_cnt
            FROM
                crc_svc_multibuy_get_stg
            WHERE
                process_id = i_process_id;            


		 --Getlist count
            IF l_itm_cnt > L_max_rwd_lists THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, L_max_rwd_lists,
                            'ITEM', 'CRC_MAX_NUM_RWD_LISTS');

                l_error := true;
            END IF;


----------changes start here-----
 if LP_errors_tab.COUNT()=0 then

            FOR rec IN c_svc_promo_hdr(i_process_id, i_chunk_id) LOOP
                l_error := false;
                l_process_error := false;          

            ---------------------------------Customer data insert ------------------
                IF rec.mechanic IS NOT NULL OR rec.legacy_promo_id IS NOT NULL THEN
                    SELECT
                        COUNT(stage_prom_comp_cust_attr_id)
                    INTO l_cust_count
                    FROM
                        rpm_stage_multibuy_header
                    WHERE
                        comments = to_char(rec.process_id);

                    IF l_cust_count = 0 THEN
                        INSERT INTO rpm_stage_prom_comp_cust_attr (
                            stage_prom_comp_cust_attr_id,
                            varchar2_1,
                            varchar2_2,
                            varchar2_3
                        ) VALUES (
                            rpm_stage_prom_cust_attr_seq.NEXTVAL,
                            rec.mechanic,
                            'dummy',
                            rec.legacy_promo_id
                        );

                    END IF;

                    l_stg_comp_custattrid := rpm_stage_prom_cust_attr_seq.currval;
                END IF;

                l_promo_comp_custattr := l_stg_comp_custattrid;  

                               -- Get PROMO DTL Custom Attribute     
                IF rec.forecast_group IS NOT NULL OR rec.coupon_spend_ind IS NOT NULL OR rec.right_spend_ind IS NOT NULL OR rec.right_earn_ind
                IS NOT NULL OR rec.sticker_spend_ind IS NOT NULL OR rec.sticker_earn_ind IS NOT NULL OR rec.superburn_points IS NOT NULL OR rec
                .extra_points IS NOT NULL THEN
                    INSERT INTO rpm_stage_prom_dtl_cust_attr (
                        stage_prom_dtl_cust_attr_id,
                        varchar2_3,
                        number_11,
                        number_12,
                        number_13,
                        number_14,
                        number_15,
                        number_16,
                        number_17,
                        number_18
                    ) VALUES (
                        rpm_stage_dtl_cust_attr_seq.NEXTVAL,
                        rec.forecast_group,
                        rec.coupon_spend_ind,
                        rec.right_spend_ind,
                        rec.right_earn_ind,
                        rec.sticker_spend_ind,
                        rec.sticker_earn_ind,
                        rec.superburn_points,
                        rec.extra_points,
                        rec.reedem_amount
                    );

                END IF;

                l_promo_dtl_custattr := rpm_stage_dtl_cust_attr_seq.currval;

            ---------------------------------Customer data insert end ---------------------------------------
                stg_multibuy_temp_rec.stage_multibuy_header_id := rpm_stage_multibuy_header_seq.nextval;
                stg_multibuy_temp_rec.stage_id := 1;
                stg_multibuy_temp_rec.stage_promo_comp_id := 1;
                stg_multibuy_temp_rec.apply_to_code := 2;
                stg_multibuy_temp_rec.apply_reward_to := rec.reward_type;
                stg_multibuy_temp_rec.promo_start_date := rec.promo_start_date;
                stg_multibuy_temp_rec.promo_end_date := to_date(to_char(rec.promo_end_date, 'MM-DD-YYYY')
                                                                || ' 23:59:00', 'MM-DD-YYYY HH24:MI:SS');

                stg_multibuy_temp_rec.ignore_constraints := 1;
                stg_multibuy_temp_rec.auto_approve_ind := 0;
                stg_multibuy_temp_rec.status := 'N';
                stg_multibuy_temp_rec.timebased_dtl_ind := 0;
                stg_multibuy_temp_rec.name := upper(rec.promo_name);
                stg_multibuy_temp_rec.dtl_start_date := rec.promo_start_date;
                stg_multibuy_temp_rec.dtl_end_date := to_date(to_char(rec.promo_end_date, 'MM-DD-YYYY')
                                                              || ' 23:59:00', 'MM-DD-YYYY HH24:MI:SS');            
                stg_multibuy_temp_rec.comments := rec.process_id;
                stg_multibuy_temp_rec.customer_type := rec.cust_segment;
                stg_multibuy_temp_rec.currency_code := 'THB';
                stg_multibuy_temp_rec.stage_prom_comp_cust_attr_id := l_promo_comp_custattr;
                stg_multibuy_temp_rec.stage_prom_dtl_cust_attr_id := l_promo_dtl_custattr;  

                IF rec.loc_type = 'S' THEN
                    stg_multibuy_temp_rec.zone_node_type := 0;
                    stg_multibuy_temp_rec.location := rec.location;
                ELSIF rec.loc_type = 'Z' THEN
                    stg_multibuy_temp_rec.zone_node_type := 1;
                    stg_multibuy_temp_rec.zone_id := l_zone_id;
                    stg_multibuy_temp_rec.zone_group_id := NULL;
                ELSIF rec.loc_type = 'A' THEN
                    stg_multibuy_temp_rec.zone_node_type := 3;
                    stg_multibuy_temp_rec.zone_group_id := 1;
                    stg_multibuy_temp_rec.location := NULL;
                END IF;

                stg_multibuy_temp_rec.vendor_funded_ind := 0;
                stg_multibuy_temp_rec.contribution_percent := NULL;

                IF exec_stg_multibuy_hdr_ins(o_error_message, stg_multibuy_temp_rec) = false THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                NULL, o_error_message);

                    l_process_error := true;
                END IF;

            END LOOP;

 ------------------------- Buy insertion loop-------
 FOR rec IN c_svc_promo_buy(i_process_id, i_chunk_id) LOOP
            l_error := false;
            l_process_error := false;

                SELECT
                  count(1)
                  into l_sku_itm_cnt_buy
                FROM
                    crc_svc_multibuy_buy_stg
                WHERE
                    skulist = rec.skulist;  

               select count(1)
                INTO l_buygrp_cnt_chk
                from crc_svc_multibuy_buy_stg bystg1
                where process_id= rec.process_id
                and buylist_group  = rec.buylist_group
                and exists
                (select 1 from crc_svc_multibuy_buy_stg bystg2
                       where bystg2.process_id=bystg1.process_id
                       and bystg2.buylist_group=bystg1.buylist_group
                        );

---------------------------------Buy calling for RPM stage tables ------------------
            i_buylist_group := rec.buylist_group;
            l_stg_buylist_id := rpm_stage_multibuy_buylist_seq.nextval;
            stg_multibuy_buy_temp_rec.stage_buylist_id := l_stg_buylist_id;
            stg_multibuy_buy_temp_rec.stage_id := 1;
            stg_multibuy_buy_temp_rec.stage_promo_comp_id := 1;
            stg_multibuy_buy_temp_rec.buylist_group_id := rec.buylist_group;
            IF rec.qual_type IS NOT NULL THEN
                IF rec.qual_type = 'A' THEN
                    stg_multibuy_buy_temp_rec.qual_type := 1;
                ELSE
                    stg_multibuy_buy_temp_rec.qual_type := 2;
                END IF;
            END IF;

            stg_multibuy_buy_temp_rec.qual_value := rec.qual_value;

      /*      if l_buygrp_cnt_chk = 1
               THEN
                stg_multibuy_buy_temp_rec.merch_type := 6;                
             ELSE 
                stg_multibuy_buy_temp_rec.merch_type := 0;
            END IF;
        */    
            IF l_sku_itm_cnt_buy > 1
                THEN 
                  stg_multibuy_buy_temp_rec.item := NULL;
                  stg_multibuy_buy_temp_rec.merch_type := 6;  
                ELSE
                  stg_multibuy_buy_temp_rec.item := rec.item;
                  stg_multibuy_buy_temp_rec.merch_type := 0;
            END IF;
            stg_multibuy_buy_temp_rec.skulist := rec.skulist;            
            stg_multibuy_buy_temp_rec.reward_application := 1;
            IF rec.reward_type  IS NOT NULL THEN
                IF rec.reward_type  = 1 THEN
                    stg_multibuy_buy_temp_rec.reward_application := 1;
                ELSE
                    stg_multibuy_buy_temp_rec.reward_application := 0;
                END IF;
            END IF;


            stg_multibuy_buy_temp_rec.status := 'N';

		   IF exec_stg_multibuy_buy_ins(o_error_message, stg_multibuy_buy_temp_rec) = false THEN
                write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table_buy, rec.row_seq,
                            NULL, o_error_message);

                l_process_error := true;
                RETURN false;
            END IF;

---------------------------------Get calling for RPM stage tables ------------------
     FOR rec IN c_svc_promo_get_ins(i_buylist_group,i_process_id, i_chunk_id) LOOP
                  SELECT
                        standard_uom				
                    INTO
                        l_uom
                    FROM
                        item_master
                    WHERE
                        item = rec.item;

                SELECT
                  count(1)
                  into l_sku_itm_cnt_get
                FROM
                    crc_svc_multibuy_get_stg
                WHERE
                    skulist = rec.skulist;     

                stg_multibuy_get_temp_rec.stage_reward_list_id := rpm_stage_multibuy_rwdlist_seq.nextval;

                if rec.reward_type = 0
                    Then 
                    stg_multibuy_get_temp_rec.stage_buylist_id := NULL;
                ELSE
                stg_multibuy_get_temp_rec.stage_buylist_id := l_stg_buylist_id;
                END IF;  

                stg_multibuy_get_temp_rec.stage_id := 1;
                stg_multibuy_get_temp_rec.stage_promo_comp_id := 1;

                if rec.reward_type in(0,1)
                    Then 
                    stg_multibuy_get_temp_rec.rewardlist_group_id := 1;
                ELSE
                stg_multibuy_get_temp_rec.rewardlist_group_id := rec.getlist_group;
                END IF;                  


                IF rec.qual_type IS NOT NULL THEN
                    IF rec.qual_type = 'A' THEN
                        stg_multibuy_get_temp_rec.qual_type := 1;
                    ELSE
                        stg_multibuy_get_temp_rec.qual_type := 2;
                    END IF;
                END IF;

                stg_multibuy_get_temp_rec.qual_value := rec.qual_value;
                stg_multibuy_get_temp_rec.merch_type := 0;
                stg_multibuy_get_temp_rec.skulist := rec.skulist;            

                    IF l_sku_itm_cnt_get > 1
                        THEN 
                          stg_multibuy_get_temp_rec.item := NULL;
                           stg_multibuy_get_temp_rec.merch_type := 6; 
                        ELSE
                          stg_multibuy_get_temp_rec.item := rec.item;
                          stg_multibuy_get_temp_rec.merch_type := 0;
                    END IF;

            --    stg_multibuy_get_temp_rec.item := rec.item;
                IF rec.change_type = 'P' THEN
                    stg_multibuy_get_temp_rec.change_type := 0;
                    stg_multibuy_get_temp_rec.change_percent := rec.change_amount;
                ELSIF rec.change_type = 'A' THEN
                    stg_multibuy_get_temp_rec.change_type := 1;
                    stg_multibuy_get_temp_rec.change_amount := -rec.change_amount;
                ELSIF rec.change_type = 'F' THEN
                    stg_multibuy_get_temp_rec.change_type := 2;
                    stg_multibuy_get_temp_rec.change_amount := rec.change_amount;
                    stg_multibuy_get_temp_rec.change_selling_uom := l_uom;
                END IF;

                stg_multibuy_get_temp_rec.status := 'N';
                IF exec_stg_multibuy_get_ins(o_error_message, stg_multibuy_get_temp_rec) = false THEN
                    write_error(i_process_id, svc_admin_upld_er_seq.nextval, i_chunk_id, l_table, rec.row_seq,
                                NULL, o_error_message);

                    l_process_error := true;
                    RETURN false;
                END IF;
            END LOOP;
    END LOOP;
   END IF;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END process_promo_get;

--------------------------------------------------------------------------------------

    FUNCTION exec_stg_multibuy_hdr_ins (
        o_error_message          IN OUT  rtk_errors.rtk_text%TYPE,
        i_multibuyhdr_temp_rec   IN      rpm_stage_multibuy_header%rowtype
    ) RETURN BOOLEAN IS
        l_program VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.EXEC_STG_MULTIBUY_HDR_INS';
    BEGIN
        INSERT INTO rpm_stage_multibuy_header VALUES i_multibuyhdr_temp_rec;

        COMMIT;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END exec_stg_multibuy_hdr_ins;   

--------------------------------------------------------------------------------------

    FUNCTION exec_stg_multibuy_buy_ins (
        o_error_message          IN OUT  rtk_errors.rtk_text%TYPE,
        i_multibuybuy_temp_rec   IN      rpm_stage_multibuy_buylist%rowtype
    ) RETURN BOOLEAN IS
        l_program VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.EXEC_STG_MULTIBUY_BUY_INS';
    BEGIN
        INSERT INTO rpm_stage_multibuy_buylist VALUES i_multibuybuy_temp_rec;

        COMMIT;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END exec_stg_multibuy_buy_ins;   

--------------------------------------------------------------------------------------

    FUNCTION exec_stg_multibuy_get_ins (
        o_error_message          IN OUT  rtk_errors.rtk_text%TYPE,
        i_multibuyget_temp_rec   IN      rpm_stage_multibuy_rwdlist%rowtype
    ) RETURN BOOLEAN IS
        l_program VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.EXEC_STG_MULTIBUY_GET_INS';
    BEGIN
        INSERT INTO rpm_stage_multibuy_rwdlist VALUES i_multibuyget_temp_rec;

        COMMIT;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END exec_stg_multibuy_get_ins;   
------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_PROMO( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       NUMBER,
                       I_chunk_id        IN       NUMBER,
                       O_promo_id           OUT   CRC_PROMO_ID_TBL )
RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.CREATE_PROMO';
   L_PROCESS_ID            SVC_COST_SUSP_SUP_HEAD.PROCESS_ID%type;
   L_chunk_id              SVC_COST_SUSP_SUP_HEAD.CHUNK_ID%type;
   l_error_count           NUMBER(2):=0;
   L_mb_promo              VARCHAR2(1) := 'N';
   L_stg_mb_prc_id        RPM_STAGE_MULTIBUY_HEADER.PROCESS_ID%TYPE;
   L_luw                   NUMBER(6)   := 5000;
   L_max_thread            RPM_STAGE_MULTIBUY_HEADER.THREAD_NUM%TYPE;
   L_mb_total_count        NUMBER(12)  := 0;
   L_mb_error_count        NUMBER(12)  := 0;
   L_mb_for_apprv_count    NUMBER(12)  := 0;
   L_mb_generated_count    NUMBER(12)  := 0;

   LP_error_status         CONSTANT VARCHAR2(1) := 'E';
   LP_approved_status      CONSTANT VARCHAR2(1) := 'A';

   L_promo_id_rec          CRC_PROMO_ID_REC := NULL;


   Cursor C_get_luw is 
   select thread_luw_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.batch.InjectorPriceEventBatch';

------------------------------------------------Multi-Buy------------------------------------------------
   Cursor C_mb_promo ( P_process_id   NUMBER) is 
   select 'Y'
     from rpm_stage_multibuy_header
    where comments = to_char(P_process_id)
      and status   = 'N';

   Cursor C_get_mb_promo ( P_stg_thr_prc_id   RPM_STAGE_MULTIBUY_HEADER.PROCESS_ID%TYPE ) is 
   select distinct thread_num
     from rpm_stage_multibuy_header
    where process_id = P_stg_thr_prc_id;

   Cursor C_get_error_mb ( P_process_id      NUMBER,
                           P_stg_mb_prc_id   RPM_STAGE_MULTIBUY_HEADER.PROCESS_ID%TYPE) is
   select csps.row_seq,
          rstp.error_message
     from rpm_stage_multibuy_header rstp,
          crc_svc_multibuy_hdr_stg csps
    where rstp.comments   = to_char(P_process_id)
      and rstp.process_id = P_stg_mb_prc_id
      and rstp.status     = LP_error_status
      and rstp.name       = UPPER(csps.promo_name)
      --and rstp.item       = csps.Item
      and csps.process_id = P_process_id
      and csps.chunk_id   = I_chunk_id
     union
     select csps.row_seq,
            rstp.buy_rwdlist_err_msg error_message
       from rpm_stage_multibuy_header rstp,
            crc_svc_multibuy_hdr_stg csps
      where rstp.comments           = to_char(P_process_id)
        and rstp.process_id         = P_stg_mb_prc_id
        and rstp.buy_rwdlist_status = LP_error_status
        and rstp.name               = UPPER(csps.promo_name)
        --and rstp.item       = csps.Item
        and csps.process_id         = P_process_id
        and csps.chunk_id           = I_chunk_id;
------------------------------------------------Multi-Buy------------------------------------------------

   Cursor C_get_promo (P_stg_mb_prc_id    RPM_STAGE_MULTIBUY_HEADER.PROCESS_ID%TYPE  ) is
   select promo_id
     from rpm_stage_multibuy_header
    where process_id = P_stg_mb_prc_id
      and status     = LP_approved_status
      and promo_id is NOT NULL;


BEGIN

if  LP_errors_tab.COUNT()=0 
   then
       O_promo_id := CRC_PROMO_ID_TBL();

       open C_get_luw;
       fetch C_get_luw into L_luw;
       close C_get_luw;

       open C_mb_promo( I_process_id );
       fetch C_mb_promo   into L_mb_promo;
       close C_mb_promo;

       IF ( L_mb_promo = 'Y' ) THEN

          IF CRC_PRICE_EVENT_INJECTOR_SQL.GET_PROCESS_ID ( O_error_message,
                                                           L_stg_mb_prc_id,
                                                           I_process_id,
                                                           'N',
                                                           RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION ) = 0 THEN

              RETURN FALSE;
          END IF;
          commit;   

          IF RPM_PRICE_EVENT_INJECTOR_SQL.PRE_INIT_THREADS ( O_error_message,
                                                             L_stg_mb_prc_id,
                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                             'N' ) = 0 THEN

              RETURN FALSE;
          END IF;
          commit;   

          IF RPM_PRICE_EVENT_INJECTOR_SQL.INIT_THREADS ( O_error_message,
                                                         L_stg_mb_prc_id,
                                                         'N',
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                         L_luw ) = 0 THEN

              RETURN FALSE;
          END IF;
          commit;   

          IF RPM_PRICE_EVENT_INJECTOR_SQL.POST_INIT_THREADS ( O_error_message,
                                                              L_max_thread,
                                                              L_stg_mb_prc_id,
                                                              RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                             'N') = 0 THEN

              RETURN FALSE;
          END IF;
          commit;


         for l_thread_num in 1 .. nvl(L_max_thread,0) loop
            if CRC_PRICE_EVENT_INJECTOR_SQL.GENERATE_MULTIBUY_PROMOTION ( O_error_message,
                                                                          l_thread_num,
                                                                          L_stg_mb_prc_id,
                                                                          'CRC_CORESVC_MULTIBUY_PROMO',
                                                                          'N' ) <> 1
            then           
                if RPM_PRICE_EVENT_INJECTOR_SQL.ROLL_BACK_VALIDATION_THREAD( O_error_message,
                                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                                             L_stg_mb_prc_id,
                                                                             l_thread_num,
                                                                             'N',
                                                                             O_error_message) <> 1
                then

                    return false;
                end if;        
            end if;
        end loop;
          IF RPM_PRICE_EVENT_INJECTOR_SQL.POST_VALIDATE ( O_error_message,
                                                          L_mb_total_count    ,
                                                          L_mb_error_count    ,
                                                          L_mb_for_apprv_count,
                                                          L_mb_generated_count,
                                                          L_stg_mb_prc_id,
                                                          RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                          'N') = 0 THEN

              RETURN FALSE;
          END IF;

          IF ( L_mb_error_count <> 0 ) THEN
             FOR i IN C_get_error_mb( I_process_id,L_stg_mb_prc_id )

             LOOP

                WRITE_ERROR( I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_chunk_id,
                             'RPM_STAGE_MULTIBUY_HEADER',
                             i.row_seq,
                             'CRC_CORESVC_MULTIBUY_PROMO.CREATE_PROMO',
                             i.error_message);
          END LOOP;

          END IF;
       END IF;
    --------------------------------Multi-Buy------------------------------------------------
       IF ( L_mb_for_apprv_count <> 0 ) THEN
          FOR i IN C_get_promo( L_stg_mb_prc_id )
          LOOP
             L_promo_id_rec := CRC_PROMO_ID_REC (i.promo_id);
             O_promo_id.extend;
             O_promo_id(O_promo_id.count) := L_promo_id_rec;
             L_promo_id_rec := NULL;
          END LOOP;
       END IF;
END IF;
  return true;
EXCEPTION
   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      WRITE_ERROR( I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   'RPM_STAGE_MULTIBUY_HEADER',
                   1,
                   'CRC_CORESVC_MULTIBUY_PROMO.CREATE_PROMO',
                   O_error_message); 
     return false;
END CREATE_PROMO;
----------------------------------------------------------------------------------------------------------------------------

    FUNCTION process (
        o_error_message   IN OUT  rtk_errors.rtk_text%TYPE,
        o_error_count     IN OUT  NUMBER,
        i_process_id      IN      NUMBER,
        i_chunk_id        IN      NUMBER
    ) RETURN BOOLEAN IS

        l_program          VARCHAR2(64) := 'CRC_CORESVC_MULTIBUY_PROMO.PROCESS';
        l_process_status   svc_process_tracker.status%TYPE := 'PS';
        l_err_count        VARCHAR2(1);
        l_warn_count       VARCHAR2(1);
        L_promo_id       CRC_PROMO_ID_TBL := NULL; 
        L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PROMO_DATA';
        L_error           BOOLEAN; 

        CURSOR c_get_err_count IS
        SELECT
            'x'
        FROM
            svc_admin_upld_er
        WHERE
            process_id = i_process_id
            AND error_type = 'E';

        CURSOR c_get_warn_count IS
        SELECT
            'x'
        FROM
            svc_admin_upld_er
        WHERE
            process_id = i_process_id
            AND error_type = 'W';

    BEGIN
        lp_primary_lang := language_sql.get_primary_language;
        lp_errors_tab := NEW errors_tab_typ();

        IF process_promo_hdr(o_error_message, i_process_id, i_chunk_id) = false THEN        
              WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      0,
                      'CRC_CORESVC_MULTIBUY_PROMO.PROCESS_PROMO_HDR',
                      O_error_message);  
                   L_error:= FALSE;
        END IF; 

        IF process_promo_buy(o_error_message, i_process_id, i_chunk_id) = false THEN        
             WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      0,
                      'CRC_CORESVC_MULTIBUY_PROMO.process_promo_buy',
                      O_error_message);  

                   L_error:= FALSE;
        END IF;

       IF process_promo_get(o_error_message, i_process_id, i_chunk_id) = false THEN
              WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      0,
                      'CRC_CORESVC_MULTIBUY_PROMO.PROCESS_PROMO_GET',
                      O_error_message);  

                   L_error:= FALSE;
        END IF; 

    O_error_count := LP_errors_tab.COUNT();

   IF CREATE_PROMO ( O_error_message,
                     I_process_id,
                     I_Chunk_id,
                     L_promo_id ) = FALSE then 
       rollback;      
       WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      0,
                      'CRC_CORESVC_MULTIBUY_PROMO.CREATE_PROMO',
                      O_error_message);  

                   L_error:= FALSE;    
   END IF;
-----------------------------------------------
--Buy deal creation calling

    O_error_count := LP_errors_tab.COUNT();
		if create_deals_buy(O_error_message,
				I_process_id,
				I_Chunk_id) = FALSE then  

		rollback;
		  L_error:= FALSE;
		end if; 

----------------------------------------------
-- Get deal creation calling
    O_error_count := LP_errors_tab.COUNT();
		if create_deals_get(O_error_message,
				I_process_id,
				I_Chunk_id) = FALSE then  
		rollback;
		  L_error:= FALSE;
		end if; 

-----------------------------------------------
--- Create fixed deal for Buy
     O_error_count := LP_errors_tab.COUNT();   
     if create_fixed_deals_buy(O_error_message  ,
                    I_process_id ,
                    I_Chunk_id) = FALSE then 
        rollback;
         L_error:= FALSE;
   end if;

-----------------------------------------------
--- Create fixed deal for get
   O_error_count := LP_errors_tab.COUNT(); 

    if create_fixed_deals_get(O_error_message  ,
                    I_process_id ,
                    I_Chunk_id) = FALSE then 
        rollback;
         L_error:= FALSE;
   end if;

-----------------------------------
-- Create CC for Get
     O_error_count := LP_errors_tab.COUNT();
   if create_cc_get(O_error_message  ,
                    I_process_id ,
                    I_Chunk_id) = FALSE then
                          rollback;
        L_error:= FALSE;

   end if; 
---------------------------------------

--Create CC for Buy
     O_error_count := LP_errors_tab.COUNT();
   if Create_cc_buy(O_error_message  ,
                    I_process_id ,
                    I_Chunk_id) = FALSE then
                          rollback;
        L_error:= FALSE;

   end if;
------------------------------------------
--Submit cost change
     O_error_count := LP_errors_tab.COUNT();
   if COST_CHG_EXE(O_error_message  ,
                    I_process_id ,
                    I_Chunk_id) = FALSE then
                          rollback;
        L_error:= FALSE;

   end if;
------------------------------------------
    o_error_count := lp_errors_tab.count();

        FORALL i IN 1..o_error_count
            INSERT INTO svc_admin_upld_er VALUES lp_errors_tab ( i );

        lp_errors_tab := NEW errors_tab_typ();
        OPEN c_get_err_count;
        FETCH c_get_err_count INTO l_err_count;
        CLOSE c_get_err_count;
        OPEN c_get_warn_count;
        FETCH c_get_warn_count INTO l_warn_count;
        CLOSE c_get_warn_count;
        IF l_err_count IS NOT NULL THEN
            l_process_status := 'PE';
        ELSIF l_warn_count IS NOT NULL THEN
            l_process_status := 'PW';
        ELSE
            l_process_status := 'PS';
        END IF;

        UPDATE svc_process_tracker
        SET
            status = (
                CASE
                    WHEN status = 'PE' THEN
                        'PE'
                    ELSE
                        l_process_status
                END
            ),
            action_date = sysdate
        WHERE
            process_id = i_process_id;

        COMMIT;
        RETURN true;
    EXCEPTION
        WHEN OTHERS THEN
            o_error_message := sql_lib.create_msg('PACKAGE_ERROR', sqlerrm, l_program, to_char(sqlcode));

            RETURN false;
    END process;

end crc_coresvc_multibuy_promo;
/